package com.picone.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;


import com.picone.chatting.ConnectionMgrService;
import com.picone.commons.Commons;
import com.picone.logger.Logger;

import org.jivesoftware.smack.packet.Presence;

import static org.jivesoftware.smackx.filetransfer.FileTransfer.Error.connection;

/**
 * Created by LianXingHua on 11/7/2016.
 */

public class ShutdownReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(Intent.ACTION_SHUTDOWN))
        {
            if (ConnectionMgrService.mConnection != null || ConnectionMgrService.mConnection.isConnected()) {
                Presence pr=new Presence(Presence.Type.unavailable);
                //Commons.g_xmppService.sendPacket(pr);
                Commons.g_xmppService.disconnect();


                Logger.d("XMPP", "showdown this project ");


            }
        }
    }

}
