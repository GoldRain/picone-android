package com.picone.activity;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.picone.R;
import com.picone.base.CommonActivity;
import com.picone.commons.Commons;
import com.picone.commons.Constants;
import com.picone.utils.CustomUncaughtExceptionHandler;
import com.picone.utils.ShutdownReceiver;

public class IntroActivity extends CommonActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);

        Commons.g_introActivity = this;
        Commons.g_isAppRunning = true ;


        Thread.UncaughtExceptionHandler handler = Thread
                .getDefaultUncaughtExceptionHandler();

        if (!(handler instanceof CustomUncaughtExceptionHandler)) {
            Thread.setDefaultUncaughtExceptionHandler(new CustomUncaughtExceptionHandler(
                    this));
        }

        IntentFilter filter = new IntentFilter(Intent.ACTION_SHUTDOWN);
        BroadcastReceiver mReceiver = new ShutdownReceiver();
        registerReceiver(mReceiver, filter);

        gotoLogin();
    }

    private void gotoLogin() {
        final String room = getIntent().getStringExtra(Constants.KEY_ROOM);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                Intent intent = new Intent(IntroActivity.this, MenuActivity.class);
                if (room != null)
                    intent.putExtra(Constants.KEY_ROOM, room);
                startActivity(intent);
                finish();
            }
        }, Constants.SPLASH_TIME);

    }
}
