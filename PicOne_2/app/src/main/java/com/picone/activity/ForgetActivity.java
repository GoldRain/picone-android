package com.picone.activity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.picone.PicOneApplication;
import com.picone.R;
import com.picone.base.CommonActivity;
import com.picone.commons.Constants;
import com.picone.commons.ReqConst;

import org.json.JSONException;
import org.json.JSONObject;

public class ForgetActivity extends CommonActivity implements View.OnClickListener {

    ImageView ui_imvBack;
    TextView ui_txvSendPwd;
    EditText ui_edtEmail;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget);

        loadLayout();
    }

    private void loadLayout() {
        ui_imvBack = (ImageView)findViewById(R.id.imv_back_for);
        ui_imvBack.setOnClickListener(this);

        ui_edtEmail = (EditText)findViewById(R.id.edt_email_for);

        ui_txvSendPwd = (TextView) findViewById(R.id.txv_send_pwd);
        ui_txvSendPwd.setOnClickListener(this);

        // Edit hide
        LinearLayout linearLayout = (LinearLayout)findViewById(R.id.lyt_container_for);
        linearLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                //TODO Auto-generatored method stub

                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(ui_edtEmail.getWindowToken(), 0);
                return false;
            }
        });
    }

    public void forgotPassword(){

        String url = ReqConst.SERVER_URL + ReqConst.REQ_FORGOTPWD ;
        String email = ui_edtEmail.getText().toString().trim();
        String params = String.format("/%s", email);

        url += params ;

        showProgress();

        Log.d("url=============forgot==", url);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {

                parseRespons(json);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                closeProgress();
                showAlertDialog(getString(R.string.error));
            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT,0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        PicOneApplication.getInstance().addToRequestQueue(stringRequest, url);
    }

    public void parseRespons(String json){

        closeProgress();

        try {

            JSONObject response = new JSONObject(json);

            int result_code = response.getInt(ReqConst.RES_CODE);

            if (result_code == ReqConst.CODE_SUCCESS){

                showToast("Success");

            } else if (result_code == ReqConst.CODE_UNREGISTER){

                closeProgress();
                showAlertDialog(getString(R.string.unregister_user));
            }
        } catch (JSONException e) {

            closeProgress();
            showAlertDialog(getString(R.string.error));
            e.printStackTrace();
        }
    }

    public boolean checkEmail(){

        if (!Patterns.EMAIL_ADDRESS.matcher(ui_edtEmail.getText().toString()).matches() || ui_edtEmail.getText().toString().length() == 0) {

            ui_edtEmail.setError("Please check your email address!");
            return false;

        } else if (!ui_edtEmail.getText().toString().endsWith("@gmail.com")){

            ui_edtEmail.setError(getString(R.string.enter_email_enter));
            return false;
        }

        return true;
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.imv_back_for:

                Intent intent  = new Intent(this, LoginActivity.class);
                startActivity(intent);
                finish();
                break;

            case R.id.txv_send_pwd:

                if (checkEmail())
                forgotPassword();
                break;
        }

    }
}
