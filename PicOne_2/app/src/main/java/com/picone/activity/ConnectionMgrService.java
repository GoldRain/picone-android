package com.picone.activity;

import android.app.Service;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.IBinder;

import com.picone.R;
import com.picone.chatting.MyChatManager;
import com.picone.chatting.MyChatMessageListener;
import com.picone.commons.Commons;
import com.picone.commons.Constants;
import com.picone.commons.ReqConst;
import com.picone.logger.Logger;
import com.picone.model.Database;
import com.picone.preference.PrefConst;
import com.picone.preference.Preference;

import org.jivesoftware.smack.AbstractConnectionListener;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.ReconnectionManager;
import org.jivesoftware.smack.SASLAuthentication;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.chat.Chat;
import org.jivesoftware.smack.chat.ChatManager;
import org.jivesoftware.smack.chat.ChatManagerListener;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;

import java.util.List;

import de.greenrobot.event.EventBus;

/*
* The ConnectionMgrService class handles all of the XMPP connection (mainly log in and disconnection)
* We implement a Service to preserve the connection long-term.
* Ideally, you should also be implementing routine server pings to keep the connection alive (otherwise
*   you may create a 'zombie connection' - a connection that's somewhat connected but unable to receive input)
* */
public class ConnectionMgrService extends Service {

    protected static final String TAG = "XMPP";

    MainActivity _activity;

    /*
    * SERVICE_NAME and HOST_NAME are your server details.
    * Make sure you edit this with your own
    * */

    public static XMPPTCPConnection mConnection = null;
    private XMPPTCPConnectionConfiguration mConnectionConfiguration;

    private boolean startConnected = false;
    private int _startFrom = Constants.XMPP_FROMBROADCAST;

    private final IBinder mBinder = new ServiceBinder();

    public boolean isConnected = false;

    public class ServiceBinder extends Binder {
        ConnectionMgrService mService() {
            return ConnectionMgrService.this;
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Commons.g_xmppService = this;
        _startFrom = Constants.XMPP_FROMBROADCAST;
        Database.init(getApplicationContext());

        onHandleIntent(intent);
        return START_STICKY;
    }

    // Handles incoming events
    protected void onHandleIntent(Intent intent) {

        if (intent != null) {
            _startFrom = intent.getIntExtra(Constants.XMPP_START, Constants.XMPP_FROMBROADCAST);
        }

        String xmppID = Preference.getInstance().getValue(this,
                PrefConst.PREFKEY_XMPPID, null);
        String userpwd = Preference.getInstance().getValue(this,
                PrefConst.PREFKEY_USERPWD, null);

        if(xmppID != null && userpwd != null) {
            startLogin(xmppID, userpwd);
        }

    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    /*
    * startLogin creates the connection for the log in process.
    * First, a connection to the server must be established. After the connection
    *   is established, then only can you process the login details.
    * */
    private void startLogin(final String username, final String password) {

        new Thread(new Runnable() {
            @Override
            public void run() {

                mConnectionConfiguration = XMPPTCPConnectionConfiguration.builder()
                        .setUsernameAndPassword(username, password) // The username and password supplied by the user
                        .setServiceName(ReqConst.CHATTING_SERVER) // Service name
                        .setPort(5222) // Incoming port (might depend on your XMPP server software)
                        .setSecurityMode(ConnectionConfiguration.SecurityMode.disabled) // Security mode is disabled for example purposes
                        .setResource("campus")
                        .build();

                mConnection = new XMPPTCPConnection(mConnectionConfiguration);
                mConnection.setPacketReplyTimeout(30000);

                mConnection.addConnectionListener(myConnectionListener);

                ReconnectionManager reconnectionMgr = ReconnectionManager.getInstanceFor(mConnection);
                reconnectionMgr.enableAutomaticReconnection();

                try {
                    mConnection.connect();
                    startConnected = true;
                } catch (Exception e) {
                    e.printStackTrace();
                }

                // If the connection is successful, we begin the login process
                if(startConnected) {
                    Logger.d(TAG, "======================Connected======================");
                    connectionLogin();

                } else {
                    Logger.d(TAG, "======================Unable to connect======================");
                    if (_startFrom == Constants.XMPP_FROMLOGIN)
                        EventBus.getDefault().post(new LoggedInEvent(false));
                }
            }
        }).start();
    }

    private boolean loggedIn = true;

    private void connectionLogin() {

        try {
            SASLAuthentication.unBlacklistSASLMechanism("PLAIN");
            SASLAuthentication.blacklistSASLMechanism("DIGEST-MD5");
            mConnection.login();

        } catch (Exception e) {
            e.printStackTrace();
            loggedIn = false;
        }

        // If the login fails, we disconnect from the server
        if(!loggedIn) {

            Logger.d(TAG, "======================LoggedIn Fail======================");
            disconnect();
            loggedIn = true;

            if (_startFrom == Constants.XMPP_FROMLOGIN)
                EventBus.getDefault().post(new LoggedInEvent(false));


        } else {
            // If the login succeeds, we implement the chat listener.
            // It's important to implement the listener here so we can receive messages sent to us
            //      when we're offline.
            Logger.d(TAG, "======================LoggedIn Success======================");

            createChatListener();

            // Callback to LoginScreen to change the UI to the ChatScreen listview
            if (_startFrom == Constants.XMPP_FROMLOGIN)
                EventBus.getDefault().post(new LoggedInEvent(true));

        }
    }

    private AbstractConnectionListener myConnectionListener = new AbstractConnectionListener() {

        @Override
        public void authenticated(XMPPConnection connection, boolean resumed) {
            super.authenticated(connection, resumed);
        }

        @Override
        public void connected(XMPPConnection connection) {
            super.connected(connection);
            isConnected = true;
            Logger.d("XMPP", "xmppconnection connected.");
        }

        @Override
        public void connectionClosed() {
            super.connectionClosed();

            if (mConnection != null)
                mConnection.removeConnectionListener(myConnectionListener);
            myConnectionListener = null;
            mConnection = null;

            isConnected = false;
            MyChatManager.isJoined = false;
            Logger.d("XMPP", "xmppconnection closed.");
        }

        @Override
        public void connectionClosedOnError(Exception e) {
            super.connectionClosedOnError(e);

            isConnected = false;
            MyChatManager.isJoined = false;
            if (Commons.g_isAppRunning) {
                Commons.g_currentActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Commons.g_currentActivity.showToast(getString(R.string.chatting_error));
                    }
                });
            }
            Logger.d("XMPP", "xmppconnection closed with error.");
        }

        @Override
        public void reconnectingIn(int seconds) {
            super.reconnectingIn(seconds);

            Logger.d("XMPP", "xmppconnection reconnecting");
        }

        @Override
        public void reconnectionFailed(Exception e) {
            super.reconnectionFailed(e);
            isConnected = false;
            MyChatManager.isJoined = false;
            Logger.d("XMPP", "xmppconnection reconnection failed");
        }

        @Override
        public void reconnectionSuccessful() {
            super.reconnectionSuccessful();
            Logger.d("XMPP", "xmppconnection reconnection successed.");
            isConnected = true;
            if (Commons.g_isAppRunning) {
                Commons.g_currentActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Commons.g_currentActivity.showToast(getString(R.string.chatting_success));
                    }
                });
            }

            if (Commons.g_chattingActivity != null) {
                Commons.g_chattingActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                       //Commons.g_chattingActivity.reenterRoom();
                      // ChattingFragment.reenterRoom();
                    }
                });
            }
        }
    };

    /*
    * CreateChatListener implements the listener class for incoming chats.
    * The class is MyChatMessageListener
    * DISCLAIMER: You should be renewing the listener if the user logs in as another user, otherwise you may
    *   have duplicate messages
    * */
    private MyChatMessageListener mChatMessageListener;

    private void createChatListener() {

        if(mConnection != null) {

            ChatManager chatManager = ChatManager.getInstanceFor(mConnection);
            chatManager.setNormalIncluded(false); // Eliminates a few debug messages

            chatManager.addChatListener(new ChatManagerListener() {
                @Override
                public void chatCreated(Chat chat, boolean createdLocally) {

//                    if (!createdLocally) {
                        Logger.d("XMPP", "chat listener created");
//                        mChatMessageListener = new MyChatMessageListener();
                        chat.addMessageListener(new MyChatMessageListener());
//                    }
                }
            });
        }
    }

    public void updateBadgeCount(int count) {

//        Commons.g_badgCount = count;
//
//        Intent intent = new Intent("android.intent.action.BADGE_COUNT_UPDATE");
//        intent.putExtra("badge_count_package_name", getPackageName());
//        intent.putExtra("badge_count_class_name", getLauncherClassName());
//        intent.putExtra("badge_count", Commons.g_badgCount);
//        sendBroadcast(intent);
    }

    private String getLauncherClassName() {

        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
        intent.setPackage(getPackageName());

        List<ResolveInfo> resolveInfoList = getPackageManager().queryIntentActivities(intent, 0);
        if(resolveInfoList != null && resolveInfoList.size() > 0) {
            return resolveInfoList.get(0).activityInfo.name;
        }

        return null;
    }

    /*
    * This disconnection method is created here to validate if the connection is not null otherwise
    *   it may crash the application.
    * */
    public void disconnect() {

        if (mConnection != null && mConnection.isConnected()) {

            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... params) {

                    mConnection.removeConnectionListener(myConnectionListener);
                    myConnectionListener = null;
                    mConnection.disconnect();
                    mConnection = null;
                    return null;
                }
            }.execute();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        disconnect();
    }
}
