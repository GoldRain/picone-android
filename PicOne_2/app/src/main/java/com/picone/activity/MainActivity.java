package com.picone.activity;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.picone.R;
import com.picone.base.CommonTabActivity;
import com.picone.chatting.ChatlistViewAdapter;
import com.picone.chatting.ChattingActivity;
import com.picone.commons.Commons;
import com.picone.commons.Constants;
import com.picone.fragment.ChatlistViewFragment;
import com.picone.fragment.ChattingFragment;
import com.picone.fragment.ContactsFragment;
import com.picone.fragment.EditProfileFragment;
import com.picone.fragment.FollowerFragment;
import com.picone.fragment.FollowingFragment;
import com.picone.fragment.InviteFriendsFragment;
import com.picone.fragment.MyFullFragment;
import com.picone.fragment.MyProfileFragment;
import com.picone.fragment.NewsfeedsFragment;
import com.picone.fragment.NotiFragment;
import com.picone.fragment.UserFullFragment;
import com.picone.fragment.UserProfileFragment;
import com.picone.model.Database;
import com.picone.model.Friendmodel;
import com.picone.model.RoomEntity;
import com.picone.model.VoteEntity;
import com.picone.preference.PrefConst;
import com.picone.preference.Preference;
import com.picone.utils.RadiusImageView;

import java.util.ArrayList;

public class MainActivity extends CommonTabActivity implements View.OnClickListener, NavigationView.OnNavigationItemSelectedListener {

    ImageView ui_imvCallmenu, ui_imvNotify, ui_imvMessage ,ui_imvEdit;
    TextView ui_userName , ui_txvThemtitle;
    NavigationView ui_drawerMenu;
    DrawerLayout ui_drawerlayout;
    ActionBarDrawerToggle drawerToggle;

    ChatlistViewAdapter _adapter;

    String[] PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE,  Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_SMS, Manifest.permission.CAMERA};
    public static int MY_PEQUEST_CODE = 123;

    View headerView ;

    RadiusImageView imv_photo ;

    TextView ui_notiShow ;

    ///
    ArrayList<RoomEntity> _rooms = new ArrayList<>();
    ChatlistViewFragment fragment ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        String room = getIntent().getStringExtra(Constants.KEY_ROOM);
        boolean fromlogin = getIntent().getBooleanExtra(Constants.KEY_FROMLOGIN, false);
        if (room != null) {
            Intent intent = new Intent(this, ChattingActivity.class);
            intent.putExtra(Constants.KEY_ROOM, room);
            startActivity(intent);
        }

        loadLayout();
        setupNavigationBar();
    }

    //==================== CARMERA Permission========================================
    public static boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        if (requestCode == MY_PEQUEST_CODE
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            //  gps functionality

        }
    }

    private void loadLayout() {

        ////////////////////////
        fragment =  new ChatlistViewFragment();
        _rooms = Commons.g_user.get_roomList();
        _adapter = new ChatlistViewAdapter(fragment);

        ui_txvUnread = (TextView) findViewById(R.id.txv_unread_message_main);
        setUnRead();


        ui_imvCallmenu = (ImageView)findViewById(R.id.imv_menu);
        ui_imvCallmenu.setOnClickListener(this);

        ui_imvMessage = (ImageView)findViewById(R.id.imv_message);
        ui_imvMessage.setOnClickListener(this);

        ui_imvNotify = (ImageView)findViewById(R.id.imv_notify);
        ui_imvNotify.setOnClickListener(this);

        ui_drawerlayout = (DrawerLayout)findViewById(R.id.drawerlayout);
        ui_drawerMenu = (NavigationView)findViewById(R.id.drawer_menu);
        ui_drawerMenu.setNavigationItemSelectedListener(this);

        headerView = ui_drawerMenu.getHeaderView(0);
        ui_imvEdit = (ImageView)headerView.findViewById(R.id.imv_edit);
        ui_userName = (TextView)headerView.findViewById(R.id.txv_name);
        ui_txvThemtitle = (TextView)findViewById(R.id.txv_themetitle);

        imv_photo = (RadiusImageView)headerView.findViewById(R.id.imv_photo);

        FrameLayout ui_header = (FrameLayout)headerView.findViewById(R.id.header);
        ui_header.setOnClickListener(this);

        ui_notiShow = (TextView)findViewById(R.id.txv_unread_noti);
        showNotiRefresh();

        photoUpdate();
        selectDrawerItem(0);

        ui_drawerlayout.addDrawerListener(drawerToggle);

    }

    public void photoUpdate() {

        if ((Commons.g_user.toString().length() > 0)){

            Log.d("Photo URL ===========>", Commons.g_user.get_photoUrl());

            ui_userName.setText(Commons.g_user.get_userName());
        }

        if (Commons.g_user.get_photoUrl().length() != 0 && !Commons.g_user.get_photoUrl().equals(null)){

            Glide.with(this).load(Commons.g_user.get_photoUrl()).placeholder(R.drawable.image_me).into(imv_photo);

            /*imv_photo.setImageUrl(Commons.g_user.get_photoUrl(),_imageLoader);*/
            /*ui_userName.setText(Commons.g_user.get_fullName());*/

        }else {

            imv_photo.setImageResource(R.drawable.image_me);
        }
    }

    void  setInit(){
        if(Constants.bitmap_my !=null)imv_photo.setImageBitmap(Constants.bitmap_my);
    }
    private void setupNavigationBar() {

        drawerToggle = new ActionBarDrawerToggle(this, ui_drawerlayout, R.string.drawer_open,R.string.drawer_close){

            @Override
            public void onDrawerOpened(View drawerView) {super.onDrawerOpened(drawerView);}

            @Override
            public void onDrawerClosed(View drawerView) {super.onDrawerClosed(drawerView);}
        };

        ui_drawerlayout.setDrawerListener(drawerToggle);
        drawerToggle.syncState();

    }

    public void selectDrawerItem(int position) {

        Fragment fragment = null ;
        Class fragmentClass = null ;

        switch (position){

            case 0:
                fragmentClass = NewsfeedsFragment.class ;
                ui_txvThemtitle.setText(getString(R.string.newsfeeds));

                break;
            case 1:
                fragmentClass = MyProfileFragment.class;
                ui_txvThemtitle.setText(getString(R.string.myprofile));
                break;
            case 2:
                fragmentClass = ContactsFragment.class ;
                ui_txvThemtitle.setText(getString(R.string.contacts));
                break;

            case 3:
                fragmentClass = InviteFriendsFragment.class ;
                ui_txvThemtitle.setText(getString(R.string.invite_friends));
                break;


            default:return;
        }
        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();

        } catch (IllegalAccessException e) {
            e.printStackTrace();}

        getSupportFragmentManager().beginTransaction().replace(R.id.frm_container,fragment).commit();
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.nav_logout){
            logout();
        }

        if (id == R.id.nav_myprofile){
            selectDrawerItem(1);
        }else if (id == R.id.nav_contacts){
            selectDrawerItem(2);

        } else if (id == R.id.nav_invite){
            selectDrawerItem(3);

        } else if (id == R.id.nav_home){
            selectDrawerItem(0);
        }

        ui_drawerlayout.closeDrawers();
        return true;
    }

    private void logout() {

        Intent intent = new Intent(MainActivity.this, MenuActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        Preference.getInstance().put(this, PrefConst.PREFKEY_USEREMAIL, "");
        Preference.getInstance().put(this, PrefConst.PREFKEY_USERPWD, "");
        Preference.getInstance().put(this, PrefConst.PREFKEY_USERNAME, "");
        startActivity(intent);
        finish();
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.header:
                selectDrawerItem(1);
                ui_drawerlayout.closeDrawers();
                break;

            case R.id.imv_menu:
                ui_drawerlayout.openDrawer(GravityCompat.START);
                break;

            case R.id.imv_notify:
                NotiFragment();
                break;

            case R.id.imv_message:

                ChatlistViewFratment();

               /* Intent intent = new Intent(this, ChattingListActivity.class);
                startActivity(intent);
                break;*/
        }

    }

    public void EditProfileFragment() {

        ui_txvThemtitle.setText(getString(R.string.edit_profile));

        EditProfileFragment fragment = new EditProfileFragment();
        FragmentTransaction fragmentTransation = getSupportFragmentManager().beginTransaction();
        fragmentTransation.replace(R.id.frm_container,fragment).commit();
    }

    public void ChatFragment(){

        ui_txvThemtitle.setText(getString(R.string.chat));

        ChattingFragment fragment = new ChattingFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frm_container,fragment).commit();
    }

    public void NotiFragment() {

        ui_txvThemtitle.setText(getString(R.string.notification));

        NotiFragment fragment = new NotiFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frm_container,fragment).commit();
    }

    public void ChatlistViewFratment() {

        ui_txvThemtitle.setText(getString(R.string.chatlist));

        ChatlistViewFragment fragment = new ChatlistViewFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frm_container,fragment).commit();
    }

    public void UserProfileFratment(VoteEntity voteModel) {

        ui_txvThemtitle.setText(getString(R.string.userprofile));

        UserProfileFragment fragment = new UserProfileFragment(voteModel);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frm_container,fragment).commit();
    }

    public void UserFullFragment() {

        ui_txvThemtitle.setText(getString(R.string.userprofile));

        UserFullFragment fragment = new UserFullFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frm_container,fragment).commit();
    }

    public void MyFullFragment() {

        ui_txvThemtitle.setText(getString(R.string.myprofile));

        MyFullFragment fragment = new MyFullFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frm_container,fragment).commit();
    }

    public void FollowerFragment() {

        ui_txvThemtitle.setText(getString(R.string.follower));

        FollowerFragment fragment = new FollowerFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frm_container,fragment).commit();
    }

    public void FollowingFragment() {

        ui_txvThemtitle.setText(getString(R.string.following));

        FollowingFragment fragment = new FollowingFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frm_container,fragment).commit();
    }

    @Override
    protected void onResume() {
        super.onResume();

        setInit();
        setUnRead();
        showNotiRefresh();

    ////////////////////////////////////////////////////////////
        // Register mMessageReceiver to receive messages.
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                new IntentFilter("my-event"));
    }

    public void gotoChatting(Friendmodel friend){
        RoomEntity _room = null;
        _room = makeRoom(friend);
        Intent intent = new Intent(MainActivity.this, ChattingActivity.class);
        intent.putExtra(Constants.KEY_ROOM, _room.get_name());
        startActivity(intent);

    }

    public RoomEntity makeRoom(Friendmodel friend) {

        ArrayList<Friendmodel> participants = new ArrayList<>();
        participants.add(friend);

        RoomEntity room = new RoomEntity(participants);

        if (!Commons.g_user.get_roomList().contains(room)) {
            Commons.g_user.get_roomList().add(room);
            Database.createRoom(room);
        }

        return room;
    }
/*    public void showNoti(int a, int b){

        ui_notiShow.setVisibility(a);
        ui_notiShow.setText(String.valueOf(b));
    }*/

    public void showNotiRefresh(){

        if (Constants.noti_counter == 0){

            ui_notiShow.setVisibility(View.GONE);

        } else {

            ui_notiShow.setVisibility(View.VISIBLE);
            ui_notiShow.setText(String.valueOf(Constants.noti_counter));
        }
    }

    @Override
    public void onBackPressed() {

        if (this.ui_drawerlayout.isDrawerOpen(GravityCompat.START)) {

            this.ui_drawerlayout.closeDrawer(GravityCompat.START);

        } else {

            onExit();
        }
    }

    public void refresh(){_adapter.refresh();}


    //------------------------------

    // handler for received Intents for the "my-event" event
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Extract data included in the Intent
            //String message = intent.getStringExtra("message");
            //Log.d("receiver", "Got message: " + message);

            Log.d("======Const", String.valueOf(Constants.noti_counter));

           /* int a = View.VISIBLE , b = Constants.noti_counter;
            showNoti(a, b);*/

            showNotiRefresh();
        }
    };

    @Override
    protected void onPause() {
        // Unregister since the activity is not visible
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

}

