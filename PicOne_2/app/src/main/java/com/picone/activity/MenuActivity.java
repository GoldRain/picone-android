package com.picone.activity;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.picone.PicOneApplication;
import com.picone.R;
import com.picone.base.CommonActivity;
import com.picone.commons.Commons;
import com.picone.commons.Constants;
import com.picone.commons.ReqConst;
import com.picone.model.UserEntity;
import com.picone.preference.PrefConst;
import com.picone.preference.Preference;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;

import org.json.JSONException;
import org.json.JSONObject;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class MenuActivity extends CommonActivity implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {

    TextView ui_txvLogin, ui_txvSignin, ui_txvFacebook, ui_txvGoogle , ui_txvTwitter;

    //private TwitterLoginButton ui_txvTwitter;

    // facebook login
    public static CallbackManager callbackManager;
    private String FEmail, Name,Firstname, Lastname,Id,Gender,Image_url;
    String photourl;

    //google login
    private static final int RC_SIGN_IN = 9001;
    private GoogleApiClient mGoogleApiClient;

    //twitter login
    TwitterAuthClient mTwitterAuthClient= new TwitterAuthClient();

    UserEntity _user = new UserEntity();
    String token = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();

        loadLayout();

        try
        {

            PackageInfo info = getPackageManager().getPackageInfo("com.picone", PackageManager.GET_SIGNATURES);

            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");

                md.update(signature.toByteArray());
                Log.i("KeyHash::", Base64.encodeToString(md.digest(), Base64.DEFAULT));//will give developer key hash
//                Toast.makeText(getApplicationContext(), Base64.encodeToString(md.digest(), Base64.DEFAULT), Toast.LENGTH_LONG).show(); //will give app key hash or release key hash

            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {}

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }

    private void loadLayout() {

        ui_txvLogin = (TextView)findViewById(R.id.txv_login);
        ui_txvLogin.setOnClickListener(this);

        ui_txvSignin = (TextView)findViewById(R.id.txv_signup);
        ui_txvSignin.setOnClickListener(this);

        ui_txvFacebook = (TextView)findViewById(R.id.txv_facebook);
        ui_txvFacebook.setOnClickListener(this);

        ui_txvGoogle = (TextView)findViewById(R.id.txv_google);
        ui_txvGoogle.setOnClickListener(this);

        ui_txvTwitter = (TextView) findViewById(R.id.txv_twitter);
        ui_txvTwitter.setOnClickListener(this);



//        ui_txvTwitter = (TwitterLoginButton)findViewById(R.id.txv_twitter);
//        ui_txvTwitter.setCallback(new Callback<TwitterSession>() {
//
//            @Override
//            public void success(Result<TwitterSession> result) {
//
//                // The TwitterSession is also available through:
//                // Twitter.getInstance().core.getSessionManager().getActiveSession()
//                TwitterSession session = result.data;
//                // TODO: Remove toast and use the TwitterSession's userID
//                // with your app's user model
//                String msg = "@" + session.getUserName() + " logged in! (#" + session.getUserId() + ")";
//                Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
//            }
//            @Override
//            public void failure(TwitterException exception) {
//                Log.d("TwitterKit", "Login with Twitter failure", exception);
//            }
//        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){

            case R.id.txv_login:
                gotoLogin();
                break;

            case R.id.txv_signup:
                gotoSignin();
                break;

            case R.id.txv_facebook:
                loginWithFB();
                break;
            case R.id.txv_google:

                googleSignin();
                break;

            case R.id.txv_twitter:

                mTwitterAuthClient.authorize(this, new com.twitter.sdk.android.core.Callback<TwitterSession>() {

                    @Override
                    public void success(Result<TwitterSession> twitterSessionResult) {
                        // Success
                        TwitterSession session = twitterSessionResult.data;

                        final String username = session.getUserName();
                        final String firstname = "", lastname = "", email = "", photoUrl = "";

                        Constants.twitter_login = 1 ;

                        SocialLogin( username,firstname, lastname, email, photoUrl);
                    }

                    @Override
                    public void failure(TwitterException e) {
                        e.printStackTrace();
                    }
                });

                break;
        }
    }


    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        // An unresolvable error has occurred and Google APIs (including Sign-In) will not
        // be available.
        Log.d("Connect State", "onConnectionFailed:" + connectionResult);
    }

    @Override
    protected void onStart() {

        super.onStart();
/*
        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);
        if (opr.isDone()) {
            // If the user's cached credentials are valid, the OptionalPendingResult will be "done"
            // and the GoogleSignInResult will be available instantly.
            Log.d("signin", "Got cached sign-in");
            GoogleSignInResult result = opr.get();
            handleSignInResult(result);

        } else {
            // If the user has not previously signed in on this device or the sign-in has expired,
            // this asynchronous branch will attempt to sign in the user silently.  Cross-device
            // single sign-on will occur in this branch.
            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(GoogleSignInResult googleSignInResult) {

                    handleSignInResult(googleSignInResult);
                }
            });
        }
*/
    }


    private void gotoLogin() {

        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();

    }

    private void gotoSignin() {

        Intent intent = new Intent(MenuActivity.this , SingupActivity.class);
        startActivity(intent);
        finish();
    }

    public void gotoMain(){

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);

        finish();
    }

    private void googleSignin() {

        Constants.google_login = 1;
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    //====================================Facebook Login Start======================================
    private void loginWithFB() {

        callbackManager = CallbackManager.Factory.create();

        // set permissions
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("email", "user_photos", "public_profile"));

        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                AccessToken accessToken = loginResult.getAccessToken();
                Profile profile = Profile.getCurrentProfile();

                // Facebook Email address
                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {

                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {

                                Log.v("LoginActivity Response ", response.toString());

                                try {
                                    Name = object.getString("name");
                                    Name.replace(" ", "");
                                    Id=object.getString("id");
                                    Firstname = object.getString("first_name");
                                    Lastname = object.getString("last_name");
                                    Gender = object.getString("gender");
                                    //String PWD = object.getString("password");
                                    FEmail = object.getString("email");
                                    Image_url = "http://graph.facebook.com/(Id)/picture?type=large";
                                    Image_url = URLEncoder.encode(Image_url);

                                    Log.d("Email = ", " " + FEmail);
                                    Log.d("Name======",Name);
                                    Log.d("firstName======",Firstname);
                                    Log.d("lastName======",Lastname);
                                    Log.d("Gender======",Gender);
                                    Log.d("id======",Id);

                                    photourl = "http://graph.facebook.com/"+Id+"/picture?type=large";

                                    Constants.facebook_login = 1;
                                    SocialLogin(Name, Firstname, Lastname, FEmail, photourl);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });

                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,first_name,last_name,email,gender, birthday");
                request.setParameters(parameters);
                request.executeAsync();

            }

            @Override
            public void onCancel() {
                LoginManager.getInstance().logOut();
            }

            @Override
            public void onError(FacebookException error) {

            }

        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        callbackManager.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN){

            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }

        mTwitterAuthClient.onActivityResult(requestCode, resultCode, data);

    }

    // [START handleSignInResult]
    private void handleSignInResult(GoogleSignInResult result) {

        Log.d("handSignResult ==> ", "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();

            SocialLogin( acct.getDisplayName(),acct.getFamilyName(), acct.getGivenName(),acct.getEmail(), String.valueOf(acct.getPhotoUrl()));

            Log.d("potoURL=========", String.valueOf(acct.getPhotoUrl()));

        } else {

        }
    }

    //==================================Face book Login End======================================

    //====================================== Social Login Start =================================

    public void SocialLogin( final String username, final String firstname, final String lastname, final String useremail, final String photo_url){


        showProgress();

        String url = ReqConst.SERVER_URL + ReqConst.REQ_FACEBOOK ;
        StringRequest myRequest = new StringRequest(
                Request.Method.POST, url ,new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                Log.d("response=====", String.valueOf(response));

                parseEditResponse(response);
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("error============", String.valueOf(error));
                        closeProgress();
                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                try {

                    String password = "123456";
                    token = Preference.getInstance().getValue(_context, PrefConst.PREFKEY_TOKEN, "");
                    //params.put("id",String.valueOf(Commons.g_user.get_idx()));

                    params.put("username",username);
                    params.put("firstname",firstname);
                    params.put("lastname",lastname);
                    params.put("useremail",  useremail);
                    params.put("photo_url", photo_url);
                    params.put("password", password);
                    params.put("token", token);


                } catch (Exception e) {}
                return params;
            }
        };

        myRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        PicOneApplication.getInstance().addToRequestQueue(myRequest,url);  // url or "tag"
    }

    private void parseEditResponse(String response) {

        Log.d("respons", response);

        try {

            JSONObject object = new JSONObject(response);

            //JSONObject response = new JSONObject(json);

            int result_code = object.getInt(ReqConst.RES_CODE);


            if (result_code == ReqConst.CODE_SUCCESS) {

                Commons.g_user = new UserEntity();

           /*     *//*showToast("Success change your profile !");*//*
                int id = object.getInt("id");
                Log.d("id====", String.valueOf(id));
                Commons.g_user.set_idx(object.getInt("id"));

                Commons.g_user.set_userName(Name);
                Commons.g_user.set_email(FEmail);
                Commons.g_user.set_photoUrl(photourl);*/


                closeProgress();

                JSONObject user = object.getJSONObject(ReqConst.RES_USERINFO);

                _user = new UserEntity();

                _user.set_idx(user.getInt(ReqConst.RES_ID));

                Log.d("id======", String.valueOf(_user.get_idx()));

                _user.set_firstName(user.getString(ReqConst.RES_FIRSTNAME));
                _user.set_lastName(user.getString(ReqConst.RES_LASTNAME));
                _user.set_userName(user.getString(ReqConst.RES_USERNAME));
                _user.set_email(user.getString(ReqConst.RES_EMAIL));

                _user.set_phoneNumber(user.getString(ReqConst.RES_PHONE));
                _user.set_country(user.getString(ReqConst.RES_COUNTRY));
                _user.set_adress(user.getString(ReqConst.RES_ADDRES));
                _user.set_interests(user.getString(ReqConst.RES_INTERESTS));
                _user.set_status(user.getString(ReqConst.REQ_STATUS));
                _user.set_photoUrl(user.getString(ReqConst.RES_PHOTO_URL));
                _user.set_following(user.getInt(ReqConst.RES_FOLLOWING));
                _user.set_follower(user.getInt(ReqConst.RES_FOLLOWER));

                //Preference.getInstance().put(this, PrefConst.PREFKEY_USERNAME,Name);
                //Preference.getInstance().put(this, PrefConst.PREFKEY_USEREMAIL,FEmail);

                Commons.g_user = _user ;
                gotoMain();

                closeProgress();

            }

        } catch (JSONException e) {

            e.printStackTrace();

            showAlertDialog("Network Error. Please Try Again.");
            closeProgress();
        }
    }
}
