package com.picone.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.picone.PicOneApplication;
import com.picone.R;
import com.picone.base.CommonActivity;
import com.picone.chatting.ConnectionMgrService;
import com.picone.commons.Commons;
import com.picone.commons.Constants;
import com.picone.commons.ReqConst;
import com.picone.logger.Logger;
import com.picone.model.Database;
import com.picone.model.Friendmodel;
import com.picone.model.RoomEntity;
import com.picone.model.UserEntity;
import com.picone.preference.PrefConst;
import com.picone.preference.Preference;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

import de.greenrobot.event.EventBus;

import static java.lang.String.valueOf;

public class LoginActivity extends CommonActivity implements View.OnClickListener {

    ImageView ui_imvBack;
    public  EditText ui_edtUsername, ui_edtPassword ;
    String _userName = "";
    String _password = "";
    String _tokenId = "" ;

    TextView ui_txvLogin, ui_txvForget;

    private UserEntity _user = new UserEntity();

    public static final int REGISTER_CODE = 100;
    private int _reqCounter = 0;
    private String _room = "";
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        _room = getIntent().getStringExtra(Constants.KEY_ROOM);
        Database.init(getApplicationContext());

        initValues();
        loadLayout();
    }

    boolean _isFromLogout = false ;

    private void initValues(){

        Intent intent = getIntent();
        try{
            _isFromLogout = intent.getBooleanExtra(Constants.KEY_LOGOUT, false);
        } catch (Exception e){
        }
    }

    public void loadLayout() {

        ui_imvBack = (ImageView)findViewById(R.id.imv_back_sign);
        ui_imvBack.setOnClickListener(this);

        ui_edtUsername = (EditText)findViewById(R.id.edt_username_login);
        ui_edtPassword = (EditText)findViewById(R.id.edt_pwd_login);

        ui_txvLogin = (TextView)findViewById(R.id.txv_login_log);
        ui_txvLogin.setOnClickListener(this);

        ui_txvForget = (TextView)findViewById(R.id.txv_forget);
        ui_txvForget.setOnClickListener(this);


        // container
        LinearLayout lytContainer = (LinearLayout) findViewById(R.id.lyt_contaner_login);
        lytContainer.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // TODO Auto-generated method stub
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(ui_edtUsername.getWindowToken(), 0);
                return false;
            }
        });

        if (_isFromLogout){

            //   save user to empty
            Preference.getInstance().put(this, PrefConst.PREFKEY_USERNAME,"");
            Preference.getInstance().put(this,PrefConst.PREFKEY_USERPWD, "");
            String tokenId = Preference.getInstance().getValue(this, PrefConst.PREFKEY_TOKEN, "");

            ui_edtUsername.setText("");
            ui_edtPassword.setText("");
            _tokenId = tokenId ;

        } else {

            String name = Preference.getInstance().getValue(this, PrefConst.PREFKEY_USERNAME, "");
            String pwd = Preference.getInstance().getValue(this, PrefConst.PREFKEY_USERPWD, "");
            String tokenId = Preference.getInstance().getValue(this, PrefConst.PREFKEY_TOKEN, "");


            if ( name.length()>0 && pwd.length() > 0 & tokenId.length() > 0) {

                Log.d("Password====++++======>",name+":"+pwd);

                ui_edtUsername.setText(name);
                ui_edtPassword.setText(pwd);
                _tokenId = tokenId ;

                processLogin();

                /*gotoMain();*/
            }
        }
    }

    public boolean checkValue(){

        if (ui_edtUsername.getText().toString().length() == 0){
            showAlertDialog(getString(R.string.enter_name));
            return false;

        } else if (ui_edtPassword.getText().toString().length() == 0){
            showAlertDialog(getString(R.string.enter_pwd));
            return false ;
        } /*else if (_tokenId.length()== 0){

            showAlertDialog("Can't get TokenId of your phone.!");
            return false ;
        }*/

        return true ;
    }

    private void processLogin() {

        String url = ReqConst.SERVER_URL + ReqConst.REQ_LOGIN ;

        showProgress();

        try {
            String username = ui_edtUsername.getText().toString().trim().replace(" ","%20");
            username = URLEncoder.encode(username,"utf-8");

            String password = ui_edtPassword.getText().toString().trim().replace(" ","%20");
            password = URLEncoder.encode(password,"utf-8");

            String tokenId = _tokenId.replace(" ", "%20");
            tokenId = URLEncoder.encode(tokenId, "utf-8");

            String params = String.format("/%s/%s/%s", username, password , tokenId);
            url += params ;

            Log.d("loginURL===========********======>", url);

        } catch (UnsupportedEncodingException e) {

            closeProgress();
            e.printStackTrace();}


        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String json) {

                //Log.d("response=====+++++",json);
                parseLoginResponse(json);

            }

        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                closeProgress();
                showAlertDialog(getString(R.string.error));

            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        PicOneApplication.getInstance().addToRequestQueue(stringRequest, url);

    }

    private void parseLoginResponse(String json) {

        try {
            JSONObject response = new JSONObject(json);

            int result_code = response.getInt(ReqConst.RES_CODE);

            Log.d("ResponseRESULT=====**********=========>", json);

            if (result_code == ReqConst.CODE_SUCCESS){

                JSONObject user = response.getJSONObject(ReqConst.RES_USERINFO);

                _user = new UserEntity();

                _user.set_idx(user.getInt(ReqConst.RES_ID));

                //Log.d("id======", String.valueOf(_user.get_idx()));

                _user.set_firstName(user.getString(ReqConst.RES_FIRSTNAME));
                _user.set_lastName(user.getString(ReqConst.RES_LASTNAME));
                _user.set_email(user.getString(ReqConst.RES_EMAIL));
                _user.set_password(ui_edtPassword.getText().toString());
                _user.set_phoneNumber(user.getString(ReqConst.RES_PHONE));
                _user.set_country(user.getString(ReqConst.RES_COUNTRY));
                _user.set_userName(ui_edtUsername.getText().toString().trim());
                _user.set_adress(user.getString(ReqConst.RES_ADDRES));
                _user.set_interests(user.getString(ReqConst.RES_INTERESTS));
                _user.set_status(user.getString(ReqConst.REQ_STATUS));
                _user.set_photoUrl(user.getString(ReqConst.RES_PHOTO_URL));
                _user.set_following(user.getInt(ReqConst.RES_FOLLOWING));
                _user.set_follower(user.getInt(ReqConst.RES_FOLLOWER));

                Preference.getInstance().put(this,
                        PrefConst.PREFKEY_USERNAME, ui_edtUsername.getText().toString().trim());

                //Preference.getInstance().put(this,PrefConst.PREFKEY_USEREMAIL, ui_edtUsername.getText().toString().trim());

                Preference.getInstance().put(this,
                        PrefConst.PREFKEY_USERPWD, ui_edtPassword.getText().toString().trim());

                Preference.getInstance().put(this,
                        PrefConst.PREFKEY_XMPPID, String.valueOf(_user.get_idx()));


                String lastLoginEmail = Preference.getInstance().getValue(getApplicationContext(), PrefConst.PREFKEY_LASTLOGINID, "");

                if (!lastLoginEmail.equals(_user.get_userName())) {
                    Preference.getInstance().put(getApplicationContext(), PrefConst.PREFKEY_LASTLOGINID, _user.get_userName());
                    Database.initDatabase();
                }

                Commons.g_user = _user ;

                loadRoomInfo();


                //gotoMain();

            } else if(result_code == ReqConst.CODE_UNREGISTER){

                closeProgress();

                showAlertDialog(getString(R.string.unregister_user));
                //Log.d("Failded ======>", valueOf(result_code));

            }else if (result_code == ReqConst.CODE_WRONGPWD) {

                closeProgress();
                showAlertDialog(getString(R.string.checkPwd));
                //Log.d("Failded ======>", valueOf(result_code));
            }

        } catch (JSONException e) {
            closeProgress();
            e.printStackTrace();

            showAlertDialog(getString(R.string.error));
        }

    }

    public void loadRoomInfo() {

        ArrayList<RoomEntity> roomEntities =  Database.getAllRoom();

        _reqCounter = roomEntities.size();

        Log.d("=====counter=====", String.valueOf(_reqCounter));

        if (_reqCounter > 0) {

            for (RoomEntity room : roomEntities) {
                getRoomInfo(room);
            }

        } else {
            loginToChattingServer();
        }
    }

    public void getRoomInfo(final RoomEntity room){

        String url = ReqConst.SERVER_URL + ReqConst.REQ_GETUSERINFO;

        String[] ids = room.get_name().split("_");

        int other = 0;

        for (int i = 0; i < ids.length; i++) {

            int idx = Integer.parseInt(ids[i]);
            if (idx != _user.get_idx()) {
                other = idx;
                break;
            }
        }

        String params = String.format("/%d", other);
        url += params;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String json) {

                parseRoomInfoResponse(json, room);
            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {
                _reqCounter--;

                if (_reqCounter <= 0)
                    loginToChattingServer();
            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT,
                0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        PicOneApplication.getInstance().addToRequestQueue(stringRequest, url);

    }


    public void parseRoomInfoResponse(String json, RoomEntity room){

        _reqCounter--;

        try {

            JSONObject response = new JSONObject(json);

            int result_code = response.getInt(ReqConst.RES_CODE);

            if (result_code == ReqConst.CODE_SUCCESS){

                Friendmodel friend = new Friendmodel();

                JSONObject user = response.getJSONObject(ReqConst.RES_USERINFO);

                friend.setId(user.getInt(ReqConst.RES_ID));
                friend.setFriendname(user.getString(ReqConst.RES_NAME));
                friend.setFriendphotourl(user.getString(ReqConst.RES_PHOTO_URL));

                room.get_participantList().add(friend);

                if (!Commons.g_user.get_roomList().contains(room))
                    Commons.g_user.get_roomList().add(room);
            }

        } catch (JSONException e){
            e.printStackTrace();
        }

        if (_reqCounter <= 0) {

            loginToChattingServer();
        }

    }

    public void loginToChattingServer() {

        if (ConnectionMgrService.mConnection == null || !ConnectionMgrService.mConnection.isConnected()) {

            Intent mServiceIntent = new Intent(LoginActivity.this, ConnectionMgrService.class);
            mServiceIntent.putExtra(Constants.XMPP_START, Constants.XMPP_FROMLOGIN);
            startService(mServiceIntent);

            Logger.d("XMPP", "not connected go to login from login activity");

        } else {

            gotoMain();

            Logger.d("XMPP", "already connected from broadcast");
        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.imv_back_sign:
                gotoMenuActivity();
                break;

            case R.id.txv_login_log:

                if (checkValue()){

                    _userName = ui_edtUsername.getText().toString().trim();
                    _password = ui_edtPassword.getText().toString().trim();

                    Preference.getInstance().put(this, PrefConst.PREFKEY_USERNAME, _userName);
                    Preference.getInstance().put(this, PrefConst.PREFKEY_USERPWD, _password);

                    _tokenId = Preference.getInstance().getValue(this, PrefConst.PREFKEY_TOKEN, "");

                    //Log.d("username====",name1+":"+pwd1);

                    processLogin();
                }
                break;

            case R.id.txv_forget:
                gotoForget();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {

            case REGISTER_CODE:

                if (resultCode == RESULT_OK) {

                    // load saved user
                    String name = Preference.getInstance().getValue(this,
                            PrefConst.PREFKEY_USERNAME, "");
                    String userpwd = Preference.getInstance().getValue(this,
                            PrefConst.PREFKEY_USERPWD, "");
                     _tokenId = Preference.getInstance().getValue(this ,
                            PrefConst.PREFKEY_TOKEN, "");

                    ui_edtUsername.setText(name);
                    ui_edtPassword.setText(userpwd);

                    processLogin();
                }
                break;
        }
    }

    public void onEventMainThread(LoggedInEvent event) {

        closeProgress();

        if(event.isSuccessful()) {
            gotoMain();
        } else {
            showAlertDialog(getString(R.string.error));
        }
    }



    private void gotoMenuActivity() {

        Intent intent = new Intent(this, MenuActivity.class);

        startActivity(intent);
        finish();
    }

    private void gotoMain() {

        closeProgress();

        Commons.g_isAppRunning = true;

        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra(Constants.KEY_FROMLOGIN, true);

        if (_room != null) {
            intent.putExtra(Constants.KEY_ROOM, _room);
        }
        startActivity(intent);
        finish();

    }

    private void gotoForget() {

        Intent intent = new Intent(this, ForgetActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {onExit();}


    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }
}
