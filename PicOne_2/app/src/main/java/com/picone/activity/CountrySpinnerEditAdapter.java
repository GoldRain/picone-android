package com.picone.activity;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.picone.R;

import java.util.Locale;

/**
 * Created by ToSuccess on 12/2/2016.
 */

public class CountrySpinnerEditAdapter extends BaseAdapter {

    MainActivity _activity;

    String[] country_name;

    public CountrySpinnerEditAdapter (MainActivity activity){

        this._activity = activity ;

        country_name = _activity.getResources().getStringArray(R.array.CountryCodes);
    }

    @Override
    public int getCount() {
        return country_name.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        LayoutInflater inflater =  (LayoutInflater)_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.item_country, parent, false);

        ImageView ui_imv_flag = (ImageView)convertView.findViewById(R.id.imv_flag);
        TextView ui_txv_name = (TextView)convertView.findViewById(R.id.txv_name);


        String[] g=country_name[position].split(",");
        String pngName = g[1].trim().toLowerCase();

        if(position==0){
            ui_txv_name.setText(g[0].trim());

        }else {
            ui_txv_name.setText(GetCountryZipCode(g[1]).trim());
            ui_imv_flag.setImageResource(_activity.getResources().getIdentifier("drawable/ic_flag_flat_" + pngName, null, _activity.getPackageName()));}




      /*  if(position==0){
           // ui_imv_flag.setImageResource();
        }else {
        }*/

        return convertView;
    }

    private String GetCountryZipCode(String ssid){

        Locale loc = new Locale("", ssid);
        return loc.getDisplayCountry().trim();
    }
}
