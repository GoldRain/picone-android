package com.picone.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.lib.recaptcha.ReCaptcha;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.picone.PicOneApplication;
import com.picone.R;
import com.picone.adapter.CountrySpinnerAdapter;
import com.picone.adapter.PhonenumberAdapter;
import com.picone.base.CommonActivity;
import com.picone.commons.Commons;
import com.picone.commons.Constants;
import com.picone.commons.ReqConst;
import com.picone.model.UserEntity;
import com.picone.preference.PrefConst;
import com.picone.preference.Preference;
import com.picone.utils.BitmapUtils;
import com.picone.utils.MultiPartRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import static com.picone.activity.MainActivity.MY_PEQUEST_CODE;

public class SingupActivity extends CommonActivity implements View.OnClickListener, ReCaptcha.OnShowChallengeListener, ReCaptcha.OnVerifyAnswerListener {

    ImageView ui_imvPhoto, ui_imvBack ;
    EditText ui_edtFistname, ui_edtLastname, ui_edtEmail,ui_edtUsername, ui_edtPwd,
            ui_edtConfirmpwd, ui_edtMobilenumber,ui_edtCountry;

    TextView ui_txvSingup;

    CountrySpinnerAdapter adapterCountry;
    Spinner ui_spinner_country_sign;

    PhonenumberAdapter adapterPhone;
    Spinner ui_spinner_phone;

    private Uri _imageCaptureUri;
    String _photoPath = "";

    String[] PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE,  Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};

    private int _idx = 0;

    /*Capcha variable Set*/
    private static final String PUBLIC_KEY  = "6LcPWugSAAAAAC-MP5sg6wp_CQiyxHvPvkQvVlVf";
    private static final String PRIVATE_KEY = "6LcPWugSAAAAALWMp-gg9QkykQQyO6ePBSUk-Hjg";

    private ReCaptcha   reCaptcha;
    private ProgressBar progress;
    private EditText    answer;
    private Button verify, reload ;

///////////////////////////////////////////////////////////////

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_singup);

        Commons.g_user = new UserEntity();
        loadLayout();
        showChallenge();
    }


    /*==================== CARMERA Permission========================================*/
    public static boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }
      /*////////////////////////////////////////////////////////////////*/
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        if (requestCode == MY_PEQUEST_CODE
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            //  gps functionality


        }
    }

    private void loadLayout() {

        ui_imvPhoto = (ImageView)findViewById(R.id.imv_photo_sign);
        ui_imvPhoto.setOnClickListener(this);
        ui_imvBack = (ImageView)findViewById(R.id.imv_back_sign);
        ui_imvBack.setOnClickListener(this);

        ui_edtFistname = (EditText)findViewById(R.id.edt_firstname_sign);
        ui_edtLastname = (EditText)findViewById(R.id.edt_lastname_sign);
        ui_edtUsername = (EditText)findViewById(R.id.edt_username_sign);
        ui_edtEmail = (EditText)findViewById(R.id.edt_email_sign);
        ui_edtPwd = (EditText)findViewById(R.id.edt_pwd_sign);
        ui_edtConfirmpwd = (EditText)findViewById(R.id.edt_confirmpwd_sign);
        ui_edtMobilenumber = (EditText)findViewById(R.id.edt_phone_sign);

        ui_txvSingup = (TextView) findViewById(R.id.txv_signup_sign);
        ui_txvSingup.setOnClickListener(this);

        ///////////////////////////Capcha Code/////////////////////////////
        reCaptcha = (ReCaptcha)findViewById(R.id.recaptcha);
        progress = (ProgressBar)findViewById(R.id.progress);
        answer = (EditText)findViewById(R.id.answer);

        verify = (Button)findViewById(R.id.verify);
        verify.setOnClickListener(this);

        reload = (Button)findViewById(R.id.reload);
        reload.setOnClickListener(this);
        ////////////////////////////////////////////////////////////////////

        adapterCountry = new CountrySpinnerAdapter(this);
        ui_spinner_country_sign = (Spinner)findViewById(R.id.spinner_country_sign);
        ui_spinner_country_sign.setAdapter(adapterCountry);

        adapterPhone = new PhonenumberAdapter(this);
        ui_spinner_phone = (Spinner)findViewById(R.id.spinner_phon_sign);
        ui_spinner_phone.setAdapter(adapterPhone);

        LinearLayout linearLayout = (LinearLayout)findViewById(R.id.lyt_container_sign);
        linearLayout.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                //TODO Auto-generated method stub
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(ui_edtUsername.getWindowToken(),0);
                return false;
            }
        });

    }

    public void selectPhoto() {

        final String[] items = {"Take photo", "Choose from Gallery","Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setItems(items, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    doTakePhoto();

                } else if(item == 1){
                    doTakeGallery();

                } else {
                    return;
                }
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    public void doTakePhoto(){

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        String picturePath = BitmapUtils.getTempFolderPath() + "photo_temp.jpg";
        _imageCaptureUri = Uri.fromFile(new File(picturePath));

        intent.putExtra(MediaStore.EXTRA_OUTPUT, _imageCaptureUri);
        startActivityForResult(intent, Constants.PICK_FROM_CAMERA);

    }

    private void doTakeGallery(){

        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(MediaStore.Images.Media.CONTENT_TYPE);
        startActivityForResult(intent, Constants.PICK_FROM_ALBUM);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data){

        switch (requestCode){

            case Constants.CROP_FROM_CAMERA: {

                if (resultCode == RESULT_OK){
                    try {

                        File saveFile = BitmapUtils.getOutputMediaFile(this);

                        InputStream in = getContentResolver().openInputStream(Uri.fromFile(saveFile));
                        BitmapFactory.Options bitOpt = new BitmapFactory.Options();
                        Bitmap bitmap = BitmapFactory.decodeStream(in, null, bitOpt);
                        in.close();

                        //set The bitmap data to image View
                        ui_imvPhoto.setImageBitmap(bitmap);
                        _photoPath = saveFile.getAbsolutePath();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
            }
            case Constants.PICK_FROM_ALBUM:

                if (resultCode == RESULT_OK){
                    _imageCaptureUri = data.getData();
                }

            case Constants.PICK_FROM_CAMERA:
            {
                try {

                    _photoPath = BitmapUtils.getRealPathFromURI(this, _imageCaptureUri);

                    Intent intent = new Intent("com.android.camera.action.CROP");
                    intent.setDataAndType(_imageCaptureUri, "image/*");

                    intent.putExtra("crop", true);
                    intent.putExtra("scale", true);
                    intent.putExtra("outputX", Constants.PROFILE_IMAGE_SIZE);
                    intent.putExtra("outputY", Constants.PROFILE_IMAGE_SIZE);
                    intent.putExtra("aspectX", 1);
                    intent.putExtra("aspectY", 1);
                    intent.putExtra("noFaceDetection", true);
                    //intent.putExtra("return-data", true);
                    intent.putExtra("output", Uri.fromFile(BitmapUtils.getOutputMediaFile(this)));

                    startActivityForResult(intent, Constants.CROP_FROM_CAMERA);
                }catch (Exception e){
                    e.printStackTrace();
                }
                break;
            }
        }
    }

    private boolean checkValue(){

        if (_photoPath.length() == 0){

            showAlertDialog(getString(R.string.select_photo));
            return false;
        }

        else if (ui_edtFistname.getText().length() == 0) {
            showAlertDialog(getString(R.string.enter_firstname));
            return false;
        }

        else if (ui_edtLastname.getText().length() == 0) {
            showAlertDialog(getString(R.string.enter_lastname));
            return false;
        }

        else if (ui_edtUsername.getText().length() == 0) {
            showAlertDialog(getString(R.string.enter_username));
            return false;

        } else if (ui_edtEmail.getText().toString().length() == 0
                && !ui_edtEmail.getText().toString().endsWith("@gmail.com")){

            showAlertDialog(getString(R.string.enter_email));
            return false;

        } else if (ui_edtPwd.getText().length() == 0) {
            showAlertDialog(getString(R.string.enter_pwd));
            return false;

        }else if (ui_edtConfirmpwd.getText().length() == 0) {
            showAlertDialog(getString(R.string.enter_confim_pwd));
            return false;

        } else if(!ui_edtPwd.getText().toString().equals(ui_edtConfirmpwd.getText().toString())) {

            showAlertDialog(getString(R.string.correct_pwd));
            return false ;

        } else if (ui_edtMobilenumber.getText().length() == 0) {
            showAlertDialog(getString(R.string.enter_mobilenumber));
            return false;
        }
        return true;
    }

    private void progressSignup() {

        final String countryname = Constants.countryname;

        Toast.makeText(_context, countryname, Toast.LENGTH_SHORT).show();

        showProgress();
        String url = ReqConst.SERVER_URL + ReqConst.REQ_SIGNUP ;
        StringRequest myRequest = new StringRequest(
                Request.Method.POST, url ,new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                Log.d("response=====", String.valueOf(response));
                parseSignupResponse(response);
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("error============", String.valueOf(error));
                        closeProgress();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                try {
                    params.put("username",ui_edtUsername.getText().toString());
                    params.put("firstname", ui_edtFistname.getText().toString());
                    params.put("lastname",ui_edtLastname.getText().toString());
                    params.put("phone", ui_edtMobilenumber.getText().toString());
                    params.put("password",ui_edtPwd.getText().toString());
                    params.put("email", ui_edtEmail.getText().toString());
                    params.put("country",countryname);

                } catch (Exception e) {}
                return params;
            }
        };

        myRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        PicOneApplication.getInstance().addToRequestQueue(myRequest,url);  // url or "tag"
    }

    private void parseSignupResponse(String response) {

        try {

            JSONObject object = new JSONObject(response);

            int result_code = object.getInt(ReqConst.RES_CODE);


            if (result_code == ReqConst.CODE_SUCCESS) {

               _idx = object.getInt(ReqConst.RES_ID);

                closeProgress();
                uploadImage();

            }else if(result_code == ReqConst.CODE_EXISTUSERNAME){

                showAlertDialog("Aleady exist username!");
                closeProgress();

            }else if (result_code == ReqConst.CODE_UNREGISTER){
                closeProgress();
               showAlertDialog("Unknown user. Please register again.!");

            }else if (result_code == ReqConst.CODE_WRONGPWD){
                closeProgress();
                showAlertDialog("Wrong Password");
            } else if (result_code == ReqConst.CODE_EXITEMAIL){
                closeProgress();
                showAlertDialog("Aleady exist email !");
            }

        } catch (JSONException e) {
            e.printStackTrace();
            showToast("Network Error. Please Try Again.");
            closeProgress();
        }

    }

    private void uploadImage() {

        try {
            File file = new File(_photoPath);

            Map<String, String> params = new HashMap<>();

            params.put(ReqConst.REQ_PARAM_ID , String.valueOf(_idx));

            String url = ReqConst.SERVER_URL + ReqConst.REQ_UPLOADPHOTO;

            MultiPartRequest reqMultiPart = new MultiPartRequest(url, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    closeProgress();
                    showAlertDialog(getString(R.string.photo_upload_fail));

                }

            }, new Response.Listener<String>() {
                @Override
                public void onResponse(String json) {

                    Log.d("success============********>>", _photoPath);

                    ParseUploadImageResponse(json);

                }

            },file, ReqConst.REQ_PARAM_FILE, params);

            reqMultiPart.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT ,0 ,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            PicOneApplication.getInstance().addToRequestQueue(reqMultiPart,url);

        } catch (Exception e){

            e.printStackTrace();
            closeProgress();
            showAlertDialog(getString(R.string.photo_upload_fail));

        }

    }

    private void ParseUploadImageResponse(String json) {

        Log.d("responseimage====",String.valueOf(json));

        closeProgress();

        try {

            JSONObject response = new JSONObject(json);
            int result_code = response.getInt(ReqConst.RES_CODE);

            if (result_code == ReqConst.CODE_SUCCESS){
                gotoLogin();

            }else if (result_code == ReqConst.CODE_UPLOADFAIL){
                showAlertDialog(getString(R.string.photo_upload_fail));
            }
        } catch (JSONException e) {
            e.printStackTrace();
            showAlertDialog(getString(R.string.photo_upload_fail));
        }

        //onSuccessSignup();
    }


    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.imv_back_sign:
                gotoMenu();
                /*finish();*/
                break;

            case R.id.imv_photo_sign:

            if (!hasPermissions(this, PERMISSIONS)){

                ActivityCompat.requestPermissions(this, PERMISSIONS, MY_PEQUEST_CODE);
            }else {
                selectPhoto();}

            break;

            case R.id.txv_signup_sign:
                if (checkValue()){
                    progressSignup();
                    /*gotoMain();*/
                }
                break;

            case R.id.verify:
                verifyAnswer();

                break;
            case R.id.reload:
                showChallenge();
        }
    }

    private void gotoLogin() {

        closeProgress();

        Log.d("photo+==============", _photoPath);

        Commons.g_user.set_photoUrl(_photoPath);

        Preference.getInstance().put(this, PrefConst.PREFKEY_USERNAME, ui_edtUsername.getText().toString().trim());

        Preference.getInstance().put(this, PrefConst.PREFKEY_USERPWD, ui_edtPwd.getText().toString().trim());

        Preference.getInstance().getValue(this, PrefConst.PREFKEY_USERNAME, "");

        setResult(RESULT_OK);
        gotoMenu();

    }

    public void gotoMenu(){

        Intent intent = new Intent(this , MenuActivity.class);
        overridePendingTransition(0,0);
        startActivity(intent);
        finish();
    }


  ///////////////////  Capcha code ///////////////////////////////
    @Override
    public void onChallengeShown(boolean shown) {

        progress.setVisibility(View.GONE);

        if (shown){

            /*If a CAPCHA is shown successfully, display it for the user enter the words*/
            reCaptcha.setVisibility(View.VISIBLE);

        } else {

            showToast(getString(R.string.show_error));
        }
    }

    @Override
    public void onAnswerVerified(boolean success) {

        if (success){
            showToast(getString(R.string.verification_success));
        }else {

            showToast(getString(R.string.verification_failed));
        }
        this.showChallenge();
    }

    private void showChallenge() {

        progress.setVisibility(View.VISIBLE);
        reCaptcha.setVisibility(View.GONE);

        reCaptcha.setLanguageCode("en");
        reCaptcha.showChallengeAsync(PUBLIC_KEY, this);
    }

    private void verifyAnswer(){

        if (TextUtils.isEmpty(answer.getText())){
            answer.setText("");
            showToast(getString(R.string.instruction));

        } else {
            progress.setVisibility(View.VISIBLE);
            reCaptcha.verifyAnswerAsync(PRIVATE_KEY, answer.getText().toString(), this);
            answer.setText("");
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////

    @Override
    public void onBackPressed() {
        onExit();
    }
}
