package com.picone;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;

import com.picone.activity.IntroActivity;
import com.picone.base.CommonActivity;
import com.picone.commons.Commons;
import com.picone.commons.Constants;

/**
 * Created by ToSuccess on 11/29/2016.
 */

public class RestartActivity extends CommonActivity implements View.OnClickListener {

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        if(!Commons.g_isAppRunning){

            String room = getIntent().getStringExtra(Constants.KEY_ROOM);

            Intent goIntro = new Intent(this, IntroActivity.class);

            if (room != null)
                goIntro.putExtra(Constants.KEY_ROOM, room);

            startActivity(goIntro);
        }

        finish();
    }

    @Override
    protected void onDestroy(){

        super.onDestroy();
    }

    @Override
    public void onClick(View view){

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event){

        return true;
    }
}
