package com.picone.chatting;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.picone.R;
import com.picone.activity.MainActivity;
import com.picone.commons.Commons;
import com.picone.commons.Constants;
import com.picone.fragment.ChatlistViewFragment;
import com.picone.fragment.ChattingFragment;
import com.picone.logger.Logger;
import com.picone.model.Friendmodel;
import com.picone.model.RoomEntity;
import com.picone.utils.RadiusImageView;

import java.util.ArrayList;

/**
 * Created by ToSuccess on 12/1/2016.
 */

public class ChatlistViewAdapter extends BaseAdapter {

    ChatlistViewFragment _fragment;
    MainActivity _activity;
    ArrayList<RoomEntity> _datas = new ArrayList<>();
    ArrayList<RoomEntity> _allDatas = new ArrayList<>();

    public ChatlistViewAdapter (ChatlistViewFragment fragment /*ChattingListActivity activity , ArrayList<RoomEntity> rooms*/){

        this._activity = (MainActivity)fragment.getActivity();
        _fragment = fragment;

        _allDatas = Commons.g_user.get_roomList();;
        _datas.addAll(_allDatas);

        Log.d("room count :", String.valueOf(_datas.size()));

    }

    public void refresh(){

        _datas.clear();
        _allDatas = Commons.g_user.get_roomList();
        _datas.addAll(_allDatas);

        Log.d("room count :", String.valueOf(_datas.size()));
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {return _datas.size();}

    @Override
    public Object getItem(int postion) {
        return _datas.get(postion);
    }

    @Override
    public long getItemId(int postion) {
        return postion;
    }

    @Override
    public View getView(int postion, View convertView, ViewGroup parent) {

        ChatlistViewHolder chatlistViewHolder ;

        if (convertView == null){

            chatlistViewHolder = new ChatlistViewHolder();

            LayoutInflater inflater = (LayoutInflater)_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_list_chatview, parent, false);

            chatlistViewHolder.ui_imvPhoto = (RadiusImageView) convertView.findViewById(R.id.imv_photo);
            chatlistViewHolder.ui_txvName = (TextView)convertView.findViewById(R.id.txv_name);
            chatlistViewHolder.ui_txvContent = (TextView)convertView.findViewById(R.id.txv_content);
            chatlistViewHolder.ui_txvDateHour = (TextView)convertView.findViewById(R.id.txv_date_hour);
            chatlistViewHolder.ui_txvChatNum = (TextView)convertView.findViewById(R.id.txv_chattingNum);

            convertView.setTag(chatlistViewHolder);

        } else {

            chatlistViewHolder = (ChatlistViewHolder)convertView.getTag();
        }

        final RoomEntity entity = _datas.get(postion);

        Friendmodel user = entity.get_participantList().get(0);

        chatlistViewHolder.ui_txvName.setText(user.getFriendname());
        chatlistViewHolder.ui_txvContent.setText(entity.get_recentMessage());
        chatlistViewHolder.ui_txvDateHour.setText(entity.get_recentTime());
        chatlistViewHolder.ui_txvChatNum.setText(String.valueOf(entity.get_recentCounter()));

        Log.d("Recent Message===========" , entity.get_recentMessage());


        if (user.getFriendphotourl().length() > 0) Glide.with(_fragment.getActivity()).load(user.getFriendphotourl()).placeholder(R.drawable.image_user).into(chatlistViewHolder.ui_imvPhoto);

        if (entity.get_recentCounter() == 0){

            chatlistViewHolder.ui_txvChatNum.setVisibility(View.GONE);
        } else {
            chatlistViewHolder.ui_txvChatNum.setVisibility(View.VISIBLE);
        }
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(_activity, ChattingActivity.class);
                intent.putExtra(Constants.KEY_ROOM, entity.get_name());
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                _activity.startActivity(intent);
            }
        });

        return convertView;
    }

    public class ChatlistViewHolder {

        RadiusImageView ui_imvPhoto;
        TextView ui_txvName;
        TextView ui_txvContent;
        TextView ui_txvDateHour;
        TextView ui_txvChatNum;
    }
}
