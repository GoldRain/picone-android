package com.picone.chatting;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.picone.PicOneApplication;
import com.picone.R;
import com.picone.RestartActivity;
import com.picone.activity.MainActivity;
import com.picone.commons.Commons;
import com.picone.commons.Constants;
import com.picone.commons.ReqConst;
import com.picone.fragment.ChatlistViewFragment;
import com.picone.fragment.ChattingFragment;
import com.picone.model.ChatEntity;
import com.picone.model.Database;
import com.picone.model.Friendmodel;
import com.picone.model.RoomEntity;
import com.picone.model.VoteEntity;

import org.jivesoftware.smack.chat.Chat;
import org.jivesoftware.smack.chat.ChatMessageListener;
import org.jivesoftware.smack.packet.Message;
import org.json.JSONException;
import org.json.JSONObject;

/*
* MyChatMessageListener directs the incoming messages to the appropriate container.
* In this case, messages are contained in the ChatList
* */
public class MyChatMessageListener implements ChatMessageListener {

    private int _senderIdx = 0;
    MainActivity _activity ;

    @Override
    public void processMessage(Chat chat, Message message) {

        String mChatSender = message.getFrom();
        _senderIdx = getIdx(mChatSender);

        String mChatMessage = message.getBody();

        // write message into db
        ChatEntity chatItem = new ChatEntity(_senderIdx, getRoomName(mChatMessage), mChatMessage);
        Database.createMessage(chatItem);

        // if app is running
        if (Commons.g_isAppRunning) {

            RoomEntity room = Commons.g_user.getRoom(getRoomName(mChatMessage));

            if (Commons.g_isAppPaused) {
                notifyNewMessage(mChatMessage);

            } else {

                if (Commons.g_currentActivity != null)
                    Commons.g_currentActivity.vibrate();
            }

            // if room not exist create room
            if (room == null) {
                // make room
                getRoomInfo(_senderIdx, mChatMessage);
            } else {

                room.set_recentMessage(getMessage(mChatMessage));
                room.set_recentTime(getDisplayTime(mChatMessage));
                room.add_rcentCounter();
                Database.updateRoom(room);

                if (Commons.g_currentActivity != null) {

                    // room recent info update
                    if (Commons.g_currentActivity.getClass().equals(MainActivity.class)) {  ///////
                        Commons.g_currentActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                ((MainActivity) Commons.g_currentActivity).refresh();
                            }
                        });
                    }
                    Commons.g_currentActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Commons.g_currentActivity.setUnRead();
                        }
                    });
                }
            }

        } else {
            notifyNewMessage(mChatMessage);
        }

    }


    public void getRoomInfo(final int idx, final String message){

        String url = ReqConst.SERVER_URL + ReqConst.REQ_GETUSERINFO;

        String params = String.format("/%d", idx);
        url += params;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {
                parseRoomInfoResponse(json, message);
            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT,
                0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        PicOneApplication.getInstance().addToRequestQueue(stringRequest, url);

    }

    public void parseRoomInfoResponse(String json, String message){

        try {

            JSONObject response = new JSONObject(json);

            int result_code = response.getInt(ReqConst.RES_CODE);

            if (result_code == ReqConst.CODE_SUCCESS){

                Friendmodel friend = new Friendmodel();

                JSONObject jsonVote = response.getJSONObject(ReqConst.RES_USERINFO);

                friend.setId(jsonVote.getInt("postid"));
                friend.setFriendphotourl(jsonVote.getString(ReqConst.RES_PHOTO_URL));
                friend.setFriendname(jsonVote.getString("name"));

                // make room
                RoomEntity roomEntity = new RoomEntity(getRoomName(message), getMessage(message) );
                roomEntity.get_participantList().add(friend);

                if (!Commons.g_user.get_roomList().contains(roomEntity)) {
                    Commons.g_user.get_roomList().add(roomEntity);
                    Database.createRoom(roomEntity);
                }

                if (Commons.g_currentActivity != null) {

                    // refresh chat room list

                    if (Commons.g_currentActivity.getClass().equals(MainActivity.class)) {
                        Commons.g_currentActivity.runOnUiThread(new Runnable() {

                            @Override
                            public void run() {

                                ((MainActivity)Commons.g_currentActivity).refresh();

                            }
                        });
                        Commons.g_currentActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Commons.g_currentActivity.setUnRead();
                            }
                        });
                    }
                }
            }

        } catch (JSONException e){
            e.printStackTrace();
        }

    }

    // ROOM#10_12_13:10_12_13_14:Who#this is room outside message body#time
    public String getRoomName(String message) {

        if (!message.startsWith(Constants.KEY_ROOM_MARKER))
            return null;

        return message.split(Constants.KEY_SEPERATOR)[1].split(":")[0];

    }

    // ROOM#10_12_13:10_12_13_14:Who#this is room outside message body#time
    public String getRoomParticipants(String message) {

        if (!message.startsWith(Constants.KEY_ROOM_MARKER))
            return null;

        return message.split(Constants.KEY_SEPERATOR)[1].split(":")[1];

    }

    // ROOM#10_12_13:10_12_13_14:Who#this is room outside message body#time
    public String getSenderName(String message) {

        if (!message.startsWith(Constants.KEY_ROOM_MARKER))
            return null;

        return message.split(Constants.KEY_SEPERATOR)[1].split(":")[2];

    }

    public String getDisplayTime(String body) {

        String fulldatetime = body.substring(body.lastIndexOf(Constants.KEY_SEPERATOR) + 1);

        String date = fulldatetime.split(",")[0];
        String fulltime = fulldatetime.split(",")[1];

        String time = fulltime.substring(0, fulltime.lastIndexOf(":"));

        int hour = Integer.valueOf(time.split(":")[0]);
        String min = time.split(":")[1];

        if (Commons.g_xmppService != null) {

            if (hour < 12) {
                time = time + " AM";
            } else {
                hour -= 12;
                if (hour == 0)
                    hour = 12;
                time = hour + ":" + min + " PM";
            }
        }

        return time;
    }

    // ROOM#1_2#message#time, ROOM#1_2#FILE#message#time
    public String getMessage(String body) {

        String body1 = body.substring(body.indexOf(Constants.KEY_SEPERATOR) + 1, body.lastIndexOf(Constants.KEY_SEPERATOR));
        String message = body1.substring(body1.indexOf(Constants.KEY_SEPERATOR) + 1);
        return message;
    }


    private void notifyNewMessage(String message) {

       if (Commons.g_xmppService != null)
            Commons.g_xmppService.updateBadgeCount(Commons.g_badgCount + 1);

        // create room
        RoomEntity room = new RoomEntity(getRoomName(message), getMessage(message),getDisplayTime(message),Commons.g_badgCount);

        if (Database.isExistRoom(room))
            Database.updateRoom(room);
        else
            Database.createRoom(room);

        Intent notiIntent = new Intent(Commons.g_xmppService, RestartActivity.class);
        notiIntent.putExtra(Constants.KEY_ROOM, getRoomName(message));
        notiIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent pendingIntent = PendingIntent.getActivity(Commons.g_xmppService, 1, notiIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        Notification.Builder builder = new Notification.Builder(Commons.g_xmppService);

        // noti small icon
        builder.setSmallIcon(R.drawable.app_logo);
        // noti preview
        builder.setTicker(getSenderName(message) + " : " + getMessage(message));
        // noti time
        builder.setWhen(System.currentTimeMillis());
        // noti title
        builder.setContentTitle(getTitleString());
        // content
        builder.setContentText(getSenderName(message) + " : " + getMessage(message));
        // action on touch
        builder.setContentIntent(pendingIntent);
        // auto cancel
        builder.setAutoCancel(true);

        builder.setOngoing(true);

        // large icon
        builder.setLargeIcon(BitmapFactory.decodeResource(Commons.g_xmppService.getResources(),
                R.drawable.app_logo));
        // sound
        builder.setDefaults(Notification.DEFAULT_SOUND);
        // vibration
        builder.setDefaults(Notification.DEFAULT_VIBRATE);

        // create noti
        NotificationManager nm = (NotificationManager) Commons.g_xmppService.getSystemService(Context.NOTIFICATION_SERVICE);
        nm.notify(Constants.NORMAL_NOTI_ID, builder.build());

    }

    public String getTitleString() {

        String ret = "[" + Commons.g_xmppService.getString(R.string.app_name) + "]";
        return ret;
    }

    public int getIdx(String name) {
        return Integer.valueOf(name.split("@")[0]);
    }

}
