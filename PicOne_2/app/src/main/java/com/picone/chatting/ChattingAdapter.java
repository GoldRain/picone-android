package com.picone.chatting;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.picone.R;
import com.picone.commons.Commons;
import com.picone.model.ChatEntity;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by ToSuccess on 11/30/2016.
 */

public class ChattingAdapter extends BaseAdapter {

    private static final int TYPE_CHAT = 0;
    private static final int TYPE_DATE = 1;
    ArrayList<Object> _datas = new ArrayList<>();
    private Date _lastDate = null ;

    ChattingActivity _activity;

    public ChattingAdapter(ChattingActivity activity){

        this._activity = activity ;
    }

    public void addItem(ChatEntity entity){

        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
        DateFormat outFormat = DateFormat.getDateInstance(DateFormat.LONG);

        String time = entity.get_time().split(",")[0];

        try {

            Date date = df.parse(time);
            if (_lastDate == null || _lastDate.before(date)) {

                String strDate = outFormat.format(date);
                _datas.add(strDate);
                _lastDate = date;
            }
        } catch (Exception ex) {
        }

        _datas.add(entity);

    }

    public void addItems(ArrayList<ChatEntity> items){

        for (ChatEntity item : items)
            addItem(item);
    }

    public void clearAll(){
        _datas.clear();
        _lastDate = null ;
    }
    @Override
    public int getCount() {return _datas.size();}

    @Override
    public Object getItem(int position) {return _datas.get(position);}

    @Override
    public long getItemId(int postion ){return postion;}

    @Override
    public int getViewTypeCount() {
        return 2;
    }


    @Override
    public int getItemViewType(int position) {
        if (getItem(position) instanceof ChatEntity)
            return TYPE_CHAT;

        return TYPE_DATE;
    }

    @Override
    public boolean isEnabled(int position) { return (getItemViewType(position) == TYPE_CHAT);}


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        int type = getItemViewType(position);

        switch (type ) {

            case TYPE_DATE: {

                DateHolder dateHolder;

                if (convertView == null) {

                    dateHolder = new ChattingAdapter.DateHolder();
                    LayoutInflater inflater = (LayoutInflater) _activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    convertView = inflater.inflate(R.layout.chatting_header, parent, false);

                    dateHolder.txvDate = (TextView) convertView.findViewById(R.id.txv_date);
                    convertView.setTag(dateHolder);


                } else {
                   dateHolder = (DateHolder) convertView.getTag();
                }

                String date = (String) _datas.get(position);
              dateHolder.txvDate.setText(date);
                }

            break;

            case TYPE_CHAT:{

                CustomHolder holder;

                if (convertView == null) {
                    holder = new CustomHolder();

                    LayoutInflater inflater = (LayoutInflater) _activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    convertView = inflater.inflate(R.layout.item_list_chatting, parent, false);

                    holder.txvOtherMsg = (TextView) convertView.findViewById(R.id.txv_othermsg);
                    holder.txvOtherTime = (TextView) convertView.findViewById(R.id.txv_othertime);
                    holder.txvMyMsg = (TextView) convertView.findViewById(R.id.txv_mymsg);
                    holder.txvMyTime = (TextView) convertView.findViewById(R.id.txv_mytime);


                    convertView.setTag(holder);

                } else {
                    holder = (CustomHolder) convertView.getTag();
                }

                holder.txvOtherMsg.setVisibility(View.GONE);
                holder.txvOtherTime.setVisibility(View.GONE);
                holder.txvMyMsg.setVisibility(View.GONE);
                holder.txvMyTime.setVisibility(View.GONE);

                ChatEntity chatEntity = (ChatEntity)_datas.get(position);

                // my message
                if (chatEntity.get_sender() == Commons.g_user.get_idx()) {

                    holder.txvMyMsg.setVisibility(View.VISIBLE);
                    holder.txvMyTime.setVisibility(View.VISIBLE);

                    holder.txvMyMsg.setText(chatEntity.get_message());
                    holder.txvMyTime.setText(chatEntity.getDisplayTime());

                } else {

                    holder.txvOtherMsg.setVisibility(View.VISIBLE);
                    holder.txvOtherTime.setVisibility(View.VISIBLE);

                    holder.txvOtherMsg.setText(chatEntity.get_message());
                    holder.txvOtherTime.setText(chatEntity.getDisplayTime());
                }

            }
            break;
        }

        return convertView;
    }

    public class CustomHolder {

        public TextView txvOtherMsg;
        public TextView txvOtherTime;
        public TextView txvMyMsg;
        public TextView txvMyTime;

    }

    public class DateHolder {

        public TextView txvDate;
    }
}
