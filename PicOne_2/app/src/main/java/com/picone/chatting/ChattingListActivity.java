package com.picone.chatting;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.picone.R;
import com.picone.activity.MainActivity;
import com.picone.base.CommonTabActivity;
import com.picone.commons.Commons;
import com.picone.commons.Constants;
import com.picone.model.RoomEntity;

import java.util.ArrayList;

public class ChattingListActivity extends CommonTabActivity implements View.OnClickListener {

    ListView ui_lisChatlist;
    ChatlistViewAdapter _adapter;
    ArrayList<RoomEntity> _rooms = new ArrayList<>();
    ImageView ui_imvBack, ui_imvMessage, ui_imvNoti;
    MainActivity _activity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chatting_list);

        loadLayout();
    }

    public void loadLayout() {

        ui_imvBack = (ImageView)findViewById(R.id.imv_back);
        ui_imvBack.setOnClickListener(this);

        ui_imvMessage = (ImageView)findViewById(R.id.imv_message);
        ui_imvMessage.setOnClickListener(this);

        ui_imvNoti = (ImageView)findViewById(R.id.imv_notify);
        ui_imvNoti.setOnClickListener(this);

        ui_txvUnread = (TextView)findViewById(R.id.txv_unread_message);

        setUnRead();

        ui_lisChatlist = (ListView)findViewById(R.id.lst_chat);

        _rooms = Commons.g_user.get_roomList();

        //_adapter = new ChatlistViewAdapter(this, _rooms);
        ui_lisChatlist.setAdapter(_adapter);
        _adapter.refresh();

    }

    public void refresh(){_adapter.refresh();}

    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.imv_back:
                finish();
                break;

            case R.id.imv_notify:

                break;

            case R.id.imv_message:

                break;
        }
    }

    @Override
    protected void onResume() {

        super.onResume();

        setUnRead();
        refresh();
    }
}
