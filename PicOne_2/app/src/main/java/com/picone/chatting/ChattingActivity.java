package com.picone.chatting;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.baoyz.widget.PullRefreshLayout;
import com.picone.PicOneApplication;
import com.picone.R;
import com.picone.activity.MainActivity;
import com.picone.base.CommonActivity;
import com.picone.commons.Commons;
import com.picone.commons.Constants;
import com.picone.commons.ReqConst;
import com.picone.logger.Logger;
import com.picone.model.ChatEntity;
import com.picone.model.Database;
import com.picone.model.Friendmodel;
import com.picone.model.RoomEntity;
import com.picone.model.UserEntity;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.chat.Chat;
import org.jivesoftware.smack.chat.ChatManager;
import org.jivesoftware.smack.packet.Message;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;


public class ChattingActivity extends CommonActivity implements View.OnClickListener {

    ChattingAdapter _adapter;
    ListView ui_lstChatting;
    EditText ui_edtMessage;
    PullRefreshLayout ui_pullRefreshLayout;
    TextView ui_txvRoomName;
    ImageView ui_imvNoti, ui_imvMessage;
    ArrayList<Object> object ;

    MainActivity _activity;

    int _recentLoadCounter = 1;

    UserEntity _user ;
    RoomEntity _roomEntity = null ;
    MyChatManager _groupChat = null ;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chatting);

        _user = Commons.g_user;

        Commons.g_chattingActivity = this ;
        //Constants.chat_status = 1;

        String roomname = getIntent().getStringExtra(Constants.KEY_ROOM);
        _roomEntity = _user.getRoom(roomname);

        _groupChat = new MyChatManager(this, ConnectionMgrService.mConnection, _roomEntity.get_name());
        updateBadgeCount(Commons.g_badgCount - _roomEntity.get_recentCounter());
        _roomEntity.init_recentCounter();
        enterRoom();

        loadLayout();

//     if (Constants.noti_counter > 0){
    }

    public void enterRoom() {

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                // We send the message here.
                // You should also check if the username is valid here.
                try {
                    _groupChat.enterRoom(String.valueOf(_user.get_idx()));

                } catch (Exception e) {
                    showToast(getString(R.string.chatting_error));
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);

                if (!_groupChat.isJoined) {
                    enterRoomRetry();
                    showToast(getString(R.string.chatting_error));
                } else {
                    showToast(getString(R.string.chatting_success));
                }
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public void enterRoomRetry() {

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                // We send the message here.
                // You should also check if the username is valid here.
                try {
                    _groupChat.enterRoom(String.valueOf(_user.get_idx()));
                } catch (Exception e) {
                    showToast(getString(R.string.chatting_error));
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);

                if (!_groupChat.isJoined)
                    showToast(getString(R.string.chatting_error));
                else {
                    showToast(getString(R.string.chatting_success));
                }
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public void reenterRoom() {

        try {
            _groupChat.reenterRoom(String.valueOf(_user.get_idx()));
        } catch (Exception e) {
            showToast(getString(R.string.chatting_error));
        }
    }

    public void checkConnection() {

        if (Commons.g_xmppService == null || !Commons.g_xmppService.isConnected || !_groupChat.isJoined) {
            showToast(getString(R.string.chatting_connecting));
        }

//        if (Constants.noti_counter > 0){
//
//            _activity.showNoti(View.VISIBLE, Constants.noti_counter);
//        }else {
//
//            _activity.showNoti(View.GONE,0);
//        }
    }

    public void loadLayout() {

        ImageView imvBack = (ImageView)findViewById(R.id.imv_back);
        imvBack.setOnClickListener(this);

        ui_imvNoti = (ImageView)findViewById(R.id.imv_notify);
        ui_imvNoti.setOnClickListener(this);

        ui_imvMessage = (ImageView)findViewById(R.id.imv_message);
        ui_imvMessage.setOnClickListener(this);

        ImageView imvSend = (ImageView) findViewById(R.id.imv_send);
        imvSend.setOnClickListener(this);

        ui_edtMessage = (EditText)findViewById(R.id.edt_message);
        ui_txvRoomName = (TextView)findViewById(R.id.txv_roomname_chat);
        ui_txvRoomName.setText(_roomEntity.get_displayName());

        ui_lstChatting = (ListView)findViewById(R.id.lst_chatting);

        _adapter = new ChattingAdapter(this);
        ui_lstChatting.setAdapter(_adapter);

        ui_pullRefreshLayout = (PullRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        ui_pullRefreshLayout.setRefreshStyle(PullRefreshLayout.STYLE_RING);
        ui_pullRefreshLayout.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                _recentLoadCounter++;
                ui_lstChatting.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_NORMAL);
                startRefreshThread();

            }

        });


        new Thread(new Runnable() {
            @Override
            public void run() {
                getFirstChattingList();
            }
        }).start();
    }

    public void getFirstChattingList() {

        final ArrayList<ChatEntity> recents = Database.getRecentMessage(_roomEntity.get_name(), _recentLoadCounter);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                _adapter.addItems(recents);
                _adapter.notifyDataSetChanged();
            }
        });
    }

    public void startRefreshThread() {

        new Thread(new Runnable() {
            @Override
            public void run() {
                getChattingList();
            }
        }).start();
    }

    public void getChattingList() {

        final ArrayList<ChatEntity> recents = Database.getRecentMessage(_roomEntity.get_name(), _recentLoadCounter);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                _adapter.clearAll();
                _adapter.addItems(recents);
                _adapter.notifyDataSetChanged();
                ui_pullRefreshLayout.setRefreshing(false);
             //   ui_lstChatting.smoothScrollToPosition(0);
            }
        });
    }

    public Friendmodel other() {

        return _roomEntity.get_participantList_().get(0);
    }

    public void addChat(int sender, String body) {

        //checkFriendBar();

        ui_lstChatting.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);

        ChatEntity chatItem = new ChatEntity(sender, _roomEntity.get_name(), body);

        _adapter.addItem(chatItem);
        _adapter.notifyDataSetChanged();

    }


    public ArrayList<Integer> getParticipants() {

        ArrayList<Integer> returned = new ArrayList<>();

        for (Friendmodel friend : _roomEntity.get_participantList()) {

            int idx = friend.getId();

            if (idx != _user.get_idx())
                returned.add(Integer.valueOf(idx));
        }

        return returned;
    }

    // basic send message
    // ROOM#[roomname]:[roomparticipants]:[sendername]#message#time
    // ROOM#1_2:1_2_3:에스오#message#time, ROOM#1_2:1_2_3:에스오#FILE#url#filename#time
    public void sendTextMessage(final String chat_message) {

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                // We send the message here.
                // You should also check if the username is valid here.
                try {

                    Logger.d("TEST", "send text doinbackground:" + chat_message);

                    // send both group message and message for outside user
                    String fullMessage = getRoomInfoString() + chat_message + Constants.KEY_SEPERATOR + getTimeString();
                    // send group message for inside user
                    _groupChat.sendMessage(fullMessage);

                    // update room recent conversation
                    _roomEntity.set_recentMessage(getMessage(fullMessage));
                    Database.updateRoom(_roomEntity);

                    // write message into db
                    ChatEntity chatItem = new ChatEntity(_user.get_idx(), _roomEntity.get_name(), fullMessage);
                    Database.createMessage(chatItem);

                    // send message for outside user
                    sendTextMessage(getParticipants(), fullMessage);

                } catch (SmackException.NotConnectedException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                String fullMessage = getRoomInfoString() + chat_message + Constants.KEY_SEPERATOR + getTimeString();
                addChat(_user.get_idx(), fullMessage);
                super.onPostExecute(aVoid);
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    // send message for outside users
    public void sendTextMessage(ArrayList<Integer> participants, String chat_message) {

        for (Integer participant : participants) {

            int idx = participant.intValue();
            sendTextMessage(Commons.idxToAddr(idx), chat_message);
        }
    }

    // send message for outside individual user
    public void sendTextMessage(String address, String chat_message) {
        // Listview is updated with our new message
        ChatManager chatManager = ChatManager.getInstanceFor(ConnectionMgrService.mConnection);

        final Chat newChat = chatManager.createChat(address);

        final Message message = new Message();
        message.setBody(chat_message);

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                // We send the message here.
                // You should also check if the username is valid here.
                try {
                    newChat.sendMessage(message);
                } catch (SmackException.NotConnectedException e) {
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }



    public String getRoomInfoString() {

        return Constants.KEY_ROOM_MARKER + _roomEntity.get_name() + ":" + _roomEntity.get_name() + ":" + _user.get_fullName() + Constants.KEY_SEPERATOR;
    }

    // 20150101,13:30:26 or 20160103,6:07:06
    public String getTimeString() {

        Calendar now = Calendar.getInstance();

        int year = now.get(Calendar.YEAR);
        int month = now.get(Calendar.MONTH) + 1;
        int date = now.get(Calendar.DATE);

        String time = String.format("%d%02d%02d", year, month, date);

        int hour = now.get(Calendar.HOUR_OF_DAY);
        int min = now.get(Calendar.MINUTE);
        int sec = now.get(Calendar.SECOND);

        time += String.format(",%d:%02d:%02d", hour, min, sec);

        return time;
    }

    // ROOM#1_2#message#time, ROOM#1_2#FILE#message#time
    public String getMessage(String body) {

        String body1 = body.substring(body.indexOf(Constants.KEY_SEPERATOR) + 1, body.lastIndexOf(Constants.KEY_SEPERATOR));
        String message = body1.substring(body1.indexOf(Constants.KEY_SEPERATOR) + 1);

        return message;
    }

    public void onExit() {

        if (_adapter.getCount() > 0) {

            ChatEntity lastItem = (ChatEntity) _adapter.getItem(_adapter.getCount() - 1);

            String recentMsg = lastItem.get_message();
            String recentTime = lastItem.getDisplayTime();

            _roomEntity.init_recentCounter();
            _roomEntity.set_recentContent(recentMsg);
            _roomEntity.set_recentTime(recentTime);
            Database.updateRoom(_roomEntity);
        }

        finish();
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.imv_back:
                onExit();

                break;

            case R.id.imv_send:

                ui_lstChatting.setAdapter(_adapter);
                ui_lstChatting.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_NORMAL);
                _adapter.notifyDataSetChanged();

                if (ui_edtMessage.length() > 0) {

                    if (_groupChat.isJoined) {
                        sendTextMessage(ui_edtMessage.getText().toString());
                        ui_edtMessage.setText("");

                    } else {
                        showToast(getString(R.string.chatting_error));
                    }
                }


                break;

            case R.id.imv_notify:

                break;
            case R.id.imv_message:
                break;

            /*case R.id.txv_addFriend:
                addFriend();
                break;*/
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK) {
            onExit();
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

       /* _groupChat.leaveRoom();*/
        Commons.g_chattingActivity = null;
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkConnection();
    }
}
