package com.picone.base;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;

import com.picone.commons.Commons;
import com.picone.model.RoomEntity;
import com.picone.model.UserEntity;

/**
 * Created by ToSuccess on 11/29/2016.
 */

public class CommonTabActivity extends CommonActivity{

    protected TextView ui_txvUnread;
    protected UserEntity _user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        _user = Commons.g_user;
        Commons.g_currentActivity = this;

    }

    public void setUnRead() {

        int unread = 0;

        for (RoomEntity room :_user.get_roomList()) {
            unread += room.get_recentCounter();
        }
        if (unread == 0) {

            ui_txvUnread.setVisibility(View.INVISIBLE);

        } else {

            ui_txvUnread.setVisibility(View.VISIBLE);
            ui_txvUnread.setText(String.valueOf(unread));
        }

        updateBadgeCount(unread);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK) {

            onExit();
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }
}
