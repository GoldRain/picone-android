package com.picone.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by ToSuccess on 12/2/2016.
 */

public class VoteEntity implements Serializable{

    int id = 0;
    int _userID = 0;
    String _userphotoUrl = "";
    String _username = "";
    String _title = "";
    String _votePhotoUrl_L = "";
    String _votePhotoUrl_R = "";

    int _votePercentL = 0;
    int _votePercentR = 0;
    int type = 0;    /*// circle = 0,  rec =1*/
    String _description = "";
    String _time = "";
    String _date = "";
    int _friendCount = 0;
    boolean _isSelected = false;
    String _interests = "";
    String _status = "" ;  //text edit status:
    String _address = "";
    int _private_status = 1; // 1: public, 0:private//
    int _following = 0;
    int _follwer = 0;
    int _relation = 0; // 0 : no relation, 1: relation // following relation //
    int _friend_status = -1; // 0: no relation, 1 ; request sent, 2: request received, 3: friend:(Request send)

    ArrayList<VoteEntity> _friendList = new ArrayList<>();
    ArrayList<RoomEntity> _roomList = new ArrayList<>();

    public int get_userID() {return _userID;}

    public void set_userID(int _userID) {this._userID = _userID;}

    public int get_private_status() {return _private_status;}

    public void set_private_status(int _private_status) {this._private_status = _private_status;}

    public String get_interests() {
        return _interests;
    }

    public void set_interests(String _interests) {
        this._interests = _interests;
    }

    public String get_status() {
        return _status;
    }

    public void set_status(String _status) {
        this._status = _status;
    }

    public String get_address() {
        return _address;
    }

    public void set_address(String _address) {this._address = _address;}

    public boolean is_isSelected() {
        return _isSelected;
    }

    public void set_isSelected(boolean _isSelected) {
        this._isSelected = _isSelected;
    }


    public int getType() {return type;}

    public void setType(int type) {
        this.type = type;
    }

    public int getId() {return id;}

    public void setId(int id) {this.id = id;}

    public String get_userphotoUrl() {return _userphotoUrl;}

    public void set_userphotoUrl(String _userphotoUrl) {this._userphotoUrl = _userphotoUrl;}

    public String get_username() {return _username;}

    public void set_username(String _username) {this._username = _username;}

    public String get_title() {return _title;}

    public void set_title(String _title) {this._title = _title;}

    public String get_votePhotoUrl_L() {return _votePhotoUrl_L;}

    public void set_votePhotoUrl_L(String _votePhotoUrl_L) {this._votePhotoUrl_L = _votePhotoUrl_L;}

    public String get_votePhotoUrl_R() {return _votePhotoUrl_R;}

    public void set_votePhotoUrl_R(String _votePhotoUrl_R) {this._votePhotoUrl_R = _votePhotoUrl_R;}

    public int get_votePercentL() {return _votePercentL;}

    public void set_votePercentL(int _votePercentL) {this._votePercentL = _votePercentL;}

    public int get_votePercentR() {return _votePercentR;}

    public void set_votePercentR(int _votePercentR) {this._votePercentR = _votePercentR;}

    public String get_description() {
        return _description;
    }

    public void set_description(String _description) {
        this._description = _description;
    }

    public String get_time() {
        return _time;
    }

    public void set_time(String _time) {
        this._time = _time;
    }

    public String get_date() {
        return _date;
    }

    public void set_date(String _date) {
        this._date = _date;
    }

    public int get_friendCount() {
        return _friendCount;
    }

    public void set_friendCount(int _friendCount) {
        this._friendCount = _friendCount;
    }

    public int get_following() {return _following;}

    public void set_following(int _following) {this._following = _following;}

    public int get_follwer() {return _follwer;}

    public void set_follwer(int _follwer) {this._follwer = _follwer;}

    public int get_relation() {return _relation;}

    public void set_relation(int _relation) {this._relation = _relation;}

    public int get_friend_status() {
        return _friend_status;
    }

    public void set_friend_status(int _friend_status) {
        this._friend_status = _friend_status;
    }

    public ArrayList<VoteEntity> get_friendList() {
        return _friendList;
    }

    public void set_friendList(ArrayList<VoteEntity> _friendList) {
        this._friendList = _friendList;
    }

    public ArrayList<RoomEntity> get_roomList() {
        return _roomList;
    }

    public void set_roomList(ArrayList<RoomEntity> _roomList) {
        this._roomList = _roomList;
    }

    public boolean isFriend(int idx) {

        for (VoteEntity friend : _friendList) {

            if (friend.getId() == idx)
                return true;
        }

        return false;
    }

    public void addFriend(VoteEntity other) {

        if (!_friendList.contains(other)) {
            _friendList.add(other);
            _friendCount++;
        }
    }

    public void deleteFriend(VoteEntity other) {

        if (_friendList.contains(other)) {
            _friendList.remove(other);
            _friendCount--;
        }
    }

    public RoomEntity getRoom(String roomName) {

        for (RoomEntity room : _roomList) {
            if (room.get_name().equals(roomName))
                return room;
        }

        return null;
    }

}
