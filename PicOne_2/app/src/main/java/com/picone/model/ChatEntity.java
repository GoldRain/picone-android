package com.picone.model;

import com.picone.commons.Constants;

/**
 * Created by ToSuccess on 11/30/2016.
 */

public class ChatEntity {
    private int _sender = 0;
    private String _message = "";
    private String _time = "";
    private String _roomname = "";

    public ChatEntity() {

    }

    public ChatEntity(int sender, String roomname, String body) {

        this._sender = sender;
        this._roomname = roomname;
        this._message = getMessage(body);
        this._time = getTime(body);
    }

    public ChatEntity(int sender, String roomname, String msg, String time) {

        _sender = sender;
        _roomname = roomname;
        _message = msg;
        _time = time;
    }

    public String get_roomname() {
        return _roomname;
    }

    public void set_roomname(String _roomname) {
        this._roomname = _roomname;
    }

    public String get_message() {
        return _message;
    }

    public void set_message(String _message) {
        this._message = _message;
    }

    public int get_sender() {
        return _sender;
    }

    public void set_sender(int _sender) {
        this._sender = _sender;
    }

    public String get_time() {
        return _time;
    }

    public void set_time(String _time) {
        this._time = _time;
    }

    // 20160103,6:07:06
    public String getDisplayTime() {

        String fulltime = _time.split(",")[1];

        String time = fulltime.substring(0, fulltime.lastIndexOf(":"));

        int hour = Integer.valueOf(time.split(":")[0]);
        String min = time.split(":")[1];

        if (hour < 12) {
            time = time + " AM";
        } else {
            hour -= 12;
            if (hour == 0)
                hour = 12;
            time = hour + ":" + min + " PM";
        }

        return time;
    }

    // ROOM#[roomname]:[roomparticipants]:[sendername]#message#time
    // ROOM#1_2:1_2_3:Alex#message#time, ROOM#1_2:1_2_3:에스오#FILE#url#filename#time
    public String getMessage(String body) {

        String body1 = body.substring(body.indexOf(Constants.KEY_SEPERATOR) + 1, body.lastIndexOf(Constants.KEY_SEPERATOR));
        String message = body1.substring(body1.indexOf(Constants.KEY_SEPERATOR) + 1);

        return message;
    }

    public String getTime(String body) {

        String time = body.substring(body.lastIndexOf(Constants.KEY_SEPERATOR) + 1);
        return time;
    }

}
