package com.picone.model;

/**
 * Created by ToSuccess on 12/15/2016.
 */

public class NotiModel {
    int id;  // friendid
    String noti_photo;
    int noti_type = 0 ;
    String userName;

    public String getUsername() {
        return userName;
    }

    public void setUsername(String userName) {
        this.userName = userName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNoti_photo() {
        return noti_photo;
    }

    public void setNoti_photo(String noti_photo) {
        this.noti_photo = noti_photo;
    }

    public int getNoti_type() {
        return noti_type;
    }

    public void setNoti_type(int noti_type) {
        this.noti_type = noti_type;
    }
}
