package com.picone.model;

import java.io.Serializable;

/**
 * Created by ToSuccess on 12/9/2016.
 */

public class Friendmodel implements Serializable {

    String friendphotourl;
    String friendname;
    int id;
    int followstatuse = 0; // 0 : unfollowwing, 1: following


    public String getFriendphotourl() {
        return friendphotourl;
    }

    public void setFriendphotourl(String friendphotourl) {
        this.friendphotourl = friendphotourl;
    }

    public String getFriendname() {
        return friendname;
    }

    public void setFriendname(String friendname) {
        this.friendname = friendname;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int isCheckstatuse() {return followstatuse;}

    public void setCheckstatuse(int checkstatuse) {
        this.followstatuse = checkstatuse;
    }

    public int getFollowstatuse() {return followstatuse;
    }

    public void setFollowstatuse(int followstatuse) {this.followstatuse = followstatuse;
    }
}
