package com.picone.model;

import com.picone.commons.Commons;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by ToSuccess on 12/13/2016.
 */

public class RoomEntity implements Serializable {

    String _name = "";
    String _participants = "";          // contains my id
    ArrayList<Friendmodel> _participantList = new ArrayList<>();


    String _recentMessage = "";
    String _recentTime = "";
    String _recentContent = "";
    int _recentCounter = 0 ;

    private boolean _isSelected = false ;

    public RoomEntity(){}

    public RoomEntity(String roomname, String recentMsg){

        _name = roomname ;
        _recentMessage = recentMsg ;
    }

    public RoomEntity(ArrayList<Friendmodel> participants){

        _participantList = participants ;
        _name = makeRoomName();
        _recentMessage = "";
    }

    public RoomEntity(String name,  String recentContent, String recentTime, int recentCounter) {

        _name = name;
        _recentContent = recentContent;
        _recentTime = recentTime;
        _recentCounter = recentCounter;

    }


    public ArrayList<Friendmodel> get_participantList_() {
        return _participantList;
    }

    public void set_participantList_(ArrayList<Friendmodel> _participantList_) {
        this._participantList = _participantList_;
    }

    public ArrayList<Friendmodel> get_participantList() {
        return _participantList;
    }

    public void set_participantList(ArrayList<Friendmodel> _participantList) {
        this._participantList = _participantList;
    }

    public String get_name() {
        return _name;
    }

    public void set_name(String _name) {
        this._name = _name;
    }

    public String get_recentMessage() {return _recentMessage;}

    public void set_recentMessage(String _recentMessage) {
        this._recentMessage = _recentMessage;
    }

    public String get_recentTime() {
        return _recentTime;
    }

    public void set_recentTime(String _recentTime) {
        this._recentTime = _recentTime;
    }

    public String get_recentContent() {return _recentContent;}

    public void set_recentContent(String _recentContent) {this._recentContent = _recentContent;}

    public int get_recentCounter() {return _recentCounter;}

    public void set_recentCounter(int _recentCounter) {this._recentCounter = _recentCounter;}

    public void add_rcentCounter() {
        set_recentCounter(_recentCounter + 1);
    }

    public void init_recentCounter() {
        set_recentCounter(0);
    }

    public boolean is_isSelected() {return _isSelected;}

    public void set_isSelected(boolean _isSelected) {this._isSelected = _isSelected;}

    public String makeRoomName (){

        String roomName = "";
        ArrayList<Integer> ids = new ArrayList<>();
        for (Friendmodel entity : _participantList){

            ids.add(Integer.valueOf(entity.getId()));
        }
        ids.add(Integer.valueOf(Commons.g_user.get_idx()));

        Collections.sort(ids);

        for (Integer id : ids){

            roomName += id + "_";
        }

        roomName = roomName.substring(0, roomName.length() - 1);

        return roomName ;
    }

    public String get_displayName(){

        String displayName = "";
        ArrayList<Integer> ids = new ArrayList<>();

        for (Friendmodel entity: _participantList){

            ids.add(Integer.valueOf(entity.getId()));
        }

        Collections.sort(ids);

        for (Integer id :ids){

            for (Friendmodel entity : _participantList){

                if (id == entity.getId()){

                    displayName += entity.getFriendname() + ",";
                    break;
                }
            }
        }

        if (displayName.length()> 2){
            displayName = displayName.substring(0, displayName.length() - 1);
        }

        return displayName ;
    }

    public String get_displayCount() {

        String displayCount = "";

        if (_participantList.size() >= 2)

            displayCount += " (" + String.valueOf(_participantList.size()+ 1) + ")";

            return displayCount ;

    }

    public Friendmodel getParcipant(int idx){

        for (Friendmodel friend : _participantList) {

            if (friend.getId() == idx){
                return friend ;
            }
        }

        return null ;
    }

    @Override
    public boolean equals(Object obj) {

        RoomEntity other = (RoomEntity) obj;
        return (get_name().equalsIgnoreCase(other.get_name()));
    }
}
