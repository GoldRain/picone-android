package com.picone.model;

import java.util.ArrayList;

/**
 * Created by ToSuccess on 11/29/2016.
 */

public class UserEntity {
    private int _idx = 0;
    private int _postId = 0;
    private String _firstName = "";
    private String _lastName = "";
    private String _userName = "";
    private String _email = "";
    private String _password = "";
    private String _label = "";
    private String _bgUrl = "";
    private String _photoUrl = "";
    private String _phoneNumber = "";
    private String _country = "";

    String _adress = "";
    String _interests = "";
    String _status = "";
    String _phone = "";
    int _following = 0;
    int _follower = 0;
    int _relation = 0;

    int _friendCount = 0;

    ArrayList<VoteEntity> _voteList = new ArrayList<>();
    ArrayList<RoomEntity> _roomList = new ArrayList<>();

    ArrayList<UserEntity> _friendList = new ArrayList<>();

    private int _allowFriend = 1;

    public int get_idx() {
        return _idx;
    }

    public void set_idx(int _idx) {
        this._idx = _idx;
    }

    public String get_firstName() {return _firstName;}

    public void set_firstName(String _firstName) {this._firstName = _firstName;}

    public String get_lastName() {return _lastName;}

    public void set_lastName(String _lastName) {this._lastName = _lastName;}

    public String get_userName() {
        return _userName;
    }

    public void set_userName(String _userName) {
        this._userName = _userName;
    }

    public String get_fullName() {

        return _firstName + " " + _lastName;
    }

    public String get_email() {
        return _email;
    }

    public void set_email(String _email) {
        this._email = _email;
    }

    public String get_password() {
        return _password;
    }

    public void set_password(String _password) {
        this._password = _password;
    }

    public String get_label() {
        return _label;
    }

    public void set_label(String _label) {
        this._label = _label;
    }

    public String get_bgUrl() {
        return _bgUrl;
    }

    public void set_bgUrl(String _bgUrl) {
        this._bgUrl = _bgUrl;
    }

    public String get_photoUrl() {
        return _photoUrl;
    }

    public void set_photoUrl(String _photoUrl) {this._photoUrl = _photoUrl;}

    public String get_phoneNumber() {
        return _phoneNumber;
    }

    public void set_phoneNumber(String _phoneNumber) {this._phoneNumber = _phoneNumber;}

    public int get_allowFriend() {
        return _allowFriend;
    }

    public void set_allowFriend(int _allowFriend) {
        this._allowFriend = _allowFriend;
    }

    public String get_country() {return _country;}

    public void set_country(String _country) {this._country = _country;}

    public String get_adress() {return _adress;}

    public void set_adress(String _adress) {this._adress = _adress;}

    public String get_interests() {return _interests;}

    public void set_interests(String _interests) {this._interests = _interests;}

    public String get_status() {return _status;}

    public void set_status(String _status) {this._status = _status;}

    public String get_phone() {return _phone;}

    public void set_phone(String _phone) {this._phone = _phone;}

    public int get_following() {return _following;}

    public void set_following(int _following) {this._following = _following;}

    public int get_follower() {
        return _follower;
    }

    public void set_follower(int _follower) {
        this._follower = _follower;
    }

    public int get_postId() {return _postId;}

    public void set_postId(int _postId) {this._postId = _postId;}

    public int get_relation() { return _relation; }

    public void set_relation(int _relation) {this._relation = _relation;}

    public ArrayList<VoteEntity> get_voteList() {
        return _voteList;
    }

    public void set_voteList(ArrayList<VoteEntity> _voteList) {
        this._voteList = _voteList;
    }

    public ArrayList<RoomEntity> get_roomList() {return _roomList;}

    public RoomEntity getRoom(String roomName) {

        for (RoomEntity room : _roomList){
            if (room.get_name().equals(roomName))
                return room;
        }

        return null;
    }

    public boolean isFriend(int idx) {

        for (UserEntity friend : _friendList) {

            if (friend.get_idx() == idx)
                return true;
        }

        return false;
    }

    public void addFriend(UserEntity other) {

        if (!_friendList.contains(other)) {
            _friendList.add(other);
            _friendCount++;
        }
    }

    public void deleteFriend(UserEntity other) {

        if (_friendList.contains(other)) {
            _friendList.remove(other);
            _friendCount--;
        }
    }

    public void set_roomList(ArrayList<RoomEntity> _roomList) {
        this._roomList = _roomList;
    }
}
