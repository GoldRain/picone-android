package com.picone.commons;

import android.graphics.Bitmap;

import com.picone.model.NotiModel;
import com.picone.model.VoteEntity;

import java.util.ArrayList;

/**
 * Created by ToSuccess on 11/29/2016.
 */

public class Constants {

    public static final int VOLLEY_TIME_OUT = 60000;

    public static final int LIMIT_FILE = 25 * 1024 * 1024;

    public static final int PROFILE_IMAGE_SIZE = 256;

    public static final int RECENT_MESSAGE_COUNT = 20;

    public static final int SPLASH_TIME = 2000;
    public static final int PICK_FROM_CAMERA = 100;
    public static final int PICK_FROM_ALBUM = 101;
    public static final int CROP_FROM_CAMERA = 102;
    public static final int EMAIL_VERIFICATION = 103;
    public static final int PICK_FROM_VIDEO = 104;
    public static final int PICK_FROM_FILE = 105;
    public static final int PICK_FROM_INVITE  = 106;

    public static final int INPUT_STATE_MESSAGE = 200;
    public static final int INPUT_NAME = 201;

    public static final String KEY_ADDRESS = "address";
    public static final String KEY_ROOM = "room";
    public static final String KEY_ROOM_TITLE = "room_title";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_LOGOUT = "loguut";
    public static final String KEY_FRIEND = "friend";
    public static final String KEY_FILE = "file";
    public static final String KEY_PARTICIPANTS = "participants";
    public static final String KEY_FROMLOGIN = "fromlogin";

    public static final String KEY_ADD_BLOCK = "addOrblock";
    public static final String KEY_ADD = "add";
    public static final String KEY_BLOCK = "block";

    public static final String KEY_SEPERATOR = "#";
    public static final String KEY_FILE_MARKER = "FILE#";
    public static final String KEY_IMAGE_MARKER = "IMAGE#";
    public static final String KEY_VIDEO_MARKER = "VIDEO#";
    public static final String KEY_ROOM_MARKER = "ROOM#";

    public static final String KEY_NOTE = "note";

    public static final String XMPP_START = "xmpp";
    public static final int XMPP_FROMBROADCAST = 0;
    public static final int XMPP_FROMLOGIN = 1;

    public static final int NORMAL_NOTI_ID = 1;
    public static final int PUSH_NOTI_ID = 2;

    public static final String KEY_MESSAGE = "message";

    public static final int PHONENUMBER_LENGTH = 10;

    public static final int ANDROID = 0;

    public static final String USER = "user";
    public static final String CLASS = "class";

    public static  Bitmap bitmap_my;
    public static Bitmap bitmap_user;

    public static Bitmap alert_userR;
    public static Bitmap alert_userL;

    public static int private_status = 1;

    public static String publicstatus="public";
    public static String countryname = null ;
    public static String userPhoto = null ;
    public static int chat_status = 0;

    public static int facebook_login = 0;
    public static int google_login = 0;
    public static int twitter_login = 0;

    public static VoteEntity voteEntity = new VoteEntity();
    public static ArrayList<NotiModel> notiModels=new ArrayList<>();  // Firebase Noti Array List========================

    public static String receivemessage="";
    public static int messagetype=0;  //0: word, 1: image  3: product messsage
    public static String recieveimageurl="";
    public static boolean messagestatuse;
    public static int noti_counter = 0 ;  //noti counter
    public static int noti_status = 0 ;   // noti current status : 0: noti:
    public static int current_page = 1; //current post page :
    public static int currentPageFollower = 1 ; //current follower page :
    public static int list_position = 0; //current post item position:
    public static int list_Follow_position = 0 ; // current follow item position:

    public static String token = null;

}
