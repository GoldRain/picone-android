package com.picone.commons;

import android.os.Handler;

import com.picone.chatting.ConnectionMgrService;
import com.picone.base.CommonActivity;
import com.picone.model.UserEntity;

/**
 * Created by ToSuccess on 11/29/2016.
 */

public class ReqConst {

    public static final String SERVER_ADDR = "http://192.168.1.120:1300/pic_one";
    //public static final String SERVER_ADDR = "http://35.160.119.140";

    public static final String CHATTING_SERVER = "35.160.119.140";

    public static boolean g_isAppRunning = false ;
    public static boolean g_isAppPaused = false ;

    public static Handler g_handler = null ;
    public static String g_appVersion = "1.0";
    public static int g_badgCount = 0 ;

    public static UserEntity g_newUser = null;
    public static UserEntity g_user = null;

    public static ConnectionMgrService g_xmppService = null;

    public static CommonActivity g_currentActivity = null;
    public static final String ROOM_SERVICE = "@conference.";

    public static final String SERVER_URL = SERVER_ADDR + "/index.php/api/";

      /*Request value*/

    public static final String REQ_SIGNUP = "signup";
    public static final String REQ_LOGIN = "login";
    public static final String REQ_FORGOTPWD = "forgotPassword";
    public static final String REQ_UPLOADPHOTO = "uploadPhoto";
    public static final String REQ_PARAM_ID = "id";
    public static final String REQ_PARAM_FILE = "file";
    public static final String REQ_EDIT = "editProfile";
    public static final String REQ_GETALLPOST = "getAllpost";
    public static final String REQ_UPLOADPOST1 = "uploadPost1";
    public static final String REQ_UPLOADPOST2 = "uploadPost2";

    public static final String REQ_EMAILCONFIRM = "emailConfirm";
    public static final String REQ_SETFOLLOWER = "setFollower";
    public static final String REQ_GETALLFOLLOWING = "getAllFollowing";
    public static final String REQ_ALLFOLLOWER = "getAllFollower";
    public static final String REQ_SETVOTE = "setVote";
    public static final String REQ_GETUSERINFO = "getUserInfo";
    public static final String REQ_ADDFRIEND = "addFriend";
    public static final String REQ_REQUESTFRIEND = "requestFriend";
    public static final String REQ_ACCEPTFRIEND = "acceptFriend";
    public static final String REQ_GETALLFRIEND = "getAllFriend";


    public static final String REQ_FACEBOOK = "loginwithfacebook";
    /*Response Value*/

    public static final String RES_CODE = "result_code";
    public static final String RES_USERINFO = "user_info";
    public static final String RES_ID = "id";
    public static final String RES_USERNAME = "username";
    public static final String RES_FIRSTNAME = "firstname";
    public static final String RES_LASTNAME = "lastname";
    public static final String RES_FOLLOWING = "following";
    public static final String RES_FOLLOWER = "follower";
    public static final String RES_PHONE = "phone";
    public static final String RES_PASSWORD = "password";
    public static final String RES_NAME = "name";
    public static final String RES_EMAIL = "email";
    public static final String RES_COUNTRY = "country";
    public static final String RES_ADDRES = "address";
    public static final String RES_INTERESTS = "interests";
    public static final String REQ_STATUS = "status";
    public static final String RES_PHOTO_URL = "photo_url";
    public static final String RES_POSTINFORS = "post_infos";
    public static final String RES_POSTID = "postid";
    public static final String RES_FOLLOWINGINFOS = "following_infos";
    public static final String RES_USERID = "userid";
    public static final String RES_FOLLOWERINFOS = "follower_infos";
    public static final String RES_STATUS = "status";
    public static final String RES_FRIENDINFOS = "friend_infos";

    public static final int CODE_SUCCESS = 0;
    public static final int CODE_EXISTUSERNAME = 101 ;
    public static final int CODE_UNREGISTER = 102 ;
    public static final int CODE_WRONGPWD = 103 ;
    public static final int CODE_UPLOADFAIL = 104;
    public static final int CODE_ALREADYVOTE = 106;
    public static final int CODE_UNREQUEST = 107;
    public static final int CODE_EXITEMAIL = 108 ;

}
