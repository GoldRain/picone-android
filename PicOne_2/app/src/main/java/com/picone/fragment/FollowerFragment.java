package com.picone.fragment;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.picone.PicOneApplication;
import com.picone.R;
import com.picone.activity.MainActivity;
import com.picone.adapter.FollowerListAdapter;
import com.picone.base.BaseFragment;
import com.picone.commons.Commons;
import com.picone.commons.Constants;
import com.picone.commons.ReqConst;
import com.picone.model.Friendmodel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.picone.R.string.follower;


public class FollowerFragment extends BaseFragment implements SwipyRefreshLayout.OnRefreshListener{

    MainActivity activity ;
    View view ;

    FollowerListAdapter _adapter;

    ListView ui_lstFollower;
    ArrayList<Friendmodel> friendmodels=new ArrayList<>();
    SwipyRefreshLayout ui_swipyLayout ;

    int _idx = 0 ;
    int _curPage = 1;
    int refreshDirection = 0; // refreshDirection 1: Bottom, -1: Top
    Boolean endpageCheck = true ;

    public FollowerFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view =  inflater.inflate(R.layout.fragment_follower, container, false);



        loadLayout();
        return view ;
    }

    private void loadLayout() {

        ui_lstFollower = (ListView)view.findViewById(R.id.lst_follower);
        _adapter =new FollowerListAdapter(activity);
        ui_lstFollower.setAdapter(_adapter);

        ui_swipyLayout = (SwipyRefreshLayout)view.findViewById(R.id.swp_follower);
        ui_swipyLayout.setOnRefreshListener(this);
        ui_swipyLayout.setDirection(SwipyRefreshLayoutDirection.BOTH);

        _curPage = Constants.currentPageFollower ;

        ui_swipyLayout.post(new Runnable() {
            @Override
            public void run() {

                getAllFollower();
            }
        });
    }

    public void getAllFollower() {

        String url = ReqConst.SERVER_URL + ReqConst.REQ_ALLFOLLOWER;
        _idx = Commons.g_user.get_idx() ;
        String params = String.format("/%d/%d",_idx, _curPage );
        url += params ;

        Log.d("Follower=====URL=========", url);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String json) {
                parseGetAllFollower(json);
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                ui_swipyLayout.setRefreshing(false);
                activity.showAlertDialog(getString(R.string.error));
            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT,0,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        PicOneApplication.getInstance().addToRequestQueue(stringRequest, url);

    }

    public void parseGetAllFollower(String json) {

        ui_swipyLayout.setRefreshing(false);

        Log.d("===========respons=======",json);

        try {

            JSONObject respons = new JSONObject(json);

            int result_code = respons.getInt(ReqConst.RES_CODE);

            if (result_code == ReqConst.CODE_SUCCESS){

                JSONArray followers = respons.getJSONArray(ReqConst.RES_FOLLOWERINFOS);


                friendmodels.clear();

                if (followers.length() == 0){

                    ui_swipyLayout.setRefreshing(false);
                    endpageCheck = false ;

                } else {

                    for (int i = 0 ; i < followers.length(); i++){

                        Constants.currentPageFollower = _curPage ;

                        JSONObject jsonFollower = (JSONObject) followers.get(i);

                        Friendmodel follower = new Friendmodel();

                        follower.setId(jsonFollower.getInt(ReqConst.RES_USERID));
                        follower.setFriendphotourl(jsonFollower.getString(ReqConst.RES_PHOTO_URL));
                        follower.setFriendname(jsonFollower.getString(ReqConst.RES_USERNAME));
                        follower.setCheckstatuse(jsonFollower.getInt(ReqConst.RES_STATUS));

                        if (follower.getId() == Commons.g_user.get_idx()) {continue;}

                        friendmodels.add(follower);

                }

                    _adapter.setFollowerDatas(friendmodels);
                    _adapter.notifyDataSetChanged();
                    ui_lstFollower.setSelection(Constants.list_Follow_position);

                }

            } else {

                ui_swipyLayout.setRefreshing(false);
                activity.showAlertDialog(getString(R.string.error));
            }

        } catch (JSONException e) {
            e.printStackTrace();

            activity.showAlertDialog(getString(R.string.error));
        }
    }

    @Override
    public void onRefresh(SwipyRefreshLayoutDirection direction) {

        Log.d("=======curPage============", String.valueOf(_curPage));

        if (direction == SwipyRefreshLayoutDirection.BOTTOM){

            if (endpageCheck == false){

                ui_swipyLayout.setRefreshing(false);
                return;

            } else {

                _curPage++;
                refreshDirection = 1;
                Constants.list_Follow_position = 0 ;
                getAllFollower();
            }

        }else if (direction == SwipyRefreshLayoutDirection.TOP && _curPage > 1){

            refreshDirection = -1;
            Constants.list_position = 0;
            _curPage = Constants.currentPageFollower - 1 ;
            endpageCheck = true ;

            getAllFollower();

        } else if (_curPage == 1 || refreshDirection == -1){

            Constants.list_position = 0 ;
            ui_swipyLayout.setRefreshing(false);
            endpageCheck = true ;
            return;
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (MainActivity)context;
    }
}
