package com.picone.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.picone.R;
import com.picone.base.BaseFragment;
import com.picone.commons.Commons;
import com.picone.commons.Constants;
import com.picone.model.VoteEntity;

import static com.picone.R.id.imv_Photo_user;

public class UserFullFragment extends BaseFragment {

    UserProfileFragment _fragment;
    VoteEntity _voteModel = new VoteEntity() ;
    ImageView ui_imvPhoto;
    View view ;

    public UserFullFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        view =  inflater.inflate(R.layout.fragment_user_full, container, false);

        ui_imvPhoto = (ImageView)view.findViewById(R.id.imv_Photo_user);
        Glide.with(this).load(Constants.userPhoto).placeholder(R.drawable.image_me).into(ui_imvPhoto);

        return view;
    }
}
