package com.picone.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.picone.R;
import com.picone.activity.MainActivity;
import com.picone.adapter.NotificationListAdapter;
import com.picone.commons.Constants;
import com.picone.model.NotiModel;

import java.util.ArrayList;


public class NotiFragment extends Fragment {

    NotificationListAdapter notificationListAdapter;
    ListView notilistview;
    View view;
    MainActivity mainActivity;

    ArrayList<NotiModel> _NotiModels = new ArrayList<>();

    public NotiFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.fragment_noti, container, false);

/*        if (Constants.noti_counter > 0){

            mainActivity.showNoti(View.VISIBLE,Constants.noti_counter);

            Log.d("======notiFragment===Noti=====counter=======", String.valueOf(Constants.noti_counter));

        } else {

            mainActivity.showNoti(View.GONE, 0);
        }*/

        mainActivity.showNotiRefresh();

        notilistview=(ListView)view.findViewById(R.id.lst_noti);
        notificationListAdapter = new NotificationListAdapter(mainActivity, _NotiModels);
        notilistview.setAdapter(notificationListAdapter);

        if(Constants.notiModels.size() <= 0){

        } else {

            for(int i=0; i < Constants.notiModels.size(); i++){

                NotiModel notiModel = Constants.notiModels.get(i);

                //Constants.noti_counter-- ;
                NotiModel _notiModel = new NotiModel();

                _notiModel.setId(notiModel.getId());  // required friend id  ====
                _notiModel.setUsername(notiModel.getUsername());
                _notiModel.setNoti_photo(notiModel.getNoti_photo());

                _NotiModels.add(_notiModel);

                //Constants.notiModels.add(_notiModel);
            }

            //notificationListAdapter.setNotiDatas(_NotiModels);
            notificationListAdapter.notifyDataSetChanged();
        }

        return view;
    }

    @Override
    public void onResume() {

        mainActivity.showNotiRefresh();
        super.onResume();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mainActivity = (MainActivity)context;
    }

}
