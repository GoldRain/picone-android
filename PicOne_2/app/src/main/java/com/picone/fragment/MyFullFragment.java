package com.picone.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.picone.R;
import com.picone.activity.MainActivity;
import com.picone.commons.Commons;
import com.picone.commons.Constants;


public class MyFullFragment extends Fragment {

    ImageView ui_imvMyphoto;

    MainActivity activity;
    View view ;

    public MyFullFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_my_full, container, false);
        loadLayout();
        return view ;
    }

    private void loadLayout() {

        ui_imvMyphoto = (ImageView)view.findViewById(R.id.imv_photo_my_full);
        ui_imvMyphoto.setImageBitmap(Constants.bitmap_my);

        UpdateDate();
    }

    private void UpdateDate() {


        if (Commons.g_user.get_photoUrl().length() != 0 && !Commons.g_user.get_photoUrl().equals(null)){

            Glide.with(this).load(Commons.g_user.get_photoUrl()).placeholder(R.drawable.image_me).into(ui_imvMyphoto);

        }else {

            ui_imvMyphoto.setImageResource(R.drawable.image_me);
        }
    }

}
