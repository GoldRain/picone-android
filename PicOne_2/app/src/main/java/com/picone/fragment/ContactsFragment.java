package com.picone.fragment;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.picone.PicOneApplication;
import com.picone.R;
import com.picone.activity.MainActivity;
import com.picone.adapter.ContactslistViewAdapter;
import com.picone.base.BaseFragment;
import com.picone.commons.Commons;
import com.picone.commons.Constants;
import com.picone.commons.ReqConst;
import com.picone.model.Friendmodel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class ContactsFragment extends BaseFragment {

    MainActivity _activity;
    ContactslistViewAdapter _adapter;
    ListView ui_lstView ;
    View view ;

    SwipyRefreshLayout ui_refreshLayout ;
    ArrayList<Friendmodel> _contact = new ArrayList<>();

    int _curPage = 1;
    int refreshDirection = 0 ; // refreshDirection 1: Bottom : -1 : Top ====

    ArrayList<Friendmodel> friendmodels = new ArrayList<>();

    public ContactsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_contacts, container, false);

        Toast.makeText(_activity, "contactfragment", Toast.LENGTH_SHORT).show();

        loadLayout();
        return view;
    }

    private void loadLayout() {

        //  api==========================


        ui_lstView = (ListView)view.findViewById(R.id.lst_container_con);

       /* _adapter = new ContactslistViewAdapter(_activity);
        ui_lstView.setAdapter(_adapter);*/

  /*      for (int i = 0; i< 20; i++){

            Friendmodel friend = new Friendmodel();

            friend.setUsername("Name");
            friend.setFriendphotourl(String.valueOf(R.drawable.image_me));
            _contact.add(friend);

          //  _adapter.addItem(friend);

        }*/

        _adapter = new ContactslistViewAdapter(_activity, _contact);
        ui_lstView.setAdapter(_adapter);

        getAllContacts();
      //  _adapter.notifyDataSetChanged();

    }

    public void getAllContacts(){

        String url = ReqConst.SERVER_URL + ReqConst.REQ_GETALLFRIEND;
        int _idx = Commons.g_user.get_idx();

        String params = String.format("/%d" , _idx);

        url += params ;

        _activity.showProgress();
        Log.d("====url========", url);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {

                parseGetAllContacts(json);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                _activity.closeProgress();
                _activity.showAlertDialog(getString(R.string.error));
            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT,0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        PicOneApplication.getInstance().addToRequestQueue(stringRequest, url);

    }

    public void parseGetAllContacts(String json){

        _activity.closeProgress();

        Log.d("=======json======", json);

        try {
            JSONObject respons = new JSONObject(json);

            int result_code = respons.getInt(ReqConst.RES_CODE);

            if (result_code == ReqConst.CODE_SUCCESS){

                JSONArray contacts = respons.getJSONArray(ReqConst.RES_FRIENDINFOS);

              /*  if (contacts.length() > 0 && refreshDirection == 1){

                    _curPage++ ;
                }

                _contact.clear();*/

                for (int i = 0 ; i < contacts.length(); i++) {

                    JSONObject jsonContact = (JSONObject)contacts.get(i);

                    Friendmodel contact = new Friendmodel();

                    contact.setId(jsonContact.getInt(ReqConst.RES_ID));
                    contact.setFriendphotourl(jsonContact.getString(ReqConst.RES_PHOTO_URL));
                    contact.setFriendname(jsonContact.getString(ReqConst.RES_NAME));

                    if (Commons.g_user.get_idx() == jsonContact.getInt(ReqConst.RES_ID)) continue;

                    _contact.add(contact);
                }

                _adapter.setContacts (_contact);
                _adapter.notifyDataSetChanged();

            } else {

                _activity.closeProgress();
                _activity.showAlertDialog(getString(R.string.error));
            }

        } catch (JSONException e) {
            e.printStackTrace();
            _activity.closeProgress();
            _activity.showAlertDialog(getString(R.string.error));
        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        _activity = (MainActivity)context;
    }

}
