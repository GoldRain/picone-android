package com.picone.fragment;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.picone.PicOneApplication;
import com.picone.R;
import com.picone.activity.MainActivity;
import com.picone.adapter.FollowerListAdapter;
import com.picone.base.BaseFragment;
import com.picone.commons.Commons;
import com.picone.commons.Constants;
import com.picone.commons.ReqConst;
import com.picone.model.Friendmodel;
import com.picone.model.VoteEntity;
import com.picone.utils.RadiusImageView;

import org.json.JSONException;
import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;


public class UserProfileFragment extends BaseFragment implements View.OnClickListener {

    MainActivity _activity;
    View view ;

    VoteEntity _voteModel = new VoteEntity();

    ImageView ui_imvPhoto, ui_imvEdit, ui_imvDown;
    RadiusImageView ui_imvSmallPho ;
    TextView ui_txvFollower, ui_txvFollowing,ui_txvName,ui_txvSmallName,ui_txvAddress, ui_txvFullprofiel ,ui_newsfeed, txv_follow_user, txv_addfriend_user
            ,txv_interest_des_user,txv_status_des_user;

    LinearLayout ui_lytInterest, ui_lytStatus,  ui_photo_name;

    int _idx = 0;
    int _userID = 0;

    public UserProfileFragment(VoteEntity voteModel) {
        // Required empty public constructor
        this._voteModel = voteModel;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_user_profile, container, false);

        loadLayout();
        return view;
    }

    private void loadLayout() {

        ui_imvPhoto = (ImageView)view.findViewById(R.id.imv_photo_user);
        ui_imvPhoto.setOnClickListener(this);

        ui_imvSmallPho = (RadiusImageView)view.findViewById(R.id.imv_smallphoto_user);

        txv_follow_user = (TextView)view.findViewById(R.id.txv_follow_user);
        txv_follow_user.setOnClickListener(this);

        txv_addfriend_user = (TextView)view.findViewById(R.id.txv_addfriend_user);
        txv_addfriend_user.setOnClickListener(this);

        ui_imvDown = (ImageView)view.findViewById(R.id.imv_down_user);
        ui_imvDown.setOnClickListener(this);

        ui_txvFollower = (TextView)view.findViewById(R.id.txv_follower_user);
        ui_txvFollower.setOnClickListener(this);

        ui_txvFollowing = (TextView)view.findViewById(R.id.txv_following_user);
        ui_txvFollowing.setOnClickListener(this);

        ui_txvName = (TextView)view.findViewById(R.id.txv_name_user);
        ui_txvSmallName = (TextView)view.findViewById(R.id.txv_smallName_user);

        ui_txvAddress = (TextView)view.findViewById(R.id.txv_ad_des_user);

        ui_txvFullprofiel = (TextView)view.findViewById(R.id.txv_full_userprofile);
        ui_txvFullprofiel.setOnClickListener(this);

        txv_interest_des_user = (TextView)view.findViewById(R.id.txv_interest_des_user);
        txv_status_des_user = (TextView)view.findViewById(R.id.txv_status_des_user);

        ui_newsfeed = (TextView)view.findViewById(R.id.txv_newsfeed);

        ui_photo_name = (LinearLayout)view.findViewById(R.id.lyt_photo_user);
        ui_lytInterest = (LinearLayout)view.findViewById(R.id.lyt_interests_user);
        ui_lytStatus = (LinearLayout)view.findViewById(R.id.lyt_status_user);

        UpdateDate();
    }

    private void UpdateDate() {

        Log.d("=======add Friends=======", String.valueOf(_voteModel.get_friend_status()));

//        voteModel = new VoteEntity();

        if ((_voteModel.toString().length() > 0)){

            ui_txvName.setText(_voteModel.get_username());
            ui_txvSmallName.setText(_voteModel.get_username());
            ui_txvFollower.setText("Follower:"+String.valueOf(_voteModel.get_follwer()));
            ui_txvFollowing.setText("Following:"+String.valueOf(_voteModel.get_following()));
            ui_txvAddress.setText(_voteModel.get_address());
            txv_interest_des_user.setText(_voteModel.get_interests());
            txv_status_des_user.setText(_voteModel.get_status());

            Log.d("RequestSent=============", String.valueOf(_voteModel.get_friend_status()));

            if (_voteModel.get_friend_status() == 1 || _voteModel.get_friend_status() == 3){

                txv_addfriend_user.setText("Request sent");
            }

            Log.d("relation=================", String.valueOf(_voteModel.get_relation()));


            if (_voteModel.get_relation() == 0){    // 0:Follow, 1: Unfollow

                txv_follow_user.setText("Follow");

            }else {txv_follow_user.setText("UnFollow");}

            Log.d("Following=========", String.valueOf(_voteModel.get_following()));
        }

        if (_voteModel.get_userphotoUrl().length() != 0 && !_voteModel.get_userphotoUrl().equals(null)){

            Glide.with(this).load(_voteModel.get_userphotoUrl()).placeholder(R.drawable.image_me).into(ui_imvPhoto);
            Glide.with(this).load(_voteModel.get_userphotoUrl()).placeholder(R.drawable.image_me).into(ui_imvSmallPho);

            /*imv_photo.setImageUrl(Commons.g_user.get_photoUrl(),_imageLoader);*/
        }else {

            ui_imvPhoto.setImageResource(R.drawable.image_me);
        }
    }


    private void setFollow() {

        _activity.showProgress();

        int _idx = 0;
        int _userID = 0;

        String url = ReqConst.SERVER_URL + ReqConst.REQ_SETFOLLOWER;

        _idx = Commons.g_user.get_idx();
        _userID = _voteModel.get_userID();

        String params = String.format("/%d/%d", _idx, _userID);
        url += params ;

        Log.d("======URL=====",url);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String json) {

                _activity.closeProgress();

                try {

                    JSONObject respons = new JSONObject(json);
                    int result_code = respons.getInt(ReqConst.RES_CODE);

                    if (result_code == ReqConst.CODE_SUCCESS) {

                        _activity.closeProgress();

                        Log.d("voteMode_status==========", String.valueOf(_voteModel.get_relation()));

                        if(_voteModel.get_relation() == 0) {

                            txv_follow_user.setText("UnFollow");
                            _voteModel.set_relation(1);
                            _activity.showToast("Success Follower");

                            int follower = Commons.g_user.get_follower();
                            follower++;
                            Commons.g_user.set_follower(follower);

                        }else {

                            txv_follow_user.setText("Follow");
                           _voteModel.set_relation(0);
                            _activity.showToast("Success UnFollow");

                            int follower = Commons.g_user.get_follower();

                            if (follower > 0){

                                follower--;
                            } else {

                                _activity.showAlertDialog("Don't UnFollow");
                            }
                            Commons.g_user.set_follower(follower);
                        }
                    }

                } catch (JSONException e) {

                    e.printStackTrace();
                    _activity.showAlertDialog(String.valueOf(R.string.error));
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                _activity.closeProgress();
                _activity.showAlertDialog(String.valueOf(R.string.error));
            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT,0,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        PicOneApplication.getInstance().addToRequestQueue(stringRequest,url);
    }

    private void requestSend(){

        _activity.showProgress();

        String url = ReqConst.SERVER_URL + ReqConst.REQ_REQUESTFRIEND;

        int sender_id = Commons.g_user.get_idx();
        int reciever_id = _voteModel.get_userID();

        String params = String.format("/%d/%d", sender_id, reciever_id);

        url += params ;

        Log.d("=====url=======", url);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {
                parseRequestSend(json);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                _activity.closeProgress();
                _activity.showAlertDialog(getString(R.string.error));
            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT,0,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        PicOneApplication.getInstance().addToRequestQueue(stringRequest, url);
    }

    public void parseRequestSend(String json){

        _activity.closeProgress();

        try {

            JSONObject respons = new JSONObject(json);
            int result_code = respons.getInt(ReqConst.RES_CODE);

            if (result_code == ReqConst.CODE_SUCCESS){

                _activity.showToast("Success Request");
                txv_addfriend_user.setText("Request sent");
                _voteModel.set_friend_status(1);

                _activity.showToast(String.valueOf(_voteModel.get_friend_status()));

            } else if (result_code == ReqConst.CODE_UNREQUEST){

                _activity.closeProgress();
                _activity.showToast("Request Faild");
            }

        } catch (JSONException e) {
            e.printStackTrace();

            _activity.closeProgress();
            _activity.showAlertDialog(getString(R.string.error));
        }

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.imv_photo_user:
                _activity.UserFullFragment();

                break;

            case R.id.txv_full_userprofile:
                fullProfile();
                break;

            case R.id.imv_down_user:
                partProfile();
                break;

            case R.id.txv_follow_user:

                if (Commons.g_user.get_idx() != _voteModel.get_userID()){

                    setFollow();
                }
                break;

            case R.id.txv_addfriend_user:

                if (Commons.g_user.get_idx() != _voteModel.get_userID()) {

                    if (_voteModel.get_friend_status() == 0 || _voteModel.get_friend_status() == 2) {
                        requestSend();
                    }

                } else {}

                break;

        }
    }

    private void fullProfile() {

        ui_txvFullprofiel.setVisibility(View.GONE);
        ui_newsfeed.setVisibility(View.GONE);
        ui_photo_name.setVisibility(View.GONE);
        ui_imvDown.setVisibility(View.GONE);

        ui_lytInterest.setVisibility(View.VISIBLE);
        ui_lytStatus.setVisibility(View.VISIBLE);
        ui_imvDown.setVisibility(View.VISIBLE);
    }

    private void partProfile() {

        ui_txvFullprofiel.setVisibility(View.VISIBLE);
        ui_newsfeed.setVisibility(View.VISIBLE);
        ui_photo_name.setVisibility(View.VISIBLE);
        ui_imvDown.setVisibility(View.VISIBLE);

        ui_lytInterest.setVisibility(View.GONE);
        ui_lytStatus.setVisibility(View.GONE);
        ui_imvDown.setVisibility(View.GONE);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        _activity = (MainActivity)context;
    }
}
