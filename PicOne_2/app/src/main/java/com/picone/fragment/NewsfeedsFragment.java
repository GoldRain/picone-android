package com.picone.fragment;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.picone.PicOneApplication;
import com.picone.R;
import com.picone.activity.MainActivity;
import com.picone.adapter.NewsfeedslistAdapter;
import com.picone.adapter.PublicAlertAdapter;
import com.picone.base.BaseFragment;
import com.picone.commons.Commons;
import com.picone.commons.Constants;
import com.picone.commons.ReqConst;
import com.picone.logger.Logger;
import com.picone.model.VoteEntity;
import com.picone.utils.BitmapUtils;
import com.picone.utils.MultiPartRequest;
import com.picone.utils.RadiusImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.R.attr.direction;
import static android.app.Activity.RESULT_OK;
import static android.content.Context.BIND_AUTO_CREATE;
import static com.picone.activity.MainActivity.MY_PEQUEST_CODE;
import static com.picone.activity.MainActivity.hasPermissions;



public class NewsfeedsFragment extends BaseFragment implements View.OnClickListener ,SwipyRefreshLayout.OnRefreshListener {

    MainActivity _activity ;
    VoteEntity _voteAlert = new VoteEntity();
    ArrayList<VoteEntity> _posts = new ArrayList<>();

    SwipyRefreshLayout ui_RefreshLayout;
    NewsfeedslistAdapter _adapter;
    ListView ui_lst_newsfees ;
    View view ;

    TextView ui_txvNewpost;
    Uri _imageCaptureUri;
    String _photoPath = "";

    //third add text:
    EditText ui_edtTitle;
    RadiusImageView ui_imvPhotoL, ui_imvPhotoR;
    ImageView imv_photoL_add_rec,imv_photoR_add_rec;
    TextView ui_txvNext_text, ui_txvBack;

    String _photo_left = "",_photo_right = "";

    //fourth add Time
    EditText ui_edtDescription;
    TextView ui_txvTimer,ui_txvDate,ui_txvDate_date , ui_txvNext_time, ui_txvCancel_time ,ui_txvTime_time;
    Spinner spinner ;

    PublicAlertAdapter _adpterPublic;

    //fifth preview
    TextView txv_title_pre, txv_description_pre,ui_txvVoteL, ui_txvVoteR,ui_txvPercentL, ui_txvPercentR
            , ui_txvPublish, ui_txvCancel_pre;

    //sixth share
    TextView ui_txvPost, ui_txvCancel_share;
    ImageView ui_imvFace, ui_imvGoogle, ui_imvTwitter, ui_imvApp;

    int order = 0;
    int _curpage = 1;
    private int _idx = 0;
    int postId= 0 ;
    String time = "";
    int refreshDirection = 0;  // refreshDirection 1: Bottom, -1: Top
    int _middle = 0;

    Boolean endpagecheck=true;

    String[] PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE,  Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_newsfeeds, container, false);

        loadLayout();
        return view ;
    }

    private void loadLayout() {

        ui_txvNewpost = (TextView)view.findViewById(R.id.plus_sign);
        ui_txvNewpost.setOnClickListener(this);

        /*ui_imvPhoto = (ImageView)view.findViewById(R.id.imv_photoL_add);*/

        ui_RefreshLayout = (SwipyRefreshLayout)view.findViewById(R.id.swipe_refresh_layout);
        ui_RefreshLayout.setOnRefreshListener( this);
        ui_RefreshLayout.setDirection(SwipyRefreshLayoutDirection.BOTH);

        _adapter = new NewsfeedslistAdapter(this);
        ui_lst_newsfees = (ListView)view.findViewById(R.id.lst_container_new);

        /*LoadItem();*/
        ui_lst_newsfees.setAdapter(_adapter);
        _curpage = Constants.current_page ;

        ui_RefreshLayout.post(new Runnable() {
                                  @Override
                                  public void run() {
                                      getAllPost();
                                  }
                              }
        );
    }

    public void getAllPost(){

        String url = ReqConst.SERVER_URL + ReqConst.REQ_GETALLPOST;

        //int id = 1;
        int id = Commons.g_user.get_idx();
        String params = String.format("/%d/%d", id, _curpage);
        url += params ;

        Log.d("======postAll========", url);

        ui_RefreshLayout.setRefreshing(true);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {
                parseGetPostResponse(json);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                ui_RefreshLayout.setRefreshing(false);

                _activity.showAlertDialog(getString(R.string.error));
            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT,0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        PicOneApplication.getInstance().addToRequestQueue(stringRequest,url);
    }

    public void parseGetPostResponse(String json){

        ui_RefreshLayout.setRefreshing(false);

        Log.d("result----", json);

        try {

            JSONObject response = new JSONObject(json);

            int result_code = response.getInt(ReqConst.RES_CODE);

            if (result_code == ReqConst.CODE_SUCCESS) {

                JSONArray posts = response.getJSONArray(ReqConst.RES_POSTINFORS);

                Log.d("JSONArray=========post", String.valueOf(posts));

                _posts.clear();

                if (posts.length() == 0){

                    ui_RefreshLayout.setRefreshing(false);
                    endpagecheck=false;

                    //Log.d("Constant++++++++current========", String.valueOf(Constants.current_page));


                } else {

                    Constants.current_page=_curpage;

                    for (int i = 0; i < posts.length(); i++) {

                        JSONObject jsonPost = (JSONObject) posts.get(i);

                        VoteEntity vote = new VoteEntity();

                        vote.setId(jsonPost.getInt("postid"));
                        vote.set_userID(jsonPost.getInt("userid"));
                        vote.set_userphotoUrl(jsonPost.getString(ReqConst.RES_PHOTO_URL));
                        vote.set_username(jsonPost.getString("name"));
                        vote.set_address(jsonPost.getString("address"));
                        vote.set_interests(jsonPost.getString("interests"));
                        vote.set_status(jsonPost.getString("status"));
                        vote.set_following(jsonPost.getInt("following"));
                        vote.set_follwer(jsonPost.getInt("follower"));
                        vote.set_title(jsonPost.getString("title"));
                        vote.set_description(jsonPost.getString("description"));
                        vote.set_votePhotoUrl_L(jsonPost.getString("image1_url"));
                        vote.set_votePhotoUrl_R(jsonPost.getString("image2_url"));
                        vote.set_votePercentL(jsonPost.getInt("vote1"));
                        vote.set_votePercentR(jsonPost.getInt("vote2"));
                        vote.set_relation(jsonPost.getInt("relation"));
                        vote.set_friend_status(jsonPost.getInt("friend_status"));

                  /*  if (vote.getId() == Commons.g_user.get_idx()){
                        continue;
                    }*/

                        _posts.add(vote);
                    }


                    _adapter.setVoteDatas(_posts);
                    _adapter.notifyDataSetChanged();

                    ui_lst_newsfees.setSelection(Constants.list_position);
                }

            } else {
                _activity.showAlertDialog(getString(R.string.error));
            }

        } catch (JSONException e) {
            e.printStackTrace();

            _activity.showAlertDialog(getString(R.string.error));
        }
    }

    @Override
    public void onRefresh(SwipyRefreshLayoutDirection direction) {


        if (direction == SwipyRefreshLayoutDirection.BOTTOM) {

            if(endpagecheck==false){

                ui_RefreshLayout.setRefreshing(false);
                return;

            }else {

                _curpage ++;
                refreshDirection = 1;  //bottom
                Constants.list_position = 0 ;

                getAllPost();
            }

        } else if ((direction == SwipyRefreshLayoutDirection.TOP) && _curpage > 1) {

            Log.d("TOP curpage : ", String.valueOf(_curpage));

            refreshDirection = -1;  // top
            Constants.list_position = 0 ;
            _curpage=Constants.current_page-1;
            endpagecheck = true ;

            getAllPost();

        } else if (_curpage == 1 || refreshDirection == -1){

            Constants.list_position = 0 ;
            ui_RefreshLayout.setRefreshing(false);
            endpagecheck = true ;
            return;
        }

        Log.d("_currentPage==========", String.valueOf(_curpage));

    }

    public void uploadPost1() {

        try {

            File file = new File(Constants.voteEntity.get_votePhotoUrl_L());

            Map<String, String> params = new HashMap<>();
            params.put("userid",String.valueOf(Commons.g_user.get_idx()));
            params.put("title", Constants.voteEntity.get_title());
            params.put("description",Constants.voteEntity.get_description());
            params.put("fromtime",Constants.voteEntity.get_date() + " " + Constants.voteEntity.get_time() );
            params.put("capacity", String.valueOf(Constants.voteEntity.get_private_status()));

            String url = ReqConst.SERVER_URL + ReqConst.REQ_UPLOADPOST1;

            String time = Constants.voteEntity.get_date() + " " + Constants.voteEntity.get_time();

            //Log.d("======time========",time);

            Log.d("PST+++URL======", url);
            Log.d("=====UserId==========", String.valueOf(Commons.g_user.get_idx()));

            MultiPartRequest reqMultiPart = new MultiPartRequest(url, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    _activity.showAlertDialog(getString(R.string.photo_upload_fail));

                }
            }, new Response.Listener<String>() {

                @Override
                public void onResponse(String json) {

                    ParseUploadPostResponse(json);
                }
            }, file, ReqConst.REQ_PARAM_FILE, params);

            reqMultiPart.setRetryPolicy(new DefaultRetryPolicy(
                    Constants.VOLLEY_TIME_OUT, 0,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            PicOneApplication.getInstance().addToRequestQueue(reqMultiPart, url);

        } catch (Exception e) {

            e.printStackTrace();
            _activity.closeProgress();
            _activity.showAlertDialog(getString(R.string.photo_upload_fail));
        }
    }

    public void ParseUploadPostResponse(String json){

        _activity.closeProgress();

        try {
            JSONObject response = new JSONObject(json);

            int result_code = response.getInt(ReqConst.RES_CODE);


            //Log.d("========result=====", String.valueOf(result_code));

            if (result_code == 0){

                postId = response.getInt(ReqConst.RES_POSTID);

                //time = response.getString("time");

                uploadPost2();
            }
            else {
                _activity.showAlertDialog(getString(R.string.photo_upload_fail));
            }

        } catch (JSONException e){
            e.printStackTrace();
            _activity.showAlertDialog(getString(R.string.photo_upload_fail));
        }
    }

    private void uploadPost2() {

        Log.d("vote=====photoL", Constants.voteEntity.get_votePhotoUrl_L());
        Log.d("vote=====photoR", Constants.voteEntity.get_votePhotoUrl_R());

        try {

            File file = new File(Constants.voteEntity.get_votePhotoUrl_R());

            Map<String, String> params = new HashMap<>();
            params.put("postid",String.valueOf(postId));

            String url = ReqConst.SERVER_URL + ReqConst.REQ_UPLOADPOST2;

            Log.d("URL===========", url);

            MultiPartRequest reqMultiPart = new MultiPartRequest(url, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    _activity.closeProgress();
                    _activity.showAlertDialog(getString(R.string.photo_upload_fail));
                }
            }, new Response.Listener<String>() {

                @Override
                public void onResponse(String json) {

                    ParseUploadPostResponse2(json);
                    Log.d("postID========+++", String.valueOf(postId));

                }

            }, file, ReqConst.REQ_PARAM_FILE, params);

            reqMultiPart.setRetryPolicy(new DefaultRetryPolicy(
                    Constants.VOLLEY_TIME_OUT, 0,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            PicOneApplication.getInstance().addToRequestQueue(reqMultiPart, url);

        } catch (Exception e) {

            e.printStackTrace();
            _activity.closeProgress();
            _activity.showAlertDialog(getString(R.string.photo_upload_fail));
        }
    }

    public void ParseUploadPostResponse2(String json){

        _activity.closeProgress();

        try {

            JSONObject response = new JSONObject(json);

            Log.d("respons=========", String.valueOf(response));
            int result_code = response.getInt(ReqConst.RES_CODE);

            if (result_code == ReqConst.CODE_SUCCESS){

                _activity.showToast("Success your Post !");
            }
            else {
                _activity.showAlertDialog(getString(R.string.photo_upload_fail));
            }

        } catch (JSONException e){
            e.printStackTrace();
            _activity.showAlertDialog(getString(R.string.photo_upload_fail));
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        _activity = (MainActivity)context;
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.plus_sign:

                //_voteAlert = new VoteEntity();
                Constants.voteEntity = new VoteEntity();
                addTextDialog();
                break;

            case R.id.frame_left1:
                if (!hasPermissions(_activity, PERMISSIONS)){

                    ActivityCompat.requestPermissions(_activity, PERMISSIONS, MY_PEQUEST_CODE);
                }else {

                    order = 1;
                    selectPhoto();}

                break;

            case R.id.frame_right1:
                if (!hasPermissions(_activity, PERMISSIONS)){

                    ActivityCompat.requestPermissions(_activity, PERMISSIONS, MY_PEQUEST_CODE);
                }else {
                    order =2;
                    selectPhoto();}
                break;
        }
    }


    /////////////////////////////////////

    public void addTextDialog(){

        Constants.alert_userL = null;
        Constants.alert_userR = null;

        LayoutInflater inflater = _activity.getLayoutInflater();
        final View alertLayout = inflater.inflate(R.layout.custom_addtext_alert, null);

        ui_edtTitle = (EditText)alertLayout.findViewById(R.id.edt_title_add);

        imv_photoL_add_rec =  (ImageView)alertLayout.findViewById(R.id.imv_photoL_add_rec);
        imv_photoR_add_rec =  (ImageView)alertLayout.findViewById(R.id.imv_photoR_add_rec);

        FrameLayout frame_left = (FrameLayout)alertLayout.findViewById(R.id.frame_left1);
        frame_left.setOnClickListener(this);

        FrameLayout frame_right = (FrameLayout)alertLayout.findViewById(R.id.frame_right1);
        frame_right.setOnClickListener(this);

        ui_txvNext_text = (TextView)alertLayout.findViewById(R.id.txv_next_add);
        ui_txvBack = (TextView)alertLayout.findViewById(R.id.txv_back_add);

        // container
        LinearLayout lytContainer = (LinearLayout) alertLayout.findViewById(R.id.lyt_container);
        lytContainer.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // TODO Auto-generated method stub
                InputMethodManager imm = (InputMethodManager) _activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(ui_edtTitle.getWindowToken(), 0);
                return false;
            }
        });

        final android.support.v7.app.AlertDialog.Builder alert = new android.support.v7.app.AlertDialog.Builder(_activity);

        alert.setView(alertLayout);
        alert.setCancelable(false);

        final android.support.v7.app.AlertDialog dialog = alert.create() ;
        dialog.show();
        dialog.setCancelable(true);

        ui_txvNext_text.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                if (ui_edtTitle.getText().toString().length() == 0){

                    _activity.showAlertDialog(getString(R.string.enter_title));

                } else if ((_photo_left.length() ==0 && _photo_right.length()== 0)){

                    _activity.showAlertDialog(getString(R.string.select_photo));

                } else {

                    Constants.voteEntity.set_title(ui_edtTitle.getText().toString().trim());
                    addTimeDialog();
                    dialog.dismiss();}
            }
        });

        ui_txvBack.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                dialog.dismiss();

            }
        });

    }

    /*VoteMode Photo Action*/

    public void selectPhoto() {

        final String[] items = {"Take photo", "Choose from Gallery","Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(_activity);
        builder.setItems(items, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    doTakePhoto();

                } else if(item == 1){
                    doTakeGallery();

                } else {
                    return;
                }
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    public void doTakePhoto() {

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        /////////////// multi take photo ////////////////////

        long myCurrentTimeMillis = System.currentTimeMillis();
        String filename=String.valueOf(myCurrentTimeMillis)+".jpg";
        Log.d("filename===",filename);

        String picturePath = BitmapUtils.getTempFolderPath() + filename ;
        _imageCaptureUri = Uri.fromFile(new File(picturePath));
        Log.d("imagecaptureURI_1=====",String.valueOf(_imageCaptureUri));

      /*  String picturePath = BitmapUtils.getTempFolderPath() + "photo_temp.jpg";
        _imageCaptureUri = Uri.fromFile(new File(picturePath));*/

        intent.putExtra(MediaStore.EXTRA_OUTPUT, _imageCaptureUri);
        startActivityForResult(intent, Constants.PICK_FROM_CAMERA);

    }

    private void doTakeGallery() {

        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(MediaStore.Images.Media.CONTENT_TYPE);
        startActivityForResult(intent, Constants.PICK_FROM_ALBUM);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data){

        switch (requestCode){

            case Constants.CROP_FROM_CAMERA: {

                if (resultCode == RESULT_OK){
                    try {

                        File saveFile = BitmapUtils.getOutputMediaFile(_activity);

                        InputStream in = _activity.getContentResolver().openInputStream(Uri.fromFile(saveFile));
                        BitmapFactory.Options bitOpt = new BitmapFactory.Options();
                        Bitmap bitmap = BitmapFactory.decodeStream(in, null, bitOpt);
                        in.close();



                        //set The bitmap data to image View

                        if(order==1 ){

                            imv_photoL_add_rec.setImageBitmap(bitmap);
                            //_photoPath = saveFile.getAbsolutePath();
                            _photo_left = _photoPath ;

                            Log.d("=========photoPath=======", _photoPath);

                            //_voteAlert.set_votePhotoUrl_L(_photoPath);
                            Constants.voteEntity.set_votePhotoUrl_L(_photoPath);
                            Constants.alert_userL = bitmap ;
                        }
                        else if(order==2){

                            imv_photoR_add_rec.setImageBitmap(bitmap);

                            _photo_right = _photoPath ;

                            Log.d("======right====photoPath=====", _photoPath);
                            //_voteAlert.set_votePhotoUrl_R(_photoPath);
                            Constants.voteEntity.set_votePhotoUrl_R(_photoPath);
                            Constants.alert_userR = bitmap ;
                        }
                        _photoPath = saveFile.getAbsolutePath();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
            }
            case Constants.PICK_FROM_ALBUM:

                if (resultCode == RESULT_OK){
                    _imageCaptureUri = data.getData();
                }

            case Constants.PICK_FROM_CAMERA:
            {
                try {

                    _photoPath = BitmapUtils.getRealPathFromURI(_activity, _imageCaptureUri);

                    Intent intent = new Intent("com.android.camera.action.CROP");
                    intent.setDataAndType(_imageCaptureUri, "image/*");

                    intent.putExtra("crop", true);
                    intent.putExtra("scale", true);
                    intent.putExtra("outputX", Constants.PROFILE_IMAGE_SIZE);
                    intent.putExtra("outputY", Constants.PROFILE_IMAGE_SIZE);
                    intent.putExtra("aspectX", 1);
                    intent.putExtra("aspectY", 1);
                    intent.putExtra("noFaceDetection", true);
                    //intent.putExtra("return-data", true);
                    intent.putExtra("output", Uri.fromFile(BitmapUtils.getOutputMediaFile(_activity)));

                    startActivityForResult(intent, Constants.CROP_FROM_CAMERA);

                }catch (Exception e){
                    e.printStackTrace();
                }

        /*        if(order==1 ){

                   // imv_photoL_add_rec.setImageBitmap(bitmap);
                    imv_photoL_add_rec.setImageURI();
                    //_photoPath = saveFile.getAbsolutePath();
                    _photo_left = _photoPath ;

                    Log.d("=========photoPath=======", _photoPath);

                    //_voteAlert.set_votePhotoUrl_L(_photoPath);
                    Constants.voteEntity.set_votePhotoUrl_L(_photoPath);
                    Constants.alert_userL = bitmap ;
                }
                else if(order==2){

                    imv_photoR_add_rec.setImageBitmap(bitmap);

                    _photo_right = _photoPath ;

                    Log.d("======right====photoPath=====", _photoPath);
                    //_voteAlert.set_votePhotoUrl_R(_photoPath);
                    Constants.voteEntity.set_votePhotoUrl_R(_photoPath);
                    Constants.alert_userR = bitmap ;
                }*/





                break;
            }
        }
    }


/*//////////////////////////////////////////////////////////////////////////////////////////*/
    public void addTimeDialog(){

        LayoutInflater inflater = _activity.getLayoutInflater();
        final View alertLayout = inflater.inflate(R.layout.custom_addtime_aler, null);

        ui_edtDescription = (EditText)alertLayout.findViewById(R.id.edt_description);
        Constants.voteEntity.set_description(ui_edtDescription.getText().toString().trim());

        ui_txvNext_text = (TextView)alertLayout.findViewById(R.id.txv_next_time);
        ui_txvNext_text.setOnClickListener(this);
        ui_txvDate_date = (TextView)alertLayout.findViewById(R.id.txv_curr_time);

        ui_txvDate_date = (TextView)alertLayout.findViewById(R.id.txv_curr_date) ;
        ui_txvDate_date.setOnClickListener(this);

        ui_txvTime_time = (TextView)alertLayout.findViewById(R.id.txv_curr_time) ;
        ui_txvTime_time.setOnClickListener(this);

        ui_txvNext_time = (TextView)alertLayout.findViewById(R.id.txv_next_time);
        ui_txvCancel_time = (TextView)alertLayout.findViewById(R.id.txv_cancel_time);

        /*spinner*/
        spinner = (Spinner)alertLayout.findViewById(R.id.spinner_public) ;
        _adpterPublic = new PublicAlertAdapter(_activity);
        spinner.setAdapter(_adpterPublic);

    /*    if (Constants.private_status == 0) {

            spinner.setSelection(1);

            //spinner.setPrompt("Private");
        }*/

        final android.support.v7.app.AlertDialog.Builder alert = new android.support.v7.app.AlertDialog.Builder(_activity);

        alert.setView(alertLayout);
        alert.setCancelable(false);

        final android.support.v7.app.AlertDialog dialog = alert.create() ;
        dialog.show();
        dialog.setCancelable(true);

        ui_txvNext_text.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                if (ui_edtDescription.getText().toString().length() == 0){

                    _activity.showAlertDialog(getString(R.string.enter_description));

                }else {

                    String description = ui_edtDescription.getText().toString().trim();
                    Constants.voteEntity.set_description(description);
                    Constants.voteEntity.set_private_status(Constants.private_status);
                priviewDialog();
                dialog.dismiss();}

            }
        });

        ui_txvCancel_time.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                dialog.dismiss();

            }
        });

        ui_txvTime_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showTimePickerDialog(view);
            }
        });

        ui_txvDate_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showDatePickerDialog(view);
            }
        });

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {

                if (position == 1 && Constants.private_status == 1) {

                    Constants.private_status = 0; //private_status = 0 : public = 1:
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {}
        });
    }

    public  class TimePickerFragment extends DialogFragment
            implements TimePickerDialog.OnTimeSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current time as the default values for the picker
            final Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);

            // Create a new instance of TimePickerDialog and return it
            return new TimePickerDialog(getActivity(), this, hour, minute,
                    DateFormat.is24HourFormat(getActivity()));
        }
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            // Do something with the time chosen by the user
            populateSetTime(hourOfDay, minute);
        }

        public void populateSetTime(int hourofDay, int minute) {
            String time = String.valueOf(hourofDay)+ ":" + String.valueOf(minute) ;
            ui_txvTime_time.setText(time);
            //_voteAlert.set_time(date);
            Constants.voteEntity.set_time(time);
        }
    }

    public void showTimePickerDialog(View v) {
        DialogFragment newFragment = new NewsfeedsFragment.TimePickerFragment();
        newFragment.show(_activity.getFragmentManager(), "timePicker");
    }

    public class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);
            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int yy, int mm, int dd) {populateSetDate(yy, mm+1, dd);}

        public void populateSetDate(int year, int month, int day) {
            String date = String.valueOf(year)+ "/" + String.valueOf(month) + "/" + String.valueOf(day);
            ui_txvDate_date.setText(date);
            Constants.voteEntity.set_date(date);
            //year1 = String.valueOf(year)+ "/" + String.valueOf(month) + "/" + String.valueOf(day);
        }
    }

    public void showDatePickerDialog(View v) {
        DialogFragment newFragment = new NewsfeedsFragment.DatePickerFragment();
        newFragment.show(_activity.getFragmentManager(), "datePicker");
    }

    public void priviewDialog(){

        LayoutInflater inflater = _activity.getLayoutInflater();
        final View alertLayout = inflater.inflate(R.layout.custom_priview_alert, null);

        ImageView imv_photoL_pre_rec = (ImageView)alertLayout.findViewById(R.id.imv_photoL_pre_rec);
        ImageView imv_photoR_pre_rec = (ImageView)alertLayout.findViewById(R.id.imv_photoR_pre_rec);

        Log.d("image : ", String.valueOf(Constants.alert_userR));

        txv_description_pre = (TextView)alertLayout.findViewById(R.id.txv_description_pre);
        txv_title_pre = (TextView)alertLayout.findViewById(R.id.txv_title_pre);

        Glide.with(_activity).load(Constants.voteEntity.get_votePhotoUrl_L()).placeholder(R.drawable.image_user).into(imv_photoL_pre_rec);
        Glide.with(_activity).load(Constants.voteEntity.get_votePhotoUrl_R()).placeholder(R.drawable.image_user).into(imv_photoR_pre_rec);

        txv_description_pre.setText(Constants.voteEntity.get_description());

        txv_title_pre = (TextView)alertLayout.findViewById(R.id.txv_title_pre);
        txv_title_pre.setText(Constants.voteEntity.get_title());

        ui_txvPublish = (TextView)alertLayout.findViewById(R.id.txv_publish);
        ui_txvCancel_pre = (TextView)alertLayout.findViewById(R.id.txv_cancel_pre);

        final android.support.v7.app.AlertDialog.Builder alert = new android.support.v7.app.AlertDialog.Builder(_activity);

        alert.setView(alertLayout);
        alert.setCancelable(false);

        final android.support.v7.app.AlertDialog dialog = alert.create() ;
        dialog.show();
        dialog.setCancelable(true);

        ui_txvPublish.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                shareDialog();
                dialog.dismiss();
            }
        });

        ui_txvCancel_pre.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                dialog.dismiss();

            }
        });
    }

    private void shareDialog() {

        LayoutInflater inflater = _activity.getLayoutInflater();
        final View alertLayout = inflater.inflate(R.layout.custom_share_alert, null);

        ui_txvPost = (TextView)alertLayout.findViewById(R.id.txv_post_share);
        ui_txvPost.setOnClickListener(this);
        ui_txvCancel_share = (TextView)alertLayout.findViewById(R.id.txv_cancel_share);
        ui_imvFace = (ImageView)alertLayout.findViewById(R.id.imv_facebook_share);
        ui_imvGoogle = (ImageView)alertLayout.findViewById(R.id.imv_google_share);
        ui_imvTwitter = (ImageView)alertLayout.findViewById(R.id.imv_twitter_share);
        ui_imvApp = (ImageView)alertLayout.findViewById(R.id.imv_app_share);

        final android.support.v7.app.AlertDialog.Builder alert = new android.support.v7.app.AlertDialog.Builder(_activity);

        alert.setView(alertLayout);
        alert.setCancelable(false);

        final android.support.v7.app.AlertDialog dialog = alert.create() ;
        dialog.show();
        dialog.setCancelable(true);

        ui_txvPost.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                Constants.private_status = 1;  //private_status = 0 : public = 1:
                Constants.voteEntity.set_votePercentR(0);
                Constants.voteEntity.set_votePercentL(0);
                Constants.voteEntity.set_userphotoUrl(Commons.g_user.get_photoUrl());

           /*     Log.d("voteentity=====", Constants.voteEntity.get_votePhotoUrl_L());
                Log.d("title=====", Constants.voteEntity.get_title());
                Log.d("description===+++++++==", Constants.voteEntity.get_description());
                Log.d("voteentity++++=====", Constants.voteEntity.get_votePhotoUrl_R());*/

                _adapter.addItem(Constants.voteEntity);
                _adapter.notifyDataSetChanged();
                uploadPost1();

                dialog.dismiss();
            }
        });

        ui_txvCancel_share.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                dialog.dismiss();

            }
        });

        ui_imvFace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ui_imvFace.setSelected(!ui_imvFace.isSelected());

                if (ui_imvFace.isSelected()){

                    postByFaceBook();
                }
            }
        });

        ui_imvGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ui_imvGoogle.setSelected(!ui_imvGoogle.isSelected());

                share_image_text_GPLUS();

            }
        });

        ui_imvTwitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ui_imvTwitter.setSelected(!ui_imvTwitter.isSelected());
                sendTweet();
            }
        });

        ui_imvApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ui_imvApp.setSelected(!ui_imvApp.isSelected());
            }
        });

    }

    public void postByFaceBook(){

        List<Intent> targetedShareIntents = new ArrayList<Intent>();

        Intent facebookIntent = getShareIntent("facebook", "Subject", "Photos");
        // subject may not work, but if you have a url place it in text_or_url
        if(facebookIntent != null) targetedShareIntents.add(facebookIntent);

        try {

            Intent chooser = Intent.createChooser(targetedShareIntents.remove(0), "Delen");
            chooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetedShareIntents.toArray(new Parcelable[]{}));

            startActivity(chooser);
        }catch (Exception e){

            _activity.showAlertDialog("Facebook app is not installed on your phone.\nIf you want invite friends from Facebook, please install Facebook app");
        }
    }

    private Intent getShareIntent(String type, String subject, String text)
    {
        boolean found = false;
        Intent share = new Intent(android.content.Intent.ACTION_SEND);
        share.setType("image/jpeg");

        // gets the list of intents that can be loaded.
        List<ResolveInfo> resInfo = _activity.getPackageManager().queryIntentActivities(share, 0);
        System.out.println("resinfo: " + resInfo);

        if (!resInfo.isEmpty()){

            for (ResolveInfo info : resInfo) {

                if (info.activityInfo.packageName.toLowerCase().contains(type) || info.activityInfo.name.toLowerCase().contains(type) ) {

                    //share.putExtra(Intent.EXTRA_STREAM,Uri.fromFile(new File(Environment.getExternalStorageDirectory()+ "/Temp" + "/photo_temp.jpg")));  //optional//use this when you want to send an image
                    share.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(new File(Constants.voteEntity.get_votePhotoUrl_L())));  //optional//use this when you want to send an image
                    //share.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(new File(Constants.voteEntity.get_votePhotoUrl_R())));  //optional//use this when you want to send an image
                    share.putExtra(Intent.EXTRA_TITLE,  Constants.voteEntity.get_title());
                    share.putExtra(Intent.EXTRA_SUBJECT, Constants.voteEntity.get_description());
                    share.putExtra(Intent.EXTRA_TEXT, "Test");
                    //share.putExtra(Intent.EXTRA_STREAM, Constants.voteEntity.get_votePercentR());

                    startActivity(Intent.createChooser(share, "TestPOST"));
                    share.setPackage(info.activityInfo.packageName);

                    found = true;


                    Log.d("========title=====", Constants.voteEntity.get_title());
                    break;
                }
            }

            if (!found) return null;

            return share;
        }

        return null;
    }

    public void share_image_text_GPLUS() {

        try {

            Intent googlePlusIntent = new Intent(Intent.ACTION_SEND);

            String filename = "googleplus_image.jpg";
            File imageFile = new File(Environment.getExternalStorageDirectory(), filename);

            googlePlusIntent.putExtra(Intent.EXTRA_TEXT, Constants.voteEntity.get_title());
            googlePlusIntent.putExtra(Intent.EXTRA_SUBJECT, Constants.voteEntity.get_description());
            googlePlusIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(new File(Constants.voteEntity.get_votePhotoUrl_L())));
            googlePlusIntent.setType("image/jpeg");
            PackageManager pm = getActivity().getPackageManager();

            List<ResolveInfo> lract = pm.queryIntentActivities(googlePlusIntent, PackageManager.MATCH_DEFAULT_ONLY);

            boolean resolved = false;

            for (ResolveInfo ri : lract) {
                if (ri.activityInfo.name.contains("googleplus")) {
                    googlePlusIntent.setClassName(ri.activityInfo.packageName,
                            ri.activityInfo.name);

                    resolved = true;
                    break;
                }
            }

            startActivity(resolved ? googlePlusIntent : Intent.createChooser(googlePlusIntent, "Choose one"));

        } catch (final ActivityNotFoundException e) {
            Toast.makeText(getActivity(), "You don't seem to have google+ installed on this device", Toast.LENGTH_SHORT).show();
        }
    }

    public void sendTweet() {

        try {

            Intent tweetIntent = new Intent(Intent.ACTION_SEND);

            String filename = "twitter_image.jpg";
            File imageFile = new File(Environment.getExternalStorageDirectory(), filename);

            tweetIntent.putExtra(Intent.EXTRA_TEXT, Constants.voteEntity.get_title());
            tweetIntent.putExtra(Intent.EXTRA_SUBJECT, Constants.voteEntity.get_description());
            tweetIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(new File(Constants.voteEntity.get_votePhotoUrl_L())));
            tweetIntent.setType("image/jpeg");
            PackageManager pm = getActivity().getPackageManager();

            List<ResolveInfo> lract = pm.queryIntentActivities(tweetIntent, PackageManager.MATCH_DEFAULT_ONLY);

            boolean resolved = false;

            for (ResolveInfo ri : lract) {
                if (ri.activityInfo.name.contains("twitter")) {
                    tweetIntent.setClassName(ri.activityInfo.packageName,
                            ri.activityInfo.name);

                    resolved = true;
                    break;
                }
            }

            startActivity(resolved ? tweetIntent : Intent.createChooser(tweetIntent, "Choose one"));

        } catch (final ActivityNotFoundException e) {
            Toast.makeText(getActivity(), "You don't seem to have twitter installed on this device", Toast.LENGTH_SHORT).show();
        }

    }
}
