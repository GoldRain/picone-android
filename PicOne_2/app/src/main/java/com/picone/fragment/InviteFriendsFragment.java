package com.picone.fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.picone.R;
import com.picone.activity.MainActivity;
import com.picone.base.BaseFragment;


public class InviteFriendsFragment extends BaseFragment {

    MainActivity activity;
    View view;

    public InviteFriendsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_invite_friends, container, false);

        loadLayout();
        Share("", "");
        return view ;
    }

    private void loadLayout() {

    }

    public void Share( String subject, String text) {


        try {
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("image/jpeg");
            intent.putExtra(Intent.EXTRA_SUBJECT, subject);
            intent.putExtra(Intent.EXTRA_TEXT , text);
            startActivity(intent);
        } catch (android.content.ActivityNotFoundException e){
            e.printStackTrace();
        } catch (java.lang.NullPointerException e){
            e.printStackTrace();
        }
    }

}
