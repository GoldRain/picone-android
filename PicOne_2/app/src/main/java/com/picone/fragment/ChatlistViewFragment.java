package com.picone.fragment;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.picone.R;
import com.picone.activity.MainActivity;
import com.picone.chatting.ChatlistViewAdapter;
import com.picone.base.BaseFragment;
import com.picone.commons.Commons;
import com.picone.model.RoomEntity;

import java.util.ArrayList;

public class ChatlistViewFragment extends BaseFragment implements View.OnClickListener {

    MainActivity _activity;
    View view ;
    ChatlistViewAdapter _adapter;
    ListView ui_lstContainer;
    ArrayList<RoomEntity> _rooms = new ArrayList<>();
    ChatlistViewFragment fragment ;

    public ChatlistViewFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view =  inflater.inflate(R.layout.fragment_chatlist, container, false);
        loadLayout();
        return view ;
    }

    private void loadLayout() {

        _rooms = Commons.g_user.get_roomList();
        Log.d("room count :", String.valueOf(_rooms.size()));
        //fragment = new ChatlistViewFragment();

        ui_lstContainer = (ListView)view.findViewById(R.id.lst_chat_fragment);
        _adapter = new ChatlistViewAdapter(this);
        ui_lstContainer.setAdapter(_adapter);
        _adapter.refresh();

    }

    @Override
    public void onClick(View view) {

    }

    public void refresh(){

        _adapter.refresh();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        _activity = (MainActivity)context;
    }

    @Override
    public void onResume() {

        refresh();
        super.onResume();
    }

}
