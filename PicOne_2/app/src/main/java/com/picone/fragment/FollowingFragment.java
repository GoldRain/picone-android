package com.picone.fragment;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.picone.PicOneApplication;
import com.picone.R;
import com.picone.activity.MainActivity;
import com.picone.adapter.FollowingAdapter;
import com.picone.base.BaseFragment;
import com.picone.commons.Commons;
import com.picone.commons.Constants;
import com.picone.commons.ReqConst;
import com.picone.model.Friendmodel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class FollowingFragment extends BaseFragment implements SwipyRefreshLayout.OnRefreshListener{

    MainActivity _activity;
    View view ;
    FollowingAdapter _adapter;
    ListView lst_following;
    SwipyRefreshLayout ui_RefreshLayout;

    ArrayList<Friendmodel> _following = new ArrayList<>();

    int _curPage = 1;
    int refreshDirection = 0; // refreshDirection 1: Bottom, -1: Top

    Friendmodel _friendmodel;

    public FollowingFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_following, container, false);

        loadLayout();
        return view ;
    }

    private void loadLayout() {

        ui_RefreshLayout = (SwipyRefreshLayout)view.findViewById(R.id.swipe_refresh_layout_following);
        ui_RefreshLayout.setOnRefreshListener(this);
        ui_RefreshLayout.setDirection(SwipyRefreshLayoutDirection.BOTH);

        lst_following = (ListView)view.findViewById(R.id.lst_following);
        _adapter = new FollowingAdapter(_activity);
        lst_following.setAdapter(_adapter);

        _curPage = 1;

        ui_RefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                getAllFollowing();
            }
        });
    }

    private void getAllFollowing() {

        String url = ReqConst.SERVER_URL + ReqConst.REQ_GETALLFOLLOWING ;
        int _idx = Commons.g_user.get_idx();

        String params = String.format("/%d/%d", _idx, _curPage);

        url += params ;

        Log.d("======url=========", url);

        ui_RefreshLayout.setRefreshing(true);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {
                parseGetAllFollowing(json);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                ui_RefreshLayout.setRefreshing(false);
                _activity.showAlertDialog(getString(R.string.error));
            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT,0,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        PicOneApplication.getInstance().addToRequestQueue(stringRequest, url);
    }

    private void parseGetAllFollowing(String json) {

        ui_RefreshLayout.setRefreshing(false);
        Log.d("respons=========", json);

        try {

            JSONObject response = new JSONObject(json);
            int result_code = response.getInt(ReqConst.RES_CODE);

            if (result_code == ReqConst.CODE_SUCCESS){

                JSONArray followings = response.getJSONArray(ReqConst.RES_FOLLOWINGINFOS);

                if (followings.length() > 0 && refreshDirection == 1){

                    _curPage++ ;
                }

                _following.clear();  // current page clear

                for (int i = 0 ; i < followings.length(); i++){

                    JSONObject jsonFollowing = (JSONObject) followings.get(i);

                    Friendmodel following = new Friendmodel();

                    following.setId(jsonFollowing.getInt(ReqConst.RES_USERID));
                    following.setFriendphotourl(jsonFollowing.getString(ReqConst.RES_PHOTO_URL));
                    following.setFriendname(jsonFollowing.getString(ReqConst.RES_USERNAME));

                    if (Commons.g_user.get_idx() == jsonFollowing.getInt("userid")){ continue; }

                    _following.add(following);

                    Log.d("Following=========model=====", jsonFollowing.getString(ReqConst.RES_USERNAME));
                    Log.d("PHOOTOTO=======url=========", jsonFollowing.getString("photo_url"));
                }

                _adapter.setFollowingDatas(_following);
                _adapter.notifyDataSetChanged();

            } else {
                ui_RefreshLayout.setRefreshing(false);
                _activity.showAlertDialog(getString(R.string.error));
            }
        } catch (JSONException e) {
            e.printStackTrace();
            ui_RefreshLayout.setRefreshing(false);
            _activity.showAlertDialog(getString(R.string.error));
        }
    }

    @Override
    public void onRefresh(SwipyRefreshLayoutDirection direction) {

        Log.d("_currentPage==========", String.valueOf(_curPage));
        if (direction == SwipyRefreshLayoutDirection.BOTTOM){
//            _curPage++ ;
            refreshDirection = 1;
            getAllFollowing();

        } else if (direction == SwipyRefreshLayoutDirection.TOP && _curPage > 1){

            _curPage-- ;
            refreshDirection = -1;
            getAllFollowing();

        } else if (_curPage == 1){

            ui_RefreshLayout.setRefreshing(false);

            return;
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        _activity = (MainActivity)context;
    }
}
