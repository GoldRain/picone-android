package com.picone.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;

import com.picone.R;
import com.picone.activity.MainActivity;
import com.picone.adapter.PrivateListViewAdapter;
import com.picone.base.BaseFragment;
import com.picone.commons.Constants;
import com.picone.model.Friendmodel;

import java.util.ArrayList;

public class PrivateFragment extends BaseFragment {

    MainActivity _activity;
    NewsfeedsFragment _fragment = new NewsfeedsFragment() ;
    View view ;

    PrivateListViewAdapter adapter_private;
    ListView ui_lstPrivate;
    FloatingActionButton ui_fabAdd;
    ArrayList<Friendmodel>friendmodels=new ArrayList<>();


    public PrivateFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view =  inflater.inflate(R.layout.fragment_private, container, false);

        loadLayout();
        return view ;
    }

    private void loadLayout() {

        for(int i=0; i<20; i++){
            Friendmodel friendmodel=new Friendmodel();
            friendmodel.setFriendname("Steve Markson");
            friendmodel.setCheckstatuse(0);
            friendmodel.setId(i);
            friendmodels.add(friendmodel);
        }

        ui_fabAdd = (FloatingActionButton)view.findViewById(R.id.fab_addFriends) ;

        ui_lstPrivate = (ListView)view.findViewById(R.id.lst_container_pri);
        adapter_private = new PrivateListViewAdapter(_fragment, _activity, friendmodels);
        ui_lstPrivate.setAdapter(adapter_private);

        ui_fabAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //_activity.selectDrawerItem(0);

                Constants.private_status = 1;
            }
        });

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        _activity = (MainActivity)context ;

    }

}
