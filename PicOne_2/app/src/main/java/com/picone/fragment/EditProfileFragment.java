package com.picone.fragment;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.picone.PicOneApplication;
import com.picone.R;
import com.picone.activity.CountrySpinnerEditAdapter;
import com.picone.activity.MainActivity;
import com.picone.adapter.PhonenumberAdapter;
import com.picone.base.BaseFragment;
import com.picone.commons.Commons;
import com.picone.commons.Constants;
import com.picone.commons.ReqConst;
import com.picone.model.UserEntity;
import com.picone.preference.PrefConst;
import com.picone.preference.Preference;
import com.picone.utils.BitmapUtils;
import com.picone.utils.MultiPartRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import static android.app.Activity.RESULT_OK;
import static com.picone.activity.MainActivity.MY_PEQUEST_CODE;

public class EditProfileFragment extends BaseFragment implements View.OnClickListener {

    MainActivity _activity;
    View view ;

    EditText edt_firstname_edt,edt_lastname_edt,edt_username_edt,edt_email_edt,
            edt_pwd_edt, edt_mobNumber_edt,edt_add_edt,edt_inter_edt,edt_status_edt;

    TextView ui_txvDone;
    ImageView ui_imvPhoto;

    private Uri _imageCaptureUri;
    String _photoPath = "";
    private int _idx = 0;

    CountrySpinnerEditAdapter _adapter;
    Spinner ui_spinner_country;

    PhonenumberAdapter adapterPhone;
    Spinner ui_spinner_phone ;


    int facebook = Constants.facebook_login;
    int google = Constants.google_login;
    int twitter = Constants.twitter_login;


    String[] PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE,  Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};


    public EditProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view =  inflater.inflate(R.layout.fragment_edit_profile, container, false);

        loadLayout();
        return view ;
    }

    private void loadLayout() {

        edt_firstname_edt = (EditText)view.findViewById(R.id.edt_firstname_edt);
        edt_lastname_edt = (EditText)view.findViewById(R.id.edt_lastname_edt);
        edt_username_edt = (EditText)view.findViewById(R.id.edt_username_edt);
        edt_email_edt = (EditText)view.findViewById(R.id.edt_email_edt);
        edt_pwd_edt = (EditText)view.findViewById(R.id.edt_pwd_edt);
        edt_mobNumber_edt = (EditText)view.findViewById(R.id.edt_mobNumber_edt);
        edt_add_edt = (EditText)view.findViewById(R.id.edt_add_edt);
        edt_inter_edt = (EditText)view.findViewById(R.id.edt_inter_edt);
        edt_status_edt = (EditText)view.findViewById(R.id.edt_status_edt);

        ui_txvDone = (TextView)view.findViewById(R.id.txv_done_edt);
        ui_txvDone.setOnClickListener(this);

        ui_imvPhoto = (ImageView)view.findViewById(R.id.imv_photo_edt);
        ui_imvPhoto.setOnClickListener(this);

        _adapter = new CountrySpinnerEditAdapter(_activity);
        ui_spinner_country = (Spinner)view.findViewById(R.id.spinner_country_edt);
        ui_spinner_country.setAdapter(_adapter);

        ui_spinner_phone = (Spinner)view.findViewById(R.id.spinner_phon_edt);
        adapterPhone = new PhonenumberAdapter(_activity);
        ui_spinner_phone.setAdapter(adapterPhone);

        checkSocialLogin();
        setDate();

    }

    public void checkSocialLogin(){

        if (facebook != 0 || google != 0 || twitter != 0){

            edt_firstname_edt.setEnabled(false);
            edt_firstname_edt.setClickable(false);

            edt_lastname_edt.setEnabled(false);
            edt_lastname_edt.setClickable(false);

            edt_email_edt.setEnabled(false);
            edt_email_edt.setClickable(false);

            edt_username_edt.setEnabled(false);
            edt_username_edt.setClickable(false);

            edt_pwd_edt.setEnabled(false);
            edt_pwd_edt.setClickable(false);

        } else {

            edt_firstname_edt.setEnabled(true);
            edt_firstname_edt.setClickable(true);

            edt_lastname_edt.setEnabled(true);
            edt_lastname_edt.setClickable(true);

            edt_email_edt.setEnabled(true);
            edt_email_edt.setClickable(true);

            edt_pwd_edt.setEnabled(true);
            edt_pwd_edt.setClickable(true);
        }
    }

    private void setDate(){

        Glide.with(this).load(Commons.g_user.get_photoUrl()).placeholder(R.drawable.image_me).into(ui_imvPhoto);
        edt_firstname_edt.setText(Commons.g_user.get_firstName());
        edt_lastname_edt.setText(Commons.g_user.get_lastName());
        edt_username_edt.setText(Commons.g_user.get_userName());
        edt_email_edt.setText(Commons.g_user.get_email());
        edt_pwd_edt.setText(Commons.g_user.get_password());
        edt_mobNumber_edt.setText(Commons.g_user.get_phoneNumber());

        edt_add_edt.setText(Commons.g_user.get_adress());
        edt_inter_edt.setText(Commons.g_user.get_interests());
        edt_status_edt.setText(Commons.g_user.get_status());

    }

    private boolean checkValue(){

        if (facebook != 0 || google != 0 || twitter != 0){

            _activity.showAlertDialog("You can't edit some of your profile");

            if (edt_mobNumber_edt.getText().length() == 0) {
                _activity.showAlertDialog(getString(R.string.enter_mobilenumber));
                return false;

            } else if (edt_add_edt.getText().toString().length() == 0) {

                _activity.showAlertDialog(getString(R.string.enter_address));
                return false;

            } else if (edt_inter_edt.getText().toString().length() == 0) {
                _activity.showAlertDialog(getString(R.string.enter_interest));
                return false;

            } else if (edt_status_edt.getText().toString().length() == 0) {

                _activity.showAlertDialog(getString(R.string.enter_status));
                return false;
            }

        } else {

            if (edt_firstname_edt.getText().length() == 0) {
                _activity.showAlertDialog(getString(R.string.enter_firstname));
                return false;

            } else if (edt_lastname_edt.getText().length() == 0 ) {
                _activity.showAlertDialog(getString(R.string.enter_lastname));
                return false;

            } else if (edt_username_edt.getText().length() == 0) {
                _activity.showAlertDialog(getString(R.string.enter_username));
                return false;

            } else if (edt_email_edt.getText().toString().length() == 0
                    && !edt_email_edt.getText().toString().endsWith("@gmail.com")) {

                _activity.showAlertDialog(getString(R.string.enter_email));
                return false;

            } else if (edt_pwd_edt.getText().length() == 0 ) {
                _activity.showAlertDialog(getString(R.string.enter_pwd));
                return false;

            } else if (edt_mobNumber_edt.getText().length() == 0) {
                _activity.showAlertDialog(getString(R.string.enter_mobilenumber));
                return false;

            } else if (edt_add_edt.getText().toString().length() == 0) {

                _activity.showAlertDialog(getString(R.string.enter_address));
                return false;

            } else if (edt_inter_edt.getText().toString().length() == 0) {
                _activity.showAlertDialog(getString(R.string.enter_interest));
                return false;

            } else if (edt_status_edt.getText().toString().length() == 0) {

                _activity.showAlertDialog(getString(R.string.enter_status));
                return false;
            }
        }

        return true;

    }

    public void selectPhoto() {

        if (facebook == 0 && google == 0 && twitter == 0) {


            final String[] items = {"Take photo", "Choose from Gallery", "Cancel"};

            AlertDialog.Builder builder = new AlertDialog.Builder(_activity);
            builder.setItems(items, new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int item) {
                    if (item == 0) {
                        doTakePhoto();

                    } else if (item == 1) {
                        doTakeGallery();

                    } else {
                        return;
                    }
                }
            });

            AlertDialog alert = builder.create();
            alert.show();

        } else {

            _activity.showAlertDialog("You can't edit some of your profile");
            return;
        }
    }

    public void doTakePhoto(){

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        String picturePath = BitmapUtils.getTempFolderPath() + "photo_temp.jpg";
        _imageCaptureUri = Uri.fromFile(new File(picturePath));

        intent.putExtra(MediaStore.EXTRA_OUTPUT, _imageCaptureUri);
        startActivityForResult(intent, Constants.PICK_FROM_CAMERA);

    }

    private void doTakeGallery(){

        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(MediaStore.Images.Media.CONTENT_TYPE);
        startActivityForResult(intent, Constants.PICK_FROM_ALBUM);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data){

        switch (requestCode){

            case Constants.CROP_FROM_CAMERA: {

                if (resultCode == RESULT_OK){
                    try {

                        File saveFile = BitmapUtils.getOutputMediaFile(_activity);

                        InputStream in = _activity.getContentResolver().openInputStream(Uri.fromFile(saveFile));
                        BitmapFactory.Options bitOpt = new BitmapFactory.Options();
                        Bitmap bitmap = BitmapFactory.decodeStream(in, null, bitOpt);
                        in.close();

                        //set The bitmap data to image View
                        ui_imvPhoto.setImageBitmap(bitmap);

                        _photoPath = saveFile.getAbsolutePath();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
            }
            case Constants.PICK_FROM_ALBUM:

                if (resultCode == RESULT_OK){
                    _imageCaptureUri = data.getData();
                }

            case Constants.PICK_FROM_CAMERA:
            {
                try {

                    _photoPath = BitmapUtils.getRealPathFromURI(_activity, _imageCaptureUri);

                    Intent intent = new Intent("com.android.camera.action.CROP");
                    intent.setDataAndType(_imageCaptureUri, "image/*");

                    intent.putExtra("crop", true);
                    intent.putExtra("scale", true);
                    intent.putExtra("outputX", Constants.PROFILE_IMAGE_SIZE);
                    intent.putExtra("outputY", Constants.PROFILE_IMAGE_SIZE);
                    intent.putExtra("aspectX", 1);
                    intent.putExtra("aspectY", 1);
                    intent.putExtra("noFaceDetection", true);
                    //intent.putExtra("return-data", true);
                    intent.putExtra("output", Uri.fromFile(BitmapUtils.getOutputMediaFile(_activity)));

                    startActivityForResult(intent, Constants.CROP_FROM_CAMERA);
                }catch (Exception e){
                    e.printStackTrace();
                }
                break;
            }
        }
    }


    private void progressEdit() {

        final String countryname = Constants.countryname;

       /* Toast.makeText(_activity, countryname, Toast.LENGTH_SHORT).show();*/

        _activity.showProgress();

        String url = ReqConst.SERVER_URL + ReqConst.REQ_EDIT ;
        StringRequest myRequest = new StringRequest(
                Request.Method.POST, url ,new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                Log.d("response=====", String.valueOf(response));
                parseEditResponse(response);
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("error============", String.valueOf(error));
                        _activity.closeProgress();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                try {
                    params.put("id",String.valueOf(Commons.g_user.get_idx()));
                    params.put("firstname",edt_firstname_edt.getText().toString());
                    params.put("lastname", edt_lastname_edt.getText().toString());
                    params.put("username", edt_username_edt.getText().toString());
                    params.put("email",edt_email_edt.getText().toString());
                    params.put("password", edt_pwd_edt.getText().toString());
                    params.put("phone",edt_mobNumber_edt.getText().toString());
                    params.put("country",String.valueOf(countryname));
                    params.put("address", edt_add_edt.getText().toString());
                    params.put("interests",edt_inter_edt.getText().toString());
                    params.put("status", edt_status_edt.getText().toString());

                } catch (Exception e) {}
                return params;
            }
        };

        myRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        PicOneApplication.getInstance().addToRequestQueue(myRequest,url);  // url or "tag"
    }

    private void parseEditResponse(String response) {

        Log.d("respons", response);

        try {

            JSONObject object = new JSONObject(response);

            int result_code = object.getInt(ReqConst.RES_CODE);


            if (result_code == ReqConst.CODE_SUCCESS) {

                /*showToast("Success change your profile !");*/

                _activity.closeProgress();

                if(_photoPath.length() == 0) {

                    ParseUploadImageResponse(response);

                }else{
                    uploadImage();
                }
            }

        } catch (JSONException e) {

            e.printStackTrace();

            _activity.showAlertDialog("Network Error. Please Try Again.");
            _activity.closeProgress();
        }

    }

    private void uploadImage() {

        try {
            Log.d("photo==========",_photoPath);
            File file = new File(_photoPath);

            Map<String, String> params = new HashMap<>();

            Log.d("id============", String.valueOf(Commons.g_user.get_idx()));

            params.put(ReqConst.REQ_PARAM_ID , String.valueOf(Commons.g_user.get_idx()));

            String url = ReqConst.SERVER_URL + ReqConst.REQ_UPLOADPHOTO;

            MultiPartRequest reqMultiPart = new MultiPartRequest(url, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    _activity.closeProgress();
                    _activity.showAlertDialog(getString(R.string.photo_upload_fail));

                }

            }, new Response.Listener<String>() {
                @Override
                public void onResponse(String json) {

                    Log.d("success============********>>", _photoPath);
                    Log.d("JSON=======",json);

                    ParseUploadImageResponse(json);

                }

            },file, ReqConst.REQ_PARAM_FILE, params);

            reqMultiPart.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT ,0 ,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            PicOneApplication.getInstance().addToRequestQueue(reqMultiPart,url);

        } catch (Exception e){

            e.printStackTrace();
            _activity.closeProgress();
            _activity.showAlertDialog(getString(R.string.photo_upload_fail));

        }
    }

    private void ParseUploadImageResponse(String json) {

        Log.d("responseimage====",json);

        _activity.closeProgress();

        try {

            JSONObject response = new JSONObject(json);
            int result_code = response.getInt(ReqConst.RES_CODE);

            Log.d("result_code==========", String.valueOf(result_code));

            if (result_code == ReqConst.CODE_SUCCESS){

                UserEntity _user = new UserEntity();

                if(_photoPath.length() != 0) {
                    _user.set_photoUrl(response.getString(ReqConst.RES_PHOTO_URL));
                }else {
                    _user.set_photoUrl(Commons.g_user.get_photoUrl());
                }

                _user.set_firstName(edt_firstname_edt.getText().toString());
                _user.set_lastName(edt_lastname_edt.getText().toString());
                _user.set_userName(edt_username_edt.getText().toString().trim());
                _user.set_email(edt_email_edt.getText().toString().trim());
                _user.set_password(edt_pwd_edt.getText().toString());
                _user.set_phoneNumber(edt_mobNumber_edt.getText().toString());
                _user.set_country(Constants.countryname);
                _user.set_adress(edt_add_edt.getText().toString().trim());
                _user.set_interests(edt_inter_edt.getText().toString());
                _user.set_status(edt_status_edt.getText().toString());

                Preference.getInstance().put(_activity,
                        PrefConst.PREFKEY_USERNAME, edt_username_edt.getText().toString().trim());

                Preference.getInstance().put(_activity,
                        PrefConst.PREFKEY_USERPWD, edt_pwd_edt.getText().toString().trim());

                Commons.g_user = _user ;

                Glide.with(this).load(Commons.g_user.get_photoUrl()).placeholder(R.drawable.image_me).into(ui_imvPhoto);

                _activity.showToast("Success change your profile !");

                _activity.selectDrawerItem(1);

                _activity.photoUpdate();

                _photoPath = "";

            }else if (result_code == ReqConst.CODE_UPLOADFAIL){
                _activity.showAlertDialog(getString(R.string.photo_upload_fail));
            }
        } catch (JSONException e) {
            e.printStackTrace();
            _activity.showAlertDialog(getString(R.string.photo_upload_fail));
        }

        //onSuccessSignup();
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){

            case R.id.txv_done_edt:
                if(checkValue()) {
                    progressEdit();
                }
                break;

            case R.id.imv_photo_edt:

                if (!_activity.hasPermissions(_activity, PERMISSIONS)){

                    ActivityCompat.requestPermissions(_activity, PERMISSIONS, MY_PEQUEST_CODE);
                }else {
                    selectPhoto();
                }
                break;
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        _activity = (MainActivity)context;
    }
}
