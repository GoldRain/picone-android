package com.picone.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.picone.R;
import com.picone.activity.MainActivity;
import com.picone.base.BaseFragment;
import com.picone.commons.Commons;
import com.picone.commons.Constants;
import com.picone.utils.RadiusImageView;

import de.hdodenhof.circleimageview.CircleImageView;


public class MyProfileFragment extends BaseFragment implements View.OnClickListener{

    MainActivity _activity ;
    View view ;

    ImageView ui_imvPhoto, ui_imvEdit, ui_imvDown ;
    RadiusImageView ui_imvSmallPho;
    TextView ui_txvFollower, ui_txvFollowing,txv_name_my,txv_smallName,ui_txvAddress, ui_txvFullprofiel ,ui_newsfeed;

    TextView txv_interest_des_my, txv_status_des_my ;
    LinearLayout  ui_lytInterest, ui_lytStatus,  ui_photo_name;

    private Uri _imageCaptureUri;
    String _photoPath = "";


    public MyProfileFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view =  inflater.inflate(R.layout.fragment_my_profile, container, false);


        loadLayout();
        return view ;
    }
        private void loadLayout() {

            ui_imvPhoto = (ImageView)view.findViewById(R.id.imv_photo_my);
            ui_imvPhoto.setOnClickListener(this);


            /*ui_imvPhoto.setImageBitmap(Constants.bitmap_my);*/
            ui_imvSmallPho = (RadiusImageView)view.findViewById(R.id.imv_photo_small);

            /*if(Constants.bitmap_my != null)
            ui_imvSmallPho.setImageBitmap(Constants.bitmap_my);*/

            ui_imvEdit = (ImageView)view.findViewById(R.id.imv_edit_my);
            ui_imvEdit.setOnClickListener(this);

            ui_imvDown = (ImageView)view.findViewById(R.id.imv_down);
            ui_imvDown.setOnClickListener(this);

            ui_txvFollower = (TextView)view.findViewById(R.id.txv_follower_my);
            ui_txvFollower.setOnClickListener(this);

            ui_txvFollowing = (TextView)view.findViewById(R.id.txv_following_my);
            ui_txvFollowing.setOnClickListener(this);

            txv_name_my = (TextView)view.findViewById(R.id.txv_name_my);
            txv_smallName = (TextView)view.findViewById(R.id.txv_smallName);

            ui_txvAddress = (TextView)view.findViewById(R.id.txv_ad_des_my);

            ui_txvFullprofiel = (TextView)view.findViewById(R.id.txv_full_profile);
            ui_txvFullprofiel.setOnClickListener(this);

            ui_newsfeed = (TextView)view.findViewById(R.id.txv_newsfeed);

            ui_photo_name = (LinearLayout)view.findViewById(R.id.lyt_photo_name);
            ui_lytInterest = (LinearLayout)view.findViewById(R.id.lyt_interests_my);
            ui_lytStatus = (LinearLayout)view.findViewById(R.id.lyt_status_my);

            txv_interest_des_my = (TextView)view.findViewById(R.id.txv_interest_des_my);
            txv_status_des_my = (TextView)view.findViewById(R.id.txv_status_des_my);


            UpdateDate();

    }

    private void UpdateDate() {

        ui_txvFollower.setText("Follower:"+String.valueOf(Commons.g_user.get_follower()));
        ui_txvFollowing.setText("Following:"+String.valueOf(Commons.g_user.get_following()));
        ui_txvAddress.setText(Commons.g_user.get_adress());
        txv_interest_des_my.setText(Commons.g_user.get_interests());
        txv_status_des_my.setText(Commons.g_user.get_status());

        if ((Commons.g_user.toString().length() > 0)){

            txv_name_my.setText(Commons.g_user.get_userName());
            txv_smallName.setText(Commons.g_user.get_userName());
        }

        if (Commons.g_user.get_photoUrl().length() != 0 && !Commons.g_user.get_photoUrl().equals(null)){

            Glide.with(this).load(Commons.g_user.get_photoUrl()).placeholder(R.drawable.image_me).into(ui_imvPhoto);
            Glide.with(this).load(Commons.g_user.get_photoUrl()).placeholder(R.drawable.image_me).into(ui_imvSmallPho);


            /*imv_photo.setImageUrl(Commons.g_user.get_photoUrl(),_imageLoader);*/

        }else {

            ui_imvPhoto.setImageResource(R.drawable.image_me);
        }
    }
    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.imv_photo_my:
                _activity.MyFullFragment();
                break;

            case R.id.imv_edit_my:
                _activity.EditProfileFragment();
                break;

            case R.id.txv_full_profile:
                fullProfile();
                break;

            case R.id.imv_down:
                partProfile();
                break;
            case R.id.txv_follower_my:
                gotoFollower();
                break;
            case R.id.txv_following_my:
                gotoFllowing_my();
                break;
        }

    }


    void gotoFollower(){

        _activity.FollowerFragment();

    }
    void gotoFllowing_my(){

        _activity.FollowingFragment();

    }

    private void fullProfile() {

        ui_txvFullprofiel.setVisibility(View.GONE);
        ui_newsfeed.setVisibility(View.GONE);
        ui_photo_name.setVisibility(View.GONE);
        ui_imvDown.setVisibility(View.GONE);

        ui_lytInterest.setVisibility(View.VISIBLE);
        ui_lytStatus.setVisibility(View.VISIBLE);
        ui_imvDown.setVisibility(View.VISIBLE);

    }

    private void partProfile() {

        ui_txvFullprofiel.setVisibility(View.VISIBLE);
        ui_newsfeed.setVisibility(View.VISIBLE);
        ui_photo_name.setVisibility(View.VISIBLE);
        ui_imvDown.setVisibility(View.VISIBLE);

        ui_lytInterest.setVisibility(View.GONE);
        ui_lytStatus.setVisibility(View.GONE);
        ui_imvDown.setVisibility(View.GONE);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        _activity = (MainActivity)context;
    }

}
