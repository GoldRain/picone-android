package com.picone.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.picone.R;
import com.picone.chatting.ChattingActivity;
import com.picone.activity.MainActivity;
import com.picone.model.Friendmodel;
import com.picone.utils.RadiusImageView;

import java.util.ArrayList;

/**
 * Created by ToSuccess on 12/2/2016.
 */

public class FollowingAdapter extends BaseAdapter {

    MainActivity _activity;

    ArrayList<Friendmodel> _following = new ArrayList<>();
    ArrayList<Friendmodel> _allFollowings =  new ArrayList<>();

    Friendmodel trip;
    FollowingHolder followingHolder ;

    int _idx ;
    int _curPage = 1;

    public FollowingAdapter (MainActivity activity){

        this._activity = activity ;

    }

    public void setFollowingDatas (ArrayList<Friendmodel> following){

        _allFollowings = following;
        _following.clear();
        _following.addAll(_allFollowings);
    }

    @Override
    public int getCount() {
        return _following.size();
    }

    @Override
    public Object getItem(int position) {
        return _following.get(position);
    }

    @Override
    public long getItemId(int postion) {
        return postion;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        trip = _following.get(position);

        if (convertView == null){

            followingHolder = new FollowingHolder();

            LayoutInflater inflater = (LayoutInflater)_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_list_following, parent, false);

            followingHolder.imv_photo_following = (RadiusImageView) convertView.findViewById(R.id.imv_photo_following);
            followingHolder.txv_name_following = (TextView)convertView.findViewById(R.id.txv_name_following);

            convertView.setTag(followingHolder);

        } else {

            followingHolder = (FollowingHolder) convertView.getTag();
        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*_activity.ChatFragment();*/

                Intent intent = new Intent(_activity, ChattingActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                _activity.startActivity(intent);
            }
        });



        final Friendmodel followingModel = _following.get(position);

        Glide.with(_activity).load(followingModel.getFriendphotourl()).placeholder(R.drawable.image_user).into(followingHolder.imv_photo_following);
        followingHolder.txv_name_following.setText(followingModel.getFriendname());

        return convertView;
    }

    public class FollowingHolder {

        RadiusImageView imv_photo_following;
        TextView txv_name_following;
    }
}
