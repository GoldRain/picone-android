package com.picone.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.picone.R;
import com.picone.activity.MainActivity;
import com.picone.fragment.UserProfileFragment;
import com.picone.utils.RadiusImageView;

/**
 * Created by ToSuccess on 12/7/2016.
 */

public class UserFollowingAdapter extends BaseAdapter{

    MainActivity activity ;
    UserProfileFragment fragment ;

    public UserFollowingAdapter (UserProfileFragment fragment){

        this.activity = (MainActivity) fragment.getActivity() ;
        this.fragment = fragment ;
    }
    @Override
    public int getCount() {
        return 10;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)  {

        UserFollowingHolder userFollowingHolder ;

    if (convertView == null){

        userFollowingHolder = new UserFollowingHolder();

        LayoutInflater inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.item_list_follower, parent,false);

        userFollowingHolder.imv_Followerphoto = (RadiusImageView)convertView.findViewById(R.id.imv_photo);
        userFollowingHolder.txv_Followername = (TextView)convertView.findViewById(R.id.txv_Followername);
        userFollowingHolder.txv_follow = (TextView)convertView.findViewById(R.id.txv_follow_user_list);

        convertView.setTag(userFollowingHolder);

    } else {

        userFollowingHolder = (UserFollowingHolder)convertView.getTag();
    }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

    return convertView;

}

public class UserFollowingHolder {

    RadiusImageView imv_Followerphoto ;
    TextView txv_Followername ,txv_follow;

    }
}
