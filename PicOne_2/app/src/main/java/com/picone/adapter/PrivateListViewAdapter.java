package com.picone.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.picone.R;
import com.picone.activity.MainActivity;
import com.picone.commons.Commons;
import com.picone.commons.Constants;
import com.picone.fragment.NewsfeedsFragment;
import com.picone.model.Friendmodel;
import com.picone.model.VoteEntity;
import com.picone.utils.RadiusImageView;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by ToSuccess on 12/9/2016.
 */

public class PrivateListViewAdapter extends BaseAdapter {

    ArrayList<Friendmodel>friendmodels = new ArrayList<>();

    MainActivity _activity ;
    NewsfeedsFragment _fragment = new NewsfeedsFragment();

    int click_cnt = 0;

    public PrivateListViewAdapter ( NewsfeedsFragment fragment, MainActivity activity, ArrayList<Friendmodel>friendmodels){

        this._activity = activity;
        this._fragment = fragment ;
        this.friendmodels = friendmodels;



    }
    @Override
    public int getCount() {
        return friendmodels.size();
    }

    @Override
    public Object getItem(int postion) {
        return friendmodels.get(postion);
    }

    @Override
    public long getItemId(int postion) {
        return postion;
    }

    @Override
    public View getView(final int postion, View convertView, ViewGroup parent) {

        PrivateHolder privateHolder = new PrivateHolder() ;

        if (convertView == null){

           // privateHolder = new PrivateHolder();
            Friendmodel friendmodel=friendmodels.get(postion);

            LayoutInflater inflater = (LayoutInflater)_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_list_private, parent,false);

            privateHolder.ui_imvPhoto = (RadiusImageView)convertView.findViewById(R.id.imv_photo_pri);
            privateHolder.ui_txvName = (TextView)convertView.findViewById(R.id.txv_name_pri);
            privateHolder.ui_imvCheck = (ImageView)convertView.findViewById(R.id.imv_addState) ;

            convertView.setTag(privateHolder);
            
        }else {

            privateHolder = (PrivateHolder)convertView.getTag();
        }

        final ImageView check = privateHolder.ui_imvCheck ;

        final Friendmodel friend = friendmodels.get(postion);

        friend.setCheckstatuse(0);

        privateHolder.ui_imvPhoto.setImageResource(R.drawable.image_me);
        privateHolder.ui_txvName.setText(friend.getFriendname());
        check.setSelected(false);
         convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                check.setSelected(!check.isSelected());
                friend.setCheckstatuse(1);
            }
        });

        return convertView;
    }

    public class PrivateHolder {
        
        RadiusImageView ui_imvPhoto;
        TextView ui_txvName;
        ImageView ui_imvCheck;
    }
}
