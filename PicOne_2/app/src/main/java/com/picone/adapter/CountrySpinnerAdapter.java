package com.picone.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.picone.R;
import com.picone.commons.Constants;

import java.util.Locale;

/**
 * Created by ToSuccess on 12/1/2016.
 */

public class CountrySpinnerAdapter extends BaseAdapter {

    Context context ;
    String [] country_name;

    public CountrySpinnerAdapter(Context context){

        this.context = context;

        country_name = context.getResources().getStringArray(R.array.CountryCodes);
    }

    @Override
    public int getCount() {
        return country_name.length;
    }

    @Override
    public Object getItem(int postion) {
        return null;
    }

    @Override
    public long getItemId(int postion ){return postion;}

    @Override
    public View getView(int position, View converView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        converView = inflater.inflate(R.layout.item_country, parent,false);

        ImageView ui_imv_flag = (ImageView)converView.findViewById(R.id.imv_flag);
        TextView ui_txv_name = (TextView)converView.findViewById(R.id.txv_name);

        String[] g = country_name[position].split(",");
        String pngName = g[0].trim().toLowerCase();

        if (position == 0){
            ui_txv_name.setText(GetCountryZipCode(g[0].trim()));

        } else {
            ui_txv_name.setText(GetCountryZipCode(g[1].trim()));
            ui_imv_flag.setImageResource(context.getResources().getIdentifier("drawable/ic_flag_flat_" + pngName, null, context.getPackageName()));

             Constants.countryname = ui_txv_name.getText().toString();

        }

        return converView;
    }

    public String GetCountryZipCode (String ssid){

        Locale locale = new Locale("", ssid);
        return locale.getDisplayCountry().trim();
    }
}
