package com.picone.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.picone.R;
import com.picone.activity.MainActivity;
import com.picone.fragment.ContactsFragment;
import com.picone.model.Friendmodel;
import com.picone.utils.RadiusImageView;

import java.util.ArrayList;

/**
 * Created by ToSuccess on 11/29/2016.
 */

public class ContactslistViewAdapter extends BaseAdapter {

    MainActivity _activity;
    ContactsFragment _fragment ;
    ArrayList<Friendmodel> _friends = new ArrayList<>();
    ArrayList<Friendmodel> _allFriends = new ArrayList<>();

    public ContactslistViewAdapter(MainActivity activity, ArrayList<Friendmodel> friends){

        this._activity = activity ;
    }

    public void setContacts(ArrayList<Friendmodel> friends) {

        _allFriends = friends ;

        _friends.clear();
        _friends.addAll(_allFriends);

        notifyDataSetChanged();
    }


    public void setDatas(ArrayList<Friendmodel> datas){

      /*  allFriends = datas ;
        friends.addAll(allFriends);
        notifyDataSetChanged();*/
    }
    public void addItem(Friendmodel data){
        _friends.add(data);
    }
    @Override
    public int getCount() {
        return _friends.size();
    }

    @Override
    public Object getItem(int position) {
        return _friends.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView( int position, View convertView, ViewGroup parent) {

        ContactsHolder contactsHolder;

        if (convertView == null){

            contactsHolder = new ContactsHolder();

            LayoutInflater inflater = (LayoutInflater)_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_list_contact, parent,false);

            //Toast.makeText(_activity, "active layout", Toast.LENGTH_SHORT).show();

            contactsHolder.ui_imvPhoto = (RadiusImageView)convertView.findViewById(R.id.imv_photo_con);
            contactsHolder.ui_txvName = (TextView)convertView.findViewById(R.id.txv_name_con);

            convertView.setTag(contactsHolder);
        } else {

            contactsHolder = (ContactsHolder) convertView.getTag();
        }

        final  Friendmodel friend = (Friendmodel)_friends.get(position);

        contactsHolder.ui_txvName.setText(friend.getFriendname());

        if (friend.getFriendphotourl().length() > 0){

            Glide.with(_activity).load(friend.getFriendphotourl()).placeholder(R.drawable.bg_non_profile).into(contactsHolder.ui_imvPhoto);

        }else {

            contactsHolder.ui_imvPhoto.setImageResource(R.drawable.bg_non_profile);
        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                /*_activity.ChatFragment();*/
               /* Intent intent = new Intent(_activity, ChattingActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                _activity.startActivity(intent);*/
                _activity.gotoChatting(friend);
            }
        });

        return convertView;
    }

    public class ContactsHolder {

        RadiusImageView ui_imvPhoto;
        TextView ui_txvName;
    }
}
