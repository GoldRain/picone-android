package com.picone.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.picone.PicOneApplication;
import com.picone.R;
import com.picone.activity.MainActivity;
import com.picone.commons.Commons;
import com.picone.commons.Constants;
import com.picone.commons.ReqConst;
import com.picone.fragment.NewsfeedsFragment;
import com.picone.model.VoteEntity;
import com.picone.utils.RadiusImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by ToSuccess on 11/29/2016.
 */

public class NewsfeedslistAdapter extends BaseAdapter{

    MainActivity _activity ;
    NewsfeedsFragment _newfeedFragment;

    private ImageLoader _imageLoader;

    private ArrayList<VoteEntity> _voteModel = new ArrayList<VoteEntity>();
    private ArrayList<VoteEntity> _allVoteModel = new ArrayList<>();

    int _idx ;  //my ID:
    /* int _vote1 = 0, _vote2 = 0;*/

    public NewsfeedslistAdapter(NewsfeedsFragment fragment){

        this._activity = (MainActivity) fragment.getActivity() ;
        this._newfeedFragment = fragment;
    }

    public void setVoteDatas(ArrayList<VoteEntity> votes){

        _allVoteModel = votes ;
        _voteModel.clear();
        _voteModel.addAll(_allVoteModel);

        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return _voteModel.size();
    }

    @Override
    public Object getItem(int position) {
         return _voteModel.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertview, ViewGroup parent) {

        final NewsHolder newsHolder ;

        if (convertview == null){

            newsHolder = new NewsHolder();

            LayoutInflater inflater = (LayoutInflater)_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertview = inflater.inflate(R.layout.news_list_item, parent, false);

            newsHolder.ui_imvUser = (RadiusImageView)convertview.findViewById(R.id.imv_user_photo);
            newsHolder.txv_user_name = (TextView)convertview.findViewById(R.id.txv_user_name);
            newsHolder.txv_title = (TextView)convertview.findViewById(R.id.txv_title);
            newsHolder.txv_description = (TextView)convertview.findViewById(R.id.txv_description);

            newsHolder.txv_vote_left = (TextView)convertview.findViewById(R.id.txv_vote_left);
            newsHolder.txv_vote_right = (TextView)convertview.findViewById(R.id.txv_vote_right);
            newsHolder.txv_percent_left = (TextView)convertview.findViewById(R.id.txv_percent_left);
            newsHolder.txv_percent_right = (TextView)convertview.findViewById(R.id.txv_percent_right);

            newsHolder.imv_vote_left_rec = (ImageView) convertview.findViewById(R.id.imv_vote_left_rec);
            newsHolder.imv_vote_right_rec = (ImageView)convertview.findViewById(R.id.imv_vote_right_rec);

            convertview.setTag(newsHolder);

        }else {

            newsHolder = (NewsHolder)convertview.getTag();
        }

        final VoteEntity voteModel = _voteModel.get(position);

        newsHolder.txv_percent_left.setText(String.valueOf(voteModel.get_votePercentL()));
        newsHolder.txv_percent_right.setText(String.valueOf(voteModel.get_votePercentR()));

        Glide.with(_activity).load(voteModel.get_userphotoUrl()).placeholder(R.drawable.image_user).into(newsHolder.ui_imvUser);
        Constants.userPhoto = voteModel.get_userphotoUrl();

        /*Left photo setting*/
        Glide.with(_activity).load(voteModel.get_votePhotoUrl_L()).placeholder(R.drawable.image_me).into(newsHolder.imv_vote_left_rec);

        /*Right photo setting*/
        Glide.with(_activity).load(voteModel.get_votePhotoUrl_R()).placeholder(R.drawable.image_user).into(newsHolder.imv_vote_right_rec);

        newsHolder.txv_user_name.setText(voteModel.get_username());
        newsHolder.txv_title.setText(voteModel.get_title());
        newsHolder.txv_description.setText(voteModel.get_description());

        newsHolder.txv_vote_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                setVote(newsHolder,voteModel, 1,0);
               /* _newfeedFragment.leftVoteCliked(_voteModel.get(position));*/
            }
        });

        newsHolder.txv_vote_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                setVote(newsHolder,voteModel , 0, 1);

               /* _newfeedFragment.rightVoteCliked(_voteModel.get(position));*/
            }
        });

        newsHolder.ui_imvUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Constants.list_position = position ;
                _activity.UserProfileFratment(voteModel);
            }
        });

        return convertview;
    }

    public void setVote(final NewsHolder _newsHolder, VoteEntity setVote , int _vote1, int _vote2){

        _activity.showProgress();

        String url = ReqConst.SERVER_URL + ReqConst.REQ_SETVOTE ;
        _idx = Commons.g_user.get_idx();
        int postid = setVote.getId() ;
        int vote1 = _vote1 ;
        int vote2 = _vote2 ;

        String params = String.format("/%d/%d/%d/%d", _idx, postid, vote1, vote2);

        url += params ;
        Log.d("=========url====== vote", url);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {

                _activity.closeProgress();

                try {

                    JSONObject respons = new  JSONObject(json);

                    Log.d("=========respons========", json);
                    Log.d("Respons=======", String.valueOf(respons));

                    int result_code = respons.getInt(ReqConst.RES_CODE);

                    if (result_code == ReqConst.CODE_SUCCESS){

                        int vote1 = respons.getInt("vote1");
                        int vote2 = respons.getInt("vote2");

                        Log.d("======vote1========", String.valueOf(vote1));

                        _newsHolder.txv_percent_left.setText(String.valueOf(vote1));
                        _newsHolder.txv_percent_right.setText(String.valueOf(vote2));

                        _activity.closeProgress();
                        _activity.showToast("You are vote !");


                    } else if (result_code == ReqConst.CODE_ALREADYVOTE){
                        _activity.showAlertDialog("You have already voted.!");

                    } else {
                        _activity.closeProgress();
                        _activity.showAlertDialog(String.valueOf(R.string.error));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    _activity.showAlertDialog(String.valueOf(R.string.error));
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                _activity.closeProgress();
                _activity.showAlertDialog(String.valueOf(R.string.error));
            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT,0,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        PicOneApplication.getInstance().addToRequestQueue(stringRequest, url);
    }

    public void addItem(VoteEntity entity){
        _voteModel.add(entity);
    }

    public class NewsHolder {

        RadiusImageView ui_imvUser;
        TextView txv_user_name;
        TextView txv_title;
        TextView txv_description ;
        TextView txv_vote_left;
        TextView txv_vote_right;
        TextView txv_percent_left;
        TextView txv_percent_right;

        ImageView imv_vote_left_rec;
        ImageView imv_vote_right_rec;

    }
}
