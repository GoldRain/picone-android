package com.picone.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.picone.R;

import java.util.Locale;

/**
 * Created by ToSuccess on 12/2/2016.
 */

public class PhonenumberAdapter extends BaseAdapter{

    private final Context context;

    String[] country_name;

    public PhonenumberAdapter(Context activity) {

        this.context = activity;

        country_name = context.getResources().getStringArray(R.array.CountryPhone);
    }

    @Override
    public int getCount() {
        return country_name.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater =  (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.item_phone, parent, false);

        TextView ui_txv_name = (TextView)convertView.findViewById(R.id.txv_phone);


        String[] g = country_name[position].split(",");

        if(position==0){

            ui_txv_name.setText(g[0].trim());

        }else {

            ui_txv_name.setText(GetCountryZipCode(g[0]).trim());}


        return convertView;
    }

    private String GetCountryZipCode(String ssid){

        Locale loc = new Locale("", ssid);
        return loc.getDisplayCountry().trim();
    }
}
