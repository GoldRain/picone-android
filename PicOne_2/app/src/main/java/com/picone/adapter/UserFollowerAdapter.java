package com.picone.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.picone.R;
import com.picone.activity.MainActivity;
import com.picone.fragment.UserProfileFragment;
import com.picone.model.Friendmodel;
import com.picone.utils.RadiusImageView;

import java.util.ArrayList;

/**
 * Created by ToSuccess on 12/7/2016.
 */

public class UserFollowerAdapter extends BaseAdapter {

    MainActivity activity ;
    UserProfileFragment fragment ;
    ArrayList<Friendmodel>friendmodels=new ArrayList<Friendmodel>();

    public UserFollowerAdapter (UserProfileFragment fragment){

        this.activity = (MainActivity) fragment.getActivity() ;
        this.fragment = fragment ;
    }

    @Override
    public int getCount() {
        return 12;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position ;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        UserFollowerHolder userFollowerHolder ;

        if (convertView == null){

            userFollowerHolder = new UserFollowerHolder();

            LayoutInflater inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_list_userfollower, parent,false);
            userFollowerHolder.friendphoto=(RadiusImageView)convertView.findViewById(R.id.imv_Followerphoto_user);
            userFollowerHolder.friendname=(TextView)convertView.findViewById(R.id.txv_Followername_user);
            userFollowerHolder.follow=(TextView)convertView.findViewById(R.id.txv_follow_user_list);

        } else {

            userFollowerHolder = (UserFollowerHolder)convertView.getTag();
        }


       // Glide.with(_activity).load(voteModel.get_votePhotoUrl_R()).placeholder(R.drawable.image_user).into(newsHolder.imv_vote_right_rec);


        return convertView;

    }

    public class UserFollowerHolder {
        RadiusImageView friendphoto;
        TextView friendname;
        TextView follow;


    }
}
