package com.picone.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.picone.R;
import com.picone.commons.Constants;

import java.util.Locale;

/**
 * Created by ToSuccess on 12/2/2016.
 */

public class PublicAlertAdapter extends BaseAdapter {

    private final Context context;

    String[] PublicState;

    public PublicAlertAdapter (Context activity) {
        this.context = activity;

        PublicState = context.getResources().getStringArray(R.array.PublicState);
    }

    @Override
    public int getCount() {
        return PublicState.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater =  (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.item_public, parent, false);

        TextView ui_txv_name = (TextView)convertView.findViewById(R.id.txv_state);




        if(position==0){
            ui_txv_name.setText(PublicState[0].trim());

        }else {
            ui_txv_name.setText(PublicState[1].trim());
        }
        Constants.publicstatus=ui_txv_name.getText().toString();



        return convertView;
    }

    private String GetCountryZipCode(String ssid){

        Locale loc = new Locale("", ssid);
        return loc.getDisplayCountry().trim();
    }
}
