package com.picone.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.picone.PicOneApplication;
import com.picone.R;
import com.picone.activity.MainActivity;
import com.picone.commons.Commons;
import com.picone.commons.Constants;
import com.picone.commons.ReqConst;
import com.picone.model.NotiModel;
import com.picone.utils.RadiusImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by ToSuccess on 12/1/2016.
 */

public class NotificationListAdapter extends BaseAdapter{

    MainActivity _activity ;

    ArrayList<NotiModel> notiModels = new ArrayList<>();


    /*String followerHolder = "";*/

    public NotificationListAdapter(MainActivity activity, ArrayList<NotiModel> notiModels){

        this._activity = activity ;
        this.notiModels = notiModels;
    }
    
    public void setNotiDatas(ArrayList<NotiModel> notiModel) {


        notiModels.clear();
        notiModels.addAll(notiModel);

        //notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return notiModels.size();
    }

    @Override
    public Object getItem(int position) {
        return notiModels.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

         final NotiModel trip = notiModels.get(position);

         final NotiFolder notiFolder ;

         if (convertView == null){

             notiFolder = new NotiFolder();

            LayoutInflater inflater = (LayoutInflater)_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_list_notification, parent,false);

             notiFolder.noti_photo = (RadiusImageView) convertView.findViewById(R.id.imv_photo_noti);
             notiFolder.noti_name = (TextView) convertView.findViewById(R.id.txv_name);
             notiFolder.noti_content = (TextView)convertView.findViewById(R.id.txv_content_noti);
             notiFolder.accept = (TextView)convertView.findViewById(R.id.txv_accept_noti ) ;

             convertView.setTag(notiFolder);

         } else {

             notiFolder = (NotiFolder) convertView.getTag();
         }

        notiFolder.noti_name.setText(trip.getUsername());
        notiFolder.noti_content.setText("You received the friend request !");
        notiFolder.accept.setText("ACCEPT");

        if (Constants.notiModels.size() > 0){

        Glide.with(_activity).load(trip.getNoti_photo()).placeholder(R.drawable.image_user).into(notiFolder.noti_photo);}

        notiFolder.accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            Toast.makeText(_activity,  String.valueOf(trip.getId()), Toast.LENGTH_SHORT).show();

                friendRequestAccept(trip, position);

               // followerHolder.accept.setText("ACCEPTED");

            }
        });

        return convertView;
    }

    public void friendRequestAccept(NotiModel notiModel, final int position){

        String url = ReqConst.SERVER_URL + ReqConst.REQ_ACCEPTFRIEND;
        int myid = Commons.g_user.get_idx();
        int friedId = notiModel.getId();
        String params = String.format("/%d/%d/", myid,  friedId);

        url += params;

        _activity.showProgress();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {

                _activity.closeProgress();

                try {
                    JSONObject response = new JSONObject(json);

                    int result_code = response.getInt(ReqConst.RES_CODE);

                    if (result_code == ReqConst.CODE_SUCCESS){
                        _activity.showToast("Accept");

                        notiModels.remove(position);

                        Constants.noti_counter-- ;

                        _activity.showNotiRefresh();

               /*         if (Constants.noti_counter == 0) {

                            _activity.showNoti(View.GONE, 0);

                        } else {

                            _activity.showNoti(View.VISIBLE, Constants.noti_counter);
                        }
*/
                        Constants.notiModels = notiModels;
                        notifyDataSetChanged();
                    }
                } catch (JSONException e) {

                    _activity.closeProgress();
                    _activity.showAlertDialog(String.valueOf(R.string.error));
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                _activity.closeProgress();
            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT,0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        PicOneApplication.getInstance().addToRequestQueue(stringRequest, url);
    }

    public class NotiFolder {

        RadiusImageView noti_photo ;
        TextView noti_name;
        TextView noti_content;
        TextView accept;
    }
}
