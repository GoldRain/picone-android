package com.picone.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.picone.PicOneApplication;
import com.picone.R;
import com.picone.activity.MainActivity;
import com.picone.commons.Commons;
import com.picone.commons.Constants;
import com.picone.commons.ReqConst;
import com.picone.model.Friendmodel;
import com.picone.utils.RadiusImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by ToSuccess on 12/1/2016.
 */

public class FollowerListAdapter extends BaseAdapter{

    MainActivity _activity ;

    ArrayList<Friendmodel>friendmodels = new ArrayList<>();
    ArrayList<Friendmodel>_allFollows = new ArrayList<>();

    int _userID;
    int _idx ;

    int _curPage = 1;

    /*String followerHolder = "";*/

    public FollowerListAdapter (MainActivity activity){
        this._activity = activity ;

    }
    
    public void setFollowerDatas(ArrayList<Friendmodel> followers) {
     
        _allFollows = followers ;
        friendmodels.clear();
        friendmodels.addAll(_allFollows);

        //notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return friendmodels.size();
    }

    @Override
    public Object getItem(int position) {
        return friendmodels.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

         final Friendmodel trip = friendmodels.get(position);

         final FollowerHolder followerHolder ;

         if (convertView == null){

            followerHolder = new FollowerHolder();

            LayoutInflater inflater = (LayoutInflater)_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_list_follower, parent,false);

            followerHolder.imv_Followerphoto = (RadiusImageView)convertView.findViewById(R.id.imv_Followerphoto);
            followerHolder.txv_Followername = (TextView)convertView.findViewById(R.id.txv_Followername ) ;
            followerHolder.txv_follow = (TextView)convertView.findViewById(R.id.txv_follow);

            convertView.setTag(followerHolder);

         } else {

            followerHolder = (FollowerHolder)convertView.getTag();
         }

        followerHolder.txv_Followername.setText(trip.getFriendname());
        Glide.with(_activity).load(trip.getFriendphotourl()).placeholder(R.drawable.image_user).into(followerHolder.imv_Followerphoto);

        if(trip.getFollowstatuse() == 1){

            followerHolder.txv_follow.setText("UnFollow");

        }else {

            followerHolder.txv_follow.setText("Follow");
        }

        followerHolder.txv_follow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                 if(trip.getFollowstatuse() == 0){
//                    followerHolder.txv_follow.setText("UnFollow");
//                     trip.setFollowstatuse(1);
//
//                }else {
//                     followerHolder.txv_follow.setText("Follow");
//                    trip.setFollowstatuse(0);
//                }
                Constants.list_Follow_position = position ;
                Follower(trip, followerHolder);
            }
        });

        return convertView;
    }

    public void Follower(final Friendmodel trip, final FollowerHolder _followerHolder) {

        _activity.showProgress();

        String url = ReqConst.SERVER_URL + ReqConst.REQ_SETFOLLOWER;

        _idx = Commons.g_user.get_idx();
        _userID = trip.getId();

        String params = String.format("/%d/%d", _idx, _userID, _curPage);
        url += params ;

        //Log.d("======URL=====",url);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String json) {

                _activity.closeProgress();

                try {

                    JSONObject respons = new JSONObject(json);
                    int result_code = respons.getInt(ReqConst.RES_CODE);

                    //Log.d("=========Follow=========", String.valueOf(trip.getFollowstatuse()));

                    if (result_code == ReqConst.CODE_SUCCESS){

                        _activity.closeProgress();

                        if(trip.getFollowstatuse() == 0){
                            _followerHolder.txv_follow.setText("UnFollow");
                            trip.setFollowstatuse(1);
                            _activity.showToast("Success Follower");

                            int follower = Commons.g_user.get_follower();
                            follower++;
                            Commons.g_user.set_follower(follower);

                        }else {
                            _followerHolder.txv_follow.setText("Follow");
                            trip.setFollowstatuse(0);
                            _activity.showToast("Success Unfollower");

                            int follower = Commons.g_user.get_follower();

                            if (follower == 0){
                                _activity.showAlertDialog("Don't Unfollower!");
                            } else follower--;

                            Commons.g_user.set_follower(follower);
                        }
                    }

                } catch (JSONException e) {

                    e.printStackTrace();
                    _activity.showAlertDialog(String.valueOf(R.string.error));
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                _activity.closeProgress();
                _activity.showAlertDialog(String.valueOf(R.string.error));
            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT,0,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        PicOneApplication.getInstance().addToRequestQueue(stringRequest,url);
    }

    public class FollowerHolder {

        RadiusImageView imv_Followerphoto ;
        TextView txv_Followername ,txv_follow;
    }


}
